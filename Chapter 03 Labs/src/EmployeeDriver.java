
public class EmployeeDriver 
{
	public static void main(String[]args)
	{
		Employee emp1 = new Employee();
		
		emp1.setName("Susan Meyers");
		emp1.setIdNumber(47899);
		emp1.setDepartment("Accounting");
		emp1.setPosition("Vice President");
	
		Employee emp2 = new Employee();
		
		emp2.setName("Mark Jones");
		emp2.setIdNumber(39119);
		emp2.setDepartment("IT");
		emp2.setPosition("Programmer");
		
		Employee emp3 = new Employee();
		
		emp3.setName("Joy Rogers");
		emp3.setIdNumber(81774);
		emp3.setDepartment("Manufacturing");
		emp3.setPosition("Engineer");
		
		System.out.println(emp1.getName());
		System.out.println(emp1.getId());
		System.out.println(emp1.getDepartment());
		System.out.println(emp1.getPosition());
		System.out.println();

		System.out.println(emp2.getName());
		System.out.println(emp2.getId());
		System.out.println(emp2.getDepartment());
		System.out.println(emp2.getPosition());
		System.out.println();
		
		System.out.println(emp3.getName());
		System.out.println(emp3.getId());
		System.out.println(emp3.getDepartment());
		System.out.println(emp3.getPosition());
		System.out.println();
		
	}







}
