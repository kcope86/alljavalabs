import java.util.Scanner;
public class IngredientAdjuster
{
	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			String input;
			int cookies;
			 double sugar;
			 double butter;
			 double flour;
			
			
			System.out.print("How many cookies would you like to make? ");
			cookies = keyboard.nextInt();
			
			sugar = (1.5d / 48d) * cookies;
			butter = (1.0d / 48d) * cookies;
			flour = (2.75d / 48d) * cookies;
			System.out.printf("You need \n" +
			"%.2f cups of sugar, %.2f cups of butter, and %.2f cups of flour \n", sugar, butter, flour);
			
			
			
			
			
			
			
			
			
			
			System.out.println("Would you like to check different amounts?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
		}while (repeat == 'Y' || repeat == 'y');
		
	}
}