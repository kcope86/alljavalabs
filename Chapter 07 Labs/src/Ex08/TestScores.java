package Ex08;

public class TestScores {
	private static int student = 0;
	private int studentNumber;
	private String[] names = new String[5];
	private char[] grades = new char[5];
	private double[][] scores = new double[5][4];

	public TestScores(String[] names, double[][] scores) {
		int row = 0;
		for (int i = 0; i < names.length; i++) {
			this.names[i] = names[i];
		}
		while (row < 5) {
			for (int c = 0; c < 4; c++) {
				this.scores[row][c] = scores[row][c];

			}
			row++;
		}

	}

	public double getAverage(int student) {
		double average = 0;

		for (int i = 0; i < 4; i++) {
			average += scores[student][i];

		}
		average /= 4;
		return average;
	}

	public char getLetterGrade(int student) {
		char grade;
		if (getAverage(student) >= 90 && getAverage(student) <= 100) {
			grade = 'A';
		} else if (getAverage(student) >= 80 && getAverage(student) <= 89) {
			grade = 'B';
		} else if (getAverage(student) >= 70 && getAverage(student) <= 79) {
			grade = 'C';
		} else if (getAverage(student) >= 60 && getAverage(student) <= 69) {
			grade = 'D';
		} else {
			grade = 'F';
		}
		return grade;

	}

	public String getName(int student) {

		return names[student];

	}
}
