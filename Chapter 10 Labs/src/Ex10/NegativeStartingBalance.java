package Ex10;

public class NegativeStartingBalance extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * No-arg constructor
	 */

	public NegativeStartingBalance() {
		super("Error: Negative starting balance");
	}

	/**
	 * The following constructor accepts the amount that was given as the starting
	 * balance.
	 */

	public NegativeStartingBalance(double amount) {
		super("Error: Negative starting balance: " + amount);
	}
}