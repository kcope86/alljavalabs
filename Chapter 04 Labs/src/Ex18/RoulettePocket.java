package Ex18;

public class RoulettePocket {
	private String stringNumber;
	private int number;

	public RoulettePocket(String number) {
		stringNumber = number;
		 

	}

	public String getPocketColor() {
		String color = "";
		if ((number > 0 && number < 11) && number % 2 == 0) {
			color += "Black";
		} else if ((number > 10 && number < 19) && number % 2 != 0) {
			color += "Black";
		} else if ((number > 18 && number < 29) && number % 2 == 0) {
			color += "Black";
		} else if ((number > 28 && number < 37) && number % 2 != 0) {
			color += "Black";
		} else if ((number > 0 && number < 11) && number % 2 != 0) {
			color += "Red";
		} else if ((number > 10 && number < 19) && number % 2 == 0) {
			color += "Red";
		} else if ((number > 18 && number < 29) && number % 2 != 0) {
			color += "Red";
		} else if ((number > 28 && number < 37) && number % 2 == 0) {
			color += "Red";
		} else {
			color += "Green";
		}
		return color;
	}
}
