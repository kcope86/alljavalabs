package Ex08;

import java.util.Scanner;

public class HighestLowest {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int firstInput;
		final int ALL_DONE = -99;
		int highest = 0;
		int lowest = 0;

		do {
			System.out.print("Enter a series of integers and type -99 when finished entering numbers: ");
			firstInput = keyboard.nextInt();

			if (firstInput >= highest && firstInput != -99) {
				highest = firstInput;
			} else if (firstInput <= lowest && firstInput != -99) {
				lowest = firstInput;
			} else if (firstInput == -99) {
				break;
			}
		} while (firstInput != ALL_DONE);
		System.out.printf("The highest number entered is %d\n", highest);
		System.out.printf("The lowest number entered is %d\n", lowest);
		keyboard.close();
	}

}
