package Ex11;

public class Geometry {

	public static double AreaOfCircle(double r) {
		double area = Math.PI * (Math.pow(r, 2));
		return area;
	}

	public static double AreaOfRectangle(double l, double w) {
		double area = l * w;
		return area;
	}

	public static double AreaOfTriangle(double b, double h) {
		double area = b * h * 0.5;
		return area;
	}

}
