package Ex15;

public class Points {
	private int books;

	public Points(int books) {
		this.books = books;
	}

	public int calcClubPoints() {
		int points;
		if (books >= 4) {
			points = 60;
		} else if (books == 3) {
			points = 30;
		} else if (books == 2) {
			points = 15;
		} else if (books == 1) {
			points = 5;
		} else {
			points = 0;
		}

		return points;
	}
}
