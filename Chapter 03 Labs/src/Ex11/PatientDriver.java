package Ex11;

public class PatientDriver {

	public static void main(String[] args) 
	{
		Patient kevin = new Patient("Kevin", "Matthew", "Cope", "5636 Delintry dr. APT h", "St. Louis", "MO", "63129", "(636)215-5396", "Mary Anne Cody", "(314)225-8084");
		Procedure p1 = new Procedure("Physical Exam", "1/22/2018", "Dr. Irvine", 250.00);
		Procedure p2 = new Procedure("X-ray", "1/22/2018", "Dr. Jamison", 500.00);
		Procedure p3 = new Procedure("Blood test", "1/22/2018", "Dr. Smith", 200.00);
		
		
		System.out.println("Patient Info: \n");
				
		System.out.printf("Patient: %s %s %s \n", kevin.getFirstName(), kevin.getMiddleName(), kevin.getLastName());
		System.out.println();
		
		System.out.printf("Address: %s %s %s %s \n", kevin.getAddress(), kevin.getCity(), kevin.getState(), kevin.getZip());
		System.out.println();
		
		System.out.printf("Phone Number: %s \n", kevin.getPhoneNumber());
		System.out.println();
		
		System.out.printf("Emergency Contact Info: %s, %s \n", kevin.getEmergencyName(), kevin.getEmergencyPhone());
		System.out.println();
		System.out.println();
		
		
		System.out.println("Procedure #1 Info:");
		System.out.println();

		System.out.println("Procedure Name: " + p1.getPractitionerName());
		System.out.println();
		
		System.out.println("Procedure Date: " + p1.getProcedureDate());
		System.out.println();
		
		System.out.println("Practitioner: " + p1.getPractitionerName());
		System.out.println();
		
		System.out.println("Charge: " + p1.getBalance());
		System.out.println();
		System.out.println();
		
		
		System.out.println("Procedure #2 Info:");
		System.out.println();

		System.out.println("Procedure Name: " + p2.getPractitionerName());
		System.out.println();
		
		System.out.println("Procedure Date: " + p2.getProcedureDate());
		System.out.println();
		
		System.out.println("Practitioner: " + p2.getPractitionerName());
		System.out.println();
		
		System.out.println("Charge: " + p2.getBalance());
		System.out.println();
		System.out.println();
		
		
		System.out.println("Procedure #3 Info:");
		System.out.println();

		System.out.println("Procedure Name: " + p3.getPractitionerName());
		System.out.println();
		
		System.out.println("Procedure Date: " + p3.getProcedureDate());
		System.out.println();
		
		System.out.println("Practitioner: " + p3.getPractitionerName());
		System.out.println();
		
		System.out.println("Charge: " + p3.getBalance());
		System.out.println();
		System.out.println();
		
		
		System.out.printf("Total charges: $%.2f", p1.getBalance() + p2.getBalance() + p3.getBalance());

		

		



		
		
	}

}
