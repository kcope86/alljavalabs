package Ex11;

import java.text.DecimalFormat;
import java.util.Scanner;

public class AreaDriver {

	public static void main(String[] args) {
		DecimalFormat area = new DecimalFormat("###,##0.##");
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		do {
			System.out.println("Geometry Calculator\n");
			System.out.println("1. Calculate the area of a Circle");
			System.out.println("2. Calculate the area of a Rectangle");
			System.out.println("3. Calculate the area of a Triangle");
			System.out.println("4. Quit");
			choice = keyboard.nextInt();
			if (choice == 1) {
				double radius;
				do {
					System.out.print("What is the radius of the circle?");
					radius = keyboard.nextDouble();
					if (radius < 0) {
						System.out.println("radius may not be negative");
					}
				} while (radius < 0);
				System.out.println("The area of the circle is " + area.format(Geometry.AreaOfCircle(radius)));
			} else if (choice == 2) {
				double length;
				double width;
				do {
					System.out.print("What is the lenght of the rectangle?");
					length = keyboard.nextDouble();
					if (length < 0) {
						System.out.println("length may not be negative");
					}
				} while (length < 0);

				do {
					System.out.print("What is the width of the rectangle?");
					width = keyboard.nextDouble();
					if (width < 0) {
						System.out.println("width may not be negative");
					}
				} while (width < 0);
				System.out.println(
						"The area of the Rectangle is " + area.format(Geometry.AreaOfRectangle(length, width)));
			} else if (choice == 3) {
				double base;
				double height;
				do {
					System.out.print("What is the base of the Triangle?");
					base = keyboard.nextDouble();
					if (base < 0) {
						System.out.println("base may not be negative");
					}
				} while (base < 0);
				do {
					System.out.print("What is the height of the Triangle?");
					height = keyboard.nextDouble();
					if (height < 0) {
						System.out.println("height may not be negative");
					}
				} while (height < 0);
				System.out.println("The area of the triangle is " + area.format(Geometry.AreaOfTriangle(base, height)));
			}

		} while (choice != 4);
	}
}
