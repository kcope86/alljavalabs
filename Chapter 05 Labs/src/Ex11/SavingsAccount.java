package Ex11;

public class SavingsAccount {
	private double interestRate;
	private double balance;
	private double totalInterest = 0;
	private double totalDeposits = 0;
	private double totalWithdrawls = 0;

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public SavingsAccount(double balance) {
		this.balance = balance;
	}

	public void withdrawl(double ammount) {
		balance -= ammount;
	}

	public void deposit(double ammount) {
		balance += ammount;
	}

	public void addMonthlyInterest() {
		balance += ((interestRate / 12) * balance);
	}

	public double calcMonthlyInterest() {
		return ((interestRate / 12) * balance);
	}

	public void calcTotalWithdrawls(double ammount) {

		totalWithdrawls += ammount;
	}

	public void calcTotalDeposits(double ammount) {
		totalDeposits += ammount;
	}

	public void calcTotalInterest(double ammount) {
		totalInterest += ammount;
	}

	public double getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(double totalInterest) {
		this.totalInterest = totalInterest;
	}

	public double getTotalDeposits() {
		return totalDeposits;
	}

	public void setTotalDeposits(double totalDeposits) {
		this.totalDeposits = totalDeposits;
	}

	public double getTotalWithdrawls() {
		return totalWithdrawls;
	}

	public void setTotalWithdrawls(double totalWithdrawls) {
		this.totalWithdrawls = totalWithdrawls;
	}
}
