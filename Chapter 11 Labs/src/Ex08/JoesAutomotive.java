package Ex08;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JoesAutomotive extends Application {

	private Separator horizontal;
	private Separator horizontal2;

	private HBox promptHBox;
	private HBox routineHBox;
	private HBox line;
	private VBox nonRoutinePromptHBox;
	private HBox nonRoutineHBox;
	private HBox nonRoutineHBox2;
	private VBox first;
	private VBox second;
	private VBox third;

	private VBox finalize;

	private HBox line2;
	private VBox resultsVBox;

	private GridPane grid;

	private TextField hours;
	private TextField parts;

	private Label routinePrompt;
	private Label nonRoutinePrompt;
	private Label nonRoutinePrompt2;

	private Label prompt2;
	private Label prompt3;
	private Label receipt;
	private Label total;

	private Button b1;
	private Button b2;
	private Button b3;
	private Button b4;
	private Button b5;
	private Button b6;
	private Button b7;
	private Button submit;
	private Button reset;

	ScrollPane s1;

	private int laborHours;
	private double laborTotal;
	private double partsTotal;
	private double totalCharges = 0;

	private final double OIL_CHANGE = 35;
	private final double LUBE_JOB = 25;
	private final double RADIATOR_FLUSH = 50;
	private final double TRANSMISSION_FLUSH = 120;
	private final double INSPECTION = 35;
	private final double REPLACE_MUFF = 200;
	private final double TIRE_ROTATION = 20;
	private final double LABOR_COST = 60;

	private String fees = "Services rendered for this customer\n";

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		// Controls
		b1 = new Button("Oil Change");
		b2 = new Button("Lube Job");
		b3 = new Button("Radiator Flush");
		b4 = new Button("Transmission Flush");
		b5 = new Button("Inspection");
		b6 = new Button("Muffler Replacement");
		b7 = new Button("Tire Rotation");
		submit = new Button("Submit");
		reset = new Button("New Customer");
		hours = new TextField();
		hours.setPromptText("0");
		parts = new TextField();
		parts.setPromptText("0");
		routinePrompt = new Label("Click routine services needed");
		nonRoutinePrompt = new Label(
				"Fill out the parts total and labor hours then click submit for each non-routine service.");
		nonRoutinePrompt2 = new Label("Or click New Customer to start a new transaction.");

		prompt2 = new Label("How much were the parts: ");
		prompt3 = new Label("How many labor hours:      ");
		receipt = new Label(fees);
		total = new Label(String.format("Total: $%,.2f", totalCharges));
		s1 = new ScrollPane();
		s1.setPrefViewportWidth(200);
		s1.setPrefViewportHeight(200);
		s1.setContent(receipt);

		// Containers
		horizontal = new Separator();
		horizontal.setPrefWidth(650);
		horizontal2 = new Separator();
		horizontal2.setPrefWidth(650);

		promptHBox = new HBox(routinePrompt);
		promptHBox.setAlignment(Pos.BASELINE_LEFT);
		promptHBox.setPadding(new Insets(10));

		routineHBox = new HBox(b1, b2, b3, b4, b5, b6, b7);
		routineHBox.setAlignment(Pos.BASELINE_LEFT);
		routineHBox.setPadding(new Insets(10));

		line = new HBox(horizontal);

		nonRoutinePromptHBox = new VBox(nonRoutinePrompt, nonRoutinePrompt2);
		nonRoutinePromptHBox.setAlignment(Pos.BASELINE_LEFT);
		nonRoutinePromptHBox.setPadding(new Insets(10));
		nonRoutineHBox = new HBox(prompt2, parts);
		nonRoutineHBox.setAlignment(Pos.BASELINE_LEFT);
		nonRoutineHBox.setPadding(new Insets(20));

		nonRoutineHBox2 = new HBox(prompt3, hours);
		nonRoutineHBox2.setAlignment(Pos.BASELINE_LEFT);
		nonRoutineHBox2.setPadding(new Insets(20));

		line2 = new HBox(horizontal2);

		resultsVBox = new VBox(30, s1, total);
		resultsVBox.setAlignment(Pos.BASELINE_LEFT);
		resultsVBox.setPadding(new Insets(20));
		finalize = new VBox(30, submit);
		finalize.setAlignment(Pos.BASELINE_LEFT);
		finalize.setPadding(new Insets(20));

		first = new VBox(promptHBox, routineHBox);
		second = new VBox(line, nonRoutinePromptHBox, nonRoutineHBox, nonRoutineHBox2, finalize, line2);
		third = new VBox(resultsVBox, reset);

		grid = new GridPane();
		grid.add(first, 0, 0);
		grid.add(second, 0, 1);
		grid.add(third, 0, 2);
		// grid.add(reset, 1, 3);

		Scene scene = new Scene(grid, 650, 600);
		primaryStage.setScene(scene);
		primaryStage.show();

		b1.setOnAction(e -> {
			fees += "Oil Change: $35.00\n";
			totalCharges += OIL_CHANGE;
			receipt.setText(fees);
			total.setText(String.format("Total: $%,.2f", totalCharges));
			s1.vvalueProperty().bind(receipt.heightProperty());
		});
		b2.setOnAction(e -> {
			fees += "Lube Job: $25.00\n";
			totalCharges += LUBE_JOB;
			receipt.setText(fees);
			total.setText(String.format("Total: $%,.2f", totalCharges));

			s1.vvalueProperty().bind(receipt.heightProperty());

		});
		b3.setOnAction(e -> {
			fees += "Radiator Flush $50.00\n";
			totalCharges += RADIATOR_FLUSH;
			receipt.setText(fees);
			total.setText(String.format("Total: $%,.2f", totalCharges));
			s1.vvalueProperty().bind(receipt.heightProperty());

		});
		b4.setOnAction(e -> {
			fees += "Transmission Flush: $120.00\n";
			totalCharges += TRANSMISSION_FLUSH;
			receipt.setText(fees);
			total.setText(String.format("Total: $%,.2f", totalCharges));
			s1.vvalueProperty().bind(receipt.heightProperty());

		});
		b5.setOnAction(e -> {
			fees += "Inspection: $35.00\n";
			totalCharges += INSPECTION;
			receipt.setText(fees);
			total.setText(String.format("Total: $%,.2f", totalCharges));
			s1.vvalueProperty().bind(receipt.heightProperty());

		});
		b6.setOnAction(e -> {
			fees += "Muffler Replacement: $200.00\n";
			totalCharges += REPLACE_MUFF;
			receipt.setText(fees);
			total.setText(String.format("Total: $%,.2f", totalCharges));
			s1.vvalueProperty().bind(receipt.heightProperty());

		});
		b7.setOnAction(e -> {
			fees += "Tire Rotation: $20.00\n";
			totalCharges += TIRE_ROTATION;
			receipt.setText(fees);
			total.setText(String.format("Total: $%,.2f", totalCharges));
			s1.vvalueProperty().bind(receipt.heightProperty());

		});
		submit.setOnAction(e -> {
			boolean isValid = true;
			parts.setStyle("-fx-text-inner-color: black;");
			hours.setStyle("-fx-text-inner-color: black;");
			try {
				partsTotal = Double.parseDouble(parts.getText());
			} catch (NumberFormatException ex) {
				parts.setStyle("-fx-text-inner-color: red;");
				parts.setText("Invalid Entry");
				isValid = false;
			}
			try {
				laborHours = Integer.parseInt(hours.getText());
			} catch (NumberFormatException ex) {
				hours.setStyle("-fx-text-inner-color: red;");
				hours.setText("Invalid Entry");
				isValid = false;
			}

			if (isValid == true) {
				laborTotal = laborHours * LABOR_COST;
				fees += String.format("Non Routine Maintenance:\n" + "Parts: $%,.2f\n" + "Labor: $%,.2f\n", partsTotal,
						laborTotal);
				totalCharges += partsTotal;
				totalCharges += laborTotal;
				receipt.setText(fees);
				total.setText(String.format("Total: $%,.2f", totalCharges));
				parts.clear();
				hours.clear();

				s1.vvalueProperty().bind(receipt.heightProperty());

			}

		});
		reset.setOnAction(e -> {
			fees = "Services rendered for this customer\n";
			totalCharges = 0;
			receipt.setText(fees);
			total.setText(String.format("Total: $%,.2f", totalCharges));
			hours.clear();
			parts.clear();

			s1.vvalueProperty().bind(receipt.heightProperty());
		});

	}

}
