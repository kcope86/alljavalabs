package Ex06;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileArray {

	public static void writeArray(String file, int[] array) throws IOException {
		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream(file));
		for (int i = 0; i < array.length; i++) {
			outputFile.writeInt(array[i]);

		}
		outputFile.close();
	}

	/*
	 * public static void calcArraySize(File f1) throws IOException { BufferedReader
	 * reader = new BufferedReader(new FileReader(f1)); int lines = 0; while
	 * (reader.readLine() != null) lines++; arraySize = lines; reader.close(); }
	 */

	public static int[] readArray(String file, int[] array) throws IOException {
		boolean endOfFile = false;
		int i = 0;
		DataInputStream inputFile = new DataInputStream(new FileInputStream(file));

		while (!endOfFile) {

			try {

				try {
					array[i] = inputFile.readInt();
					++i;
				} catch (EOFException ex) {
					endOfFile = true;
				}
			} catch (IOException ex) {
				ex.toString();
			}

		}
		inputFile.close();
		return array;
	}
}
