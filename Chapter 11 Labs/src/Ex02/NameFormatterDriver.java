package Ex02;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NameFormatterDriver extends Application {

	Label promptLbl;
	Label outputLbl = new Label("");

	TextField firstName;
	TextField middleName;
	TextField lastName;
	TextField titleName;

	private String fName;
	private String mName;
	private String lName;
	private String title;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {

		primaryStage.setTitle("Personal Information.");

		firstName = new TextField();
		firstName.setPromptText("Enter your first name");

		middleName = new TextField();
		middleName.setPromptText("Enter your middle name");

		lastName = new TextField();
		lastName.setPromptText("Enter your last name");

		titleName = new TextField();
		titleName.setPromptText("Enter your title");

		promptLbl = new Label("How would you like to see your information");
		Button b1 = new Button("Title, First, Middle, Last");
		Button b2 = new Button(" First, Middle, Last");
		Button b3 = new Button("First, Last");
		Button b4 = new Button("Last, First, Middle, Title");
		Button b5 = new Button("Last, First, Middle,");
		Button b6 = new Button("Last, First");

		VBox vBox = new VBox(10, firstName, middleName, lastName, titleName, promptLbl, b1, b2, b3, b4, b5, b6,
				outputLbl);
		vBox.setPadding(new Insets(50));
		vBox.setAlignment(Pos.CENTER);
		Scene scene = new Scene(vBox, 400, 400);
		primaryStage.setScene(scene);
		primaryStage.show();

		b1.setOnAction(new LayoutBtn1());
		b2.setOnAction(new LayoutBtn2());
		b3.setOnAction(new LayoutBtn3());
		b4.setOnAction(new LayoutBtn4());
		b5.setOnAction(new LayoutBtn5());
		b6.setOnAction(new LayoutBtn6());

	}

	class LayoutBtn1 implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			fName = firstName.getText();
			mName = middleName.getText();
			lName = lastName.getText();
			title = titleName.getText();
			outputLbl.setText(title + " " + fName + " " + mName + " " + lName);

		}
	}

	class LayoutBtn2 implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			fName = firstName.getText();
			mName = middleName.getText();
			lName = lastName.getText();
			title = titleName.getText();
			outputLbl.setText(fName + " " + mName + " " + lName);

		}
	}

	class LayoutBtn3 implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			fName = firstName.getText();
			mName = middleName.getText();
			lName = lastName.getText();
			title = titleName.getText();
			outputLbl.setText(fName + " " + lName);

		}
	}

	class LayoutBtn4 implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			fName = firstName.getText();
			mName = middleName.getText();
			lName = lastName.getText();
			title = titleName.getText();
			outputLbl.setText(lName + " " + fName + " " + mName + " " + title);
		}
	}

	class LayoutBtn5 implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			fName = firstName.getText();
			mName = middleName.getText();
			lName = lastName.getText();
			title = titleName.getText();
			outputLbl.setText(lName + " " + fName + " " + mName);
		}
	}

	class LayoutBtn6 implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			fName = firstName.getText();
			mName = middleName.getText();
			lName = lastName.getText();
			title = titleName.getText();
			outputLbl.setText(lName + " " + fName);
		}
	}

}