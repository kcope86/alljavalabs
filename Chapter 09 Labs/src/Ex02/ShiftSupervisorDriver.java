package Ex02;

import java.util.Scanner;

public class ShiftSupervisorDriver {

	public static void main(String[] args) {
		String name;
		String empNumber = "XXX-X";
		String input;
		String hireDate;
		double bonus;
		double salary;

		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter the employee name: ");
		name = keyboard.nextLine();
		System.out.println();
		do {
			System.out.print("Enter employee number: ");
			input = keyboard.nextLine().toUpperCase();
			if (Employee.isValid(input)) {
				empNumber = input;
			} else {
				System.out.println("Invalid format for employee number.");
			}

		} while (!Employee.isValid(input));
		System.out.print("Enter Date Hired: ");
		hireDate = keyboard.nextLine();
		System.out.print("Enter salary: ");
		salary = keyboard.nextDouble();
		System.out.print("Enter yearly bonus: ");
		bonus = keyboard.nextDouble();

		System.out.println();
		ShiftSupervisor worker1 = new ShiftSupervisor();
		worker1.setName(name);
		worker1.setEmployeeNumber(empNumber);
		worker1.setHireDate(hireDate);
		worker1.setSalary(salary);
		worker1.setBonus(bonus);

		System.out.println("Using Mutators");
		System.out.println(worker1.toString());
		System.out.println();

		ShiftSupervisor worker2 = new ShiftSupervisor(name, empNumber, hireDate, salary, bonus);
		System.out.println("Using constructor with fields");
		System.out.println(worker2.toString());
	}

}
