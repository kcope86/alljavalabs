package Ex02;

public class LibraryDriver {

	public static void main(String[] args) {

		Book book1 = new Book("War and Peace", 1869, "Leo Tolstoy");
		Library stl = new Library(book1, "St. Louis Public Library");

		System.out.println(stl.toString());
	}

}
