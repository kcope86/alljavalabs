package Ex10;

public class Temperature {
	private double temp;

	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}

	public Temperature() {

	}

	public boolean isEthylFreezing(double temp) {
		if (temp <= -173) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isEthylBoiling(double temp) {
		if (temp >= 172) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isOxygenFreezing(double temp) {
		if (temp <= -362) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isOxygenBoiling(double temp) {
		if (temp >= -306) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isWaterFreezing(double temp) {
		if (temp <= 32) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isWaterBoiling(double temp) {
		if (temp >= 212) {
			return true;
		} else {
			return false;
		}
	}
}
