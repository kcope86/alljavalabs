package Ex16;

import java.util.Scanner;

public class MagicDateDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int month;
		int day;
		int year;

		System.out.print("Enter the day of the month: ");
		day = keyboard.nextInt();
		System.out.print("Enter the month of the year: ");
		month = keyboard.nextInt();
		System.out.print("Enter thelast two digits of the year: ");
		year = keyboard.nextInt();

		MagicDate md1 = new MagicDate(day, month, year);
		if (md1.isMagic() == true) {
			System.out.printf("%d/%d/%d is a magic date.", md1.getDay(), md1.getMonth(), md1.getYear());

		}
		if (md1.isMagic() == false) {
			System.out.printf("%d/%d/%d is not a magic date.", md1.getDay(), md1.getMonth(), md1.getYear());

		}

	}

}
