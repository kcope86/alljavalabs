public class EnergyDrinkConsumption
{
	public static void main(String[]args)
	{
			final int SURVEYED = 15000;
			final float ENERGY_DRINKERS = SURVEYED * .18f;
			final float CITRUS = ENERGY_DRINKERS * .58f;
			
			
			
			System.out.printf("Out of 15,000 people %.2f Buy at least one energy drink per week \n", ENERGY_DRINKERS);
			System.out.printf("Out of %.2f people, %.2f prefer citrus flavored energy drinks. \n", ENERGY_DRINKERS, CITRUS);
			
			
	}
}