package Ex15;

import java.io.IOException;

public class UpperCaseFileDriver {

	public static void main(String[] args) throws IOException {
		UpperCaseFile lyrics = new UpperCaseFile("DarkestHour.txt", "DarkestHourUpper.txt");
		lyrics.writeToUpper();
	}

}
