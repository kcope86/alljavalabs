package Ex03;

public class InvalidQuantityException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidQuantityException(String msg) {
		super(msg);
	}

}
