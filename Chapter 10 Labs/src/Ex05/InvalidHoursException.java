package Ex05;

public class InvalidHoursException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidHoursException(String msg) {
		super(msg);
	}

}
