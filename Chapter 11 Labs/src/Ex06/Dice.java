package Ex06;

import java.util.Random;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Dice extends Application {

	ImageView die1;
	ImageView die2;

	Image sideOne;
	Image sideTwo;
	Image sideThree;
	Image sideFour;
	Image sideFive;
	Image sideSix;
	Button roll;

	int num1;
	int num2;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		roll = new Button("Roll the dice!");
		sideOne = new Image("file:die1.png");
		sideTwo = new Image("file:die2.png");
		sideThree = new Image("file:die3.png");
		sideFour = new Image("file:die4.png");
		sideFive = new Image("file:die5.png");
		sideSix = new Image("file:die6.png");

		die1 = new ImageView();
		die2 = new ImageView();

		GridPane grid = new GridPane();
		grid.add(roll, 0, 0);
		grid.add(die1, 0, 1);
		grid.add(die2, 1, 1);

		Scene scene = new Scene(grid, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.show();

		roll.setOnAction(e -> {

			Random rand;
			rand = new Random();
			num1 = rand.nextInt(6) + 1;
			num2 = rand.nextInt(6) + 1;
			switch (num1) {

			case 1:
				die1.setImage(sideOne);
				break;
			case 2:
				die1.setImage(sideTwo);
				break;
			case 3:
				die1.setImage(sideThree);
				break;
			case 4:
				die1.setImage(sideFour);
				break;
			case 5:
				die1.setImage(sideFive);
				break;
			case 6:
				die1.setImage(sideSix);
				break;

			}

			switch (num2) {

			case 1:
				die2.setImage(sideOne);
				break;
			case 2:
				die2.setImage(sideTwo);
				break;
			case 3:
				die2.setImage(sideThree);
				break;
			case 4:
				die2.setImage(sideFour);
				break;
			case 5:
				die2.setImage(sideFive);
				break;
			case 6:
				die2.setImage(sideSix);
				break;

			}

		});

	}

}
