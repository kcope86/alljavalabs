package Ex01;

public class ProductionWorker extends Employee {

	private int shift;
	private double payRate;
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;

	public int getShift() {
		return shift;
	}

	public void setShift(int shift) {
		this.shift = shift;
	}

	public double getPayRate() {
		return payRate;
	}

	public void setPayRate(double payRate) {
		this.payRate = payRate;
	}

	public ProductionWorker(String name, String employeeNumber, String hireDate, int shift, double payRate) {
		super(name, employeeNumber, hireDate);
		this.shift = shift;
		this.payRate = payRate;
	}

	@Override
	public String toString() {
		return String.format(
				"Employee Name: %s\n" + "Employee Number: %s\n" + "Employee Hire Date: %s\n" + "Employee Shift: %s\n"
						+ "Employee Pay Rate: %.2f\n",
				this.getName(), this.getEmployeeNumber(), this.getHireDate(), this.getShift(), this.getPayRate());

	}

	public ProductionWorker() {

	}

}
