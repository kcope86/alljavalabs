package Ex01;

public class StudentDriver {

	public static void main(String[] args) {

		// Instantiate a new Student() object.
		Student s1 = new Student();

		// Set new Student() id. Call to the mutator method
		s1.setId(5);
		s1.setStudentName("Kevin");

		// Call to the accessor
		System.out.println(s1.getId());
		System.out.println(s1.getName());
		System.out.println(s1.getGpa());

	}

}
