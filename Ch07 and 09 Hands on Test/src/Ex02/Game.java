package Ex02;

public class Game {

	// fields
	String gameName;
	int maxPlayers;

	// getters and setters (Accessors/Mutators)
	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	// constructor with args
	public Game(String gameName, int maxPlayers) {
		this.gameName = gameName;
		this.maxPlayers = maxPlayers;
	}

	// no-arg constructor
	public Game() {
	}

	// toString method
	public String toString() {
		String str;
		str = String.format("Game Name: %s\n" + "Max Players %d", gameName, maxPlayers);
		return str;

	}

}
