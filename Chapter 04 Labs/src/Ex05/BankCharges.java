package Ex05;

public class BankCharges {
	private final double MONTHLY_FEE = 10;
	private double checkFees;
	private double serviceFees;
	private double balance;
	private double numOfChecks;

	public double getBalance() {
		return balance;
	}

	public double getServiceFees() {

		return serviceFees;
	}

	public double getChecks() {
		return numOfChecks;
	}

	public BankCharges(double balance, double checks) {
		this.balance = balance;
		numOfChecks = checks;
	}

	public BankCharges() {
		balance = 0;
		numOfChecks = 0;
	}

	public double applyMonthlyFee() {
		balance -= MONTHLY_FEE;
		serviceFees += MONTHLY_FEE;
		return balance;

	}

	public double applyLowFundsFee() {
		serviceFees += 15;
		balance -= 15;
		return balance;

	}

	public double calcCheckFees() {
		if (numOfChecks >= 60) {
			checkFees = numOfChecks * 0.04;
			serviceFees += numOfChecks * 0.04;
		} else if (numOfChecks >= 40 && numOfChecks <= 59) {
			checkFees = numOfChecks * 0.06;
			serviceFees += numOfChecks * 0.06;
		} else if (numOfChecks >= 20 && numOfChecks <= 39) {
			checkFees = numOfChecks * 0.08;
			serviceFees += numOfChecks * 0.08;
		} else if (numOfChecks >= 0 && numOfChecks <= 20) {
			checkFees = numOfChecks * 0.10;
			serviceFees += numOfChecks * 0.10;
		}
		return checkFees;
	}

}
