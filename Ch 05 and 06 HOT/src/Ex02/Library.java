package Ex02;

public class Library {
	private Book book;
	private String name;

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return "Library name: " + name + "\n" + "Book Title:   " + book.getName() + "\n" + "Book Author:  "
				+ book.getAuthor() + "\n" + "Book publish date: " + book.getPublished() + "\n";
	}

	public Library(Book book, String name) {

		this.book = book;
		book.setAuthor("aklsdjf");
		this.name = name;
	}
}
