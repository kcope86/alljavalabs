package Ex03;

import java.util.Scanner;

public class BankAccountDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		int[] bankAccount = new int[] { 5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 8080152, 4562555, 5552012,
				5050552, 7825887, 1250255, 1005231, 6545231, 3852085, 7576551, 7881200, 4581002 };

		BankAccount.selectionSort(bankAccount);
		System.out.print("Please enter your account number: ");
		int idnum = keyboard.nextInt();

		if (BankAccount.accountSearch(bankAccount, idnum) == -1) {
			System.out.print("Account number not found");

		} else {
			System.out.println(bankAccount[BankAccount.accountSearch(bankAccount, idnum)] + " Is valid.");
		}

	}

}
