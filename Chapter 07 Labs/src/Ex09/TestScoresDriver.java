package Ex09;

import java.util.Scanner;

public class TestScoresDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		String[] names = new String[5];
		double testScore;
		double[][] scores = new double[5][4];
		int choice = 0;
		String input;
		char repeat;
		int index = 0;

		System.out.print("Enter the name for the first Student: ");
		names[index] = keyboard.nextLine();

		System.out.println();

		for (int i = 0; i < 4; i++) {
			do {
				System.out.print("Enter score for test " + (i + 1) + ": \n");
				testScore = keyboard.nextDouble();
				if (testScore < 0 || testScore > 100) {
					System.out.println("test score cannot be less than 0 or more than 100");
				}
			} while (testScore < 0 || testScore > 100);
			scores[index][i] = testScore;

		}
		keyboard.nextLine();
		index++;
		System.out.print("Enter the name for the second Student: ");
		names[index] = keyboard.nextLine();

		for (int i = 0; i < 4; i++) {
			do {
				System.out.print("Enter score for test " + (i + 1) + ": \n");
				testScore = keyboard.nextDouble();
				if (testScore < 0 || testScore > 100) {
					System.out.println("test score cannot be less than 0 or more than 100");
				}
			} while (testScore < 0 || testScore > 100);
			scores[index][i] = testScore;

		}
		keyboard.nextLine();

		index++;

		System.out.print("Enter the name for the third Student: ");
		names[index] = keyboard.nextLine();

		for (int i = 0; i < 4; i++) {
			do {
				System.out.print("Enter score for test " + (i + 1) + ": \n");
				testScore = keyboard.nextDouble();
				if (testScore < 0 || testScore > 100) {
					System.out.println("test score cannot be less than 0 or more than 100");
				}
			} while (testScore < 0 || testScore > 100);
			scores[index][i] = testScore;

		}
		keyboard.nextLine();

		index++;

		System.out.print("Enter the name for the fourth Student: ");
		names[index] = keyboard.nextLine();

		for (int i = 0; i < 4; i++) {
			do {
				System.out.print("Enter score for test " + (i + 1) + ": \n");
				testScore = keyboard.nextDouble();
				if (testScore < 0 || testScore > 100) {
					System.out.println("test score cannot be less than 0 or more than 100");
				}
			} while (testScore < 0 || testScore > 100);
			scores[index][i] = testScore;

		}
		keyboard.nextLine();

		index++;

		System.out.print("Enter the name for the fifth Student: ");
		names[index] = keyboard.nextLine();

		for (int i = 0; i < 4; i++) {
			do {
				System.out.print("Enter score for test " + (i + 1) + ": \n");
				testScore = keyboard.nextDouble();
				if (testScore < 0 || testScore > 100) {
					System.out.println("test score cannot be less than 0 or more than 100");
				}
			} while (testScore < 0 || testScore > 100);
			scores[index][i] = testScore;

		}
		keyboard.nextLine();

		index++;

		TestScores exam1 = new TestScores(names, scores);
		do {
			do {
				System.out.println("Whos results would you like to see?");

				for (int i = 0; i < names.length; i++) {
					System.out.println("Press " + (i + 1) + " for " + exam1.getName(i));

				}
				choice = keyboard.nextInt();
				keyboard.nextLine();
				if (choice > 5 || choice < 1)
					;
			} while (choice > 5 || choice < 1);

			int student = choice - 1;
			System.out.println("The results for " + exam1.getName(student) + " are as follows: ");
			System.out.println("Average Test Score: " + exam1.getAverage(student));
			System.out.println("Letter Grade: " + exam1.getLetterGrade(student));
			System.out.println();
			System.out.print("Would you like to see another students results? (Y for Yes, N for No): ");
			input = keyboard.nextLine().toUpperCase();

			repeat = input.charAt(0);
			// keyboard.nextLine();

		} while (repeat != 'N');
	}

}
