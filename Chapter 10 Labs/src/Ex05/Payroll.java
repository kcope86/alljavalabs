package Ex05;

public class Payroll {
	String name;
	int idNumber;
	double hourlyRate;
	double hoursWorked;

	public String getName() {
		return name;
	}

	public int getIdNumber() {
		return idNumber;
	}

	public double getHourly() {
		return hourlyRate;
	}

	public double getHours() {
		return hoursWorked;
	}

	public void setName(String name) throws InvalidNameException {
		if (name.equals("")) {
			throw new InvalidNameException("Name cannot be blank.");
		}
		this.name = name;

	}

	public void setIdNumber(int id) throws InvalidIdException {
		if (id < 1) {
			throw new InvalidIdException("ID number must be greater than 0.");
		}
		idNumber = id;

	}

	public void setHourlyRate(double rate) throws InvalidHourlyPayException {
		if (rate < 0 || rate > 25)
			throw new InvalidHourlyPayException("Hourly pay rate cannot be less than 0, or greater than 25.");
		hourlyRate = rate;
	}

	public void setHoursWorked(double hours) throws InvalidHoursException {
		if (hours < 0 || hours > 84)
			throw new InvalidHoursException("Hours worked cannot be less than 0 or greater than 84.");
		hoursWorked = hours;
	}

	public double calcGrossPay(double hours, double rate) {
		double grossPay = hours * rate;
		return grossPay;
	}

}
