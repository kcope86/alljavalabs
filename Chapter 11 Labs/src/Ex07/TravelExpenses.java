package Ex07;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TravelExpenses extends Application {
	TextField days;
	TextField airfare;
	TextField carRental;
	TextField milesDriven;
	TextField parkingFees;
	TextField taxiFees;
	TextField registrationFees;
	TextField lodging;
	Button submit;
	Label totalCharges;
	Label over;
	Label under;
	Label l1;
	Label l2;
	Label l3;
	Label l4;
	Label l5;
	Label l6;
	Label l7;
	Label l8;
	Label perDiem;

	VBox vBox1;
	VBox vBox2;
	VBox vBox3;
	VBox vBox4;
	GridPane grid;

	private double totalAllowance;
	private double totalFees;
	private double totalAirfare;
	private double totalCarRent;
	private int totalMiles;
	private double totalParking;
	private double totalTaxi;
	private double totalRegistration;
	private double totalLodging;
	private int numDays;

	private final double MEALS = 47;
	private final double PARKING = 20;
	private final double TAXI = 40;
	private final double LODGING = 195;
	private final double MILES = .42;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Travel Expense Calculator");
		days = new TextField();
		days.setText("0");
		airfare = new TextField();
		airfare.setText("0");
		carRental = new TextField();
		carRental.setText("0");
		milesDriven = new TextField();
		milesDriven.setText("0");
		parkingFees = new TextField();
		parkingFees.setText("0");
		taxiFees = new TextField();
		taxiFees.setText("0");
		registrationFees = new TextField();
		registrationFees.setText("0");
		lodging = new TextField();
		lodging.setText("0");
		submit = new Button("Submit");

		l1 = new Label("How many days were your trip:");
		l2 = new Label("Cost of airfare:");
		l3 = new Label("Cost of Car Rental (if any):");
		l4 = new Label("Miles driven(if private vehicle was used):");
		l5 = new Label("Parking fees (if any):");
		l6 = new Label("Taxi charges (if any):");
		l7 = new Label("Registration fees (if any):");
		l8 = new Label("Lodging per night:");
		totalCharges = new Label("Total Charges: ");
		perDiem = new Label("Total Allowed: ");
		over = new Label("Deficit: ");
		under = new Label("Surplus: ");

		vBox1 = new VBox(14, l1, l2, l3, l4, l5, l6, l7, l8, submit);
		vBox2 = new VBox(5, days, airfare, carRental, milesDriven, parkingFees, taxiFees, registrationFees, lodging);
		vBox3 = new VBox(10, totalCharges, perDiem, over, under);

		vBox1.setPadding(new Insets(50));
		vBox1.setAlignment(Pos.BASELINE_LEFT);
		vBox2.setPadding(new Insets(50));
		vBox2.setAlignment(Pos.BASELINE_LEFT);
		vBox3.setPadding(new Insets(50));
		vBox3.setAlignment(Pos.BASELINE_LEFT);

		grid = new GridPane();
		grid.add(vBox1, 0, 0);
		grid.add(vBox2, 1, 0);
		grid.add(vBox3, 0, 1);

		// grid.add(results, 0, 2);

		Scene scene = new Scene(grid, 600, 600);
		primaryStage.setScene(scene);
		primaryStage.show();

		submit.setOnAction(e -> {
			boolean isValid;

			isValid = true;
			days.setStyle("-fx-text-inner-color: black;");
			airfare.setStyle("-fx-text-inner-color: black;");
			carRental.setStyle("-fx-text-inner-color: black;");
			milesDriven.setStyle("-fx-text-inner-color: black;");
			parkingFees.setStyle("-fx-text-inner-color: black;");
			taxiFees.setStyle("-fx-text-inner-color: black;");
			registrationFees.setStyle("-fx-text-inner-color: black;");
			lodging.setStyle("-fx-text-inner-color: black;");

			try {
				numDays = Integer.parseInt(days.getText());
			} catch (NumberFormatException ex) {
				days.setStyle("-fx-text-inner-color: red;");

				days.setText("not a number");
				isValid = false;

			}
			try {
				totalAirfare = Double.parseDouble(airfare.getText());

			} catch (NumberFormatException ex) {
				airfare.setStyle("-fx-text-inner-color: red;");

				airfare.setText("not a number");
				isValid = false;

			}
			try {
				totalCarRent = Double.parseDouble(carRental.getText());

			} catch (NumberFormatException ex) {
				carRental.setStyle("-fx-text-inner-color: red;");

				carRental.setText("not a number");
				isValid = false;

			}
			try {
				totalMiles = Integer.parseInt(milesDriven.getText());

			} catch (NumberFormatException ex) {
				milesDriven.setStyle("-fx-text-inner-color: red;");

				milesDriven.setText("not a number");
				isValid = false;

			}
			try {
				totalParking = Double.parseDouble(parkingFees.getText());

			} catch (NumberFormatException ex) {
				parkingFees.setStyle("-fx-text-inner-color: red;");

				parkingFees.setText("not a number");
				isValid = false;

			}
			try {
				totalTaxi = Double.parseDouble(taxiFees.getText());

			} catch (NumberFormatException ex) {
				taxiFees.setStyle("-fx-text-inner-color: red;");

				taxiFees.setText("not a number");
				isValid = false;

			}
			try {
				totalRegistration = Double.parseDouble(registrationFees.getText());

			} catch (NumberFormatException ex) {
				registrationFees.setStyle("-fx-text-inner-color: red;");
				registrationFees.setText("not a number");
				isValid = false;

			}
			try {
				totalLodging = Double.parseDouble(lodging.getText());

			} catch (NumberFormatException ex) {
				lodging.setStyle("-fx-text-inner-color: red;");

				lodging.setText("not a number");
				isValid = false;

			}

			if (isValid == true) {
				totalAllowance = (MEALS * numDays) + (PARKING * numDays) + (TAXI * numDays) + (LODGING * numDays)
						+ (MILES * totalMiles);
				totalFees = totalAirfare + totalCarRent + totalParking + totalTaxi + totalRegistration
						+ (totalLodging * numDays);
				perDiem.setText(String.format("Total Allowed: $%,.2f", totalAllowance));
				totalCharges.setText(String.format("Total Charges: $%,.2f", totalFees));
				if (totalAllowance > totalFees) {
					double deficit = totalAllowance - totalFees;
					under.setText(String.format("Surplus: $%,.2f", deficit));
					over.setText("No deficit");
				}
				if (totalAllowance < totalFees) {
					double surplus = totalFees - totalAllowance;
					over.setText(String.format("Surplus: $%,.2f", surplus));
					under.setText("No surplus");
				}
			} else {
				perDiem.setText("Check above for invalid entries");
				totalCharges.setText("Check above for invalid entries");
				under.setText("Check above for invalid entries");
				over.setText("Check above for invalid entries");
			}
		});

	}

}
