package Ex09;

import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int employeeID;
		double grossPay = 0;
		double stateTax = 0;
		double federalTax = 0;
		double ficaWithHeld = 0;
		double totalTaxes;

		do {
			do {

				System.out.print("Enter Your employee ID: ");
				employeeID = keyboard.nextInt();

			} while (employeeID < 0);
			if (employeeID == 0) {
				break;
			}
			do {
				totalTaxes = 0;
				do {
					System.out.print("Enter Your gross pay: ");
					grossPay = keyboard.nextInt();
				} while (grossPay < 0);

				do {
					System.out.print("Enter Your state tax: ");
					stateTax = keyboard.nextInt();
					if (stateTax > grossPay || stateTax < 0) {
						System.out.println("State tax cannot be larger than gross pay or less than 0.");
					}
				} while (stateTax < 0 || stateTax > grossPay);
				totalTaxes += stateTax;
				do {
					System.out.print("Enter Your federal tax: ");
					federalTax = keyboard.nextInt();
					if (federalTax > grossPay || federalTax < 0) {
						System.out.println("Federal tax cannot be larger than gross pay or less than 0.");
					}
				} while (federalTax < 0 || federalTax > grossPay);
				totalTaxes += federalTax;
				do {
					System.out.print("Enter Your FICA withholdings: ");
					ficaWithHeld = keyboard.nextInt();
					if (ficaWithHeld > grossPay || ficaWithHeld < 0) {
						System.out.println("FICA Withholdings cannot be larger than gross pay or less than 0.");
					}
				} while (ficaWithHeld < 0 || ficaWithHeld > grossPay);
				totalTaxes += ficaWithHeld;
				if (totalTaxes > grossPay) {
					System.out.println("Taxes cannot be more than gross pay.");
				}
			} while (totalTaxes > grossPay);
			Payroll emp1 = new Payroll(employeeID, grossPay, stateTax, federalTax, ficaWithHeld);
			System.out.printf("Net pay for employee number %d---$%,.2f\n", emp1.getEmployeeID(), emp1.calcNetPay());
		} while (employeeID != 0);
		System.out.println("Thank you for using the payroll program.");

	}

}
