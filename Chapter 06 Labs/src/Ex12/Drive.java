package Ex12;

import java.util.Scanner;

public class Drive {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		FuelGauge g1 = new FuelGauge();
		Odometer o1 = new Odometer();
		String input;
		char repeat;
		do {
			g1.fillUp();
			do {

				o1.addMiles();
				// System.out.println("You have gone " + o1.getMileage() + " miles");
				o1.decreaseGas(g1);
				// System.out.println("You have " + g1.getFuelLevel() + " gallons of fuel
				// left");

			} while (g1.hasFuel());
			System.out.println("You have gone " + o1.getMileage());
			System.out.println("You are out of fuel");
			System.out.print("Type D to fill up and drive or Q to quit");
			input = keyboard.nextLine().toUpperCase();
			repeat = input.charAt(0);
		} while (repeat != 'Q');
	}

}
