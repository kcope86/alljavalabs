package Ex08;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;

public class FileDecryptionDriver {

	public static void main(String[] args) throws IOException {

		FileDecryption.decrypt("encryptedData.dat");
		System.out.println("Your data has been decrypted.");

		DataInputStream inputFile = new DataInputStream(new FileInputStream("decryptedData.dat"));
		boolean endOfFile = false;
		while (!endOfFile) {
			try {
				System.out.println(inputFile.readUTF());

			} catch (EOFException ex) {
				endOfFile = true;

			}

		}

	}

}
