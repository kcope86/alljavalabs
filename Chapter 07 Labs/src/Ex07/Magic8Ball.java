package Ex07;

import java.util.Random;

public class Magic8Ball {

	private String[] eightBall;

	public Magic8Ball(String[] response) {
		eightBall = new String[response.length];
		for (int i = 0; i < response.length; i++) {
			eightBall[i] = response[i];
		}
	}

	public String getFortune() {
		Random rand = new Random();

		int n = rand.nextInt(10);
		return eightBall[n];
	}
}
