package Ex10;

import java.util.Scanner;

public class TemperatureDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		double temp;
		Temperature temp1 = new Temperature();
		System.out.print("Enter a temperature: ");
		temp = keyboard.nextDouble();
		temp1.setTemp(temp);

		if (temp1.isEthylFreezing(temp) == true) {
			System.out.printf("Ethyl is frozen at %.2f\n", temp);
		}
		if (temp1.isEthylBoiling(temp) == true) {
			System.out.printf("Ethyl is boiling at %.2f\n", temp);
		}

		if (temp1.isOxygenFreezing(temp) == true) {
			System.out.printf("Ocygen is frozen at %.2f\n", temp);
		}
		if (temp1.isOxygenBoiling(temp) == true) {
			System.out.printf("Oxygen is boiling at %.2f\n", temp);
		}

		if (temp1.isWaterFreezing(temp) == true) {
			System.out.printf("Water is frozen at %.2f\n", temp);
		}
		if (temp1.isWaterBoiling(temp) == true) {
			System.out.printf("Water is boiling at %.2f\n", temp);
		}

	}

}
