package Ex13;

import java.util.Scanner;

public class GameDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter name for player 1: ");
		Player player1 = new Player(keyboard.nextLine());
		System.out.print("Enter name for player 2: ");
		Player player2 = new Player(keyboard.nextLine());
		Die die1 = new Die(6);
		Die die2 = new Die(6);
		String input;
		char roll;

		do {
			System.out.println(player1.getName() + "'s turn");
			die1.roll();
			die2.roll();
			System.out.println();

			System.out.println(player1.getName() + "'s score is " + player1.getScore());
			System.out.println();

			System.out.println(player1.getName() + " rolled " + (die1.getValue() + die2.getValue()) + " points");
			player1.calcScore(die1.getValue(), die2.getValue());
			System.out.println(player1.getName() + "'s new score is " + player1.getScore());
			System.out.println();

			System.out.println(player2.getName() + "'s turn");
			die1.roll();
			die2.roll();
			System.out.println();

			System.out.println(player2.getName() + "'s score is " + player2.getScore());
			System.out.println();

			System.out.println(player2.getName() + " rolled " + (die1.getValue() + die2.getValue()) + " points");
			player2.calcScore(die1.getValue(), die2.getValue());
			System.out.println(player2.getName() + "'s new score is " + player2.getScore());
			System.out.println();

			if (player1.getScore() == 1) {
				break;
			}
			if (player2.getScore() == 1) {
				break;
			}
			System.out.print("Press R to roll again: ");
			input = keyboard.nextLine().toUpperCase();
			roll = input.charAt(0);

		} while (roll == 'R');
		if (player1.getScore() == 1) {
			System.out.println(player1.getName() + " Wins!");
		}
		if (player2.getScore() == 1) {
			System.out.println(player2.getName() + " Wins!");
		}
	}

}
