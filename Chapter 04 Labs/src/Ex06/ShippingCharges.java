package Ex06;

public class ShippingCharges {
	private double weight;
	private double rate;
	private int milesShipped;
	private int per500Miles;

	double totalCharge;

	public ShippingCharges(double kgs, int miles) {
		weight = kgs;
		milesShipped = miles;
	}

	public double getWeight() {
		return weight;
	}

	public double getRate() {
		return rate;
	}

	public void calcWeightPrice() {

		if (weight > 10) {
			rate = 4.80;
		} else if (weight > 6 && weight <= 10) {
			rate = 3.70;
		} else if (weight > 2 && weight <= 6) {
			rate = 2.20;
		} else {
			rate = 1.10;
		}

	}

	public void calcMilePrice() {

		if (milesShipped < 500)
			per500Miles = milesShipped / 500 + 1;
		else if (milesShipped >= 500 && milesShipped % 500 != 0) {
			per500Miles = milesShipped / 500 + 1;
		} else if (milesShipped >= 500 && milesShipped % 500 == 0) {
			per500Miles = milesShipped / 500;
		}

	}

	public double calcTotalCost()

	{
		totalCharge = rate * per500Miles;
		return totalCharge;
	}

}
