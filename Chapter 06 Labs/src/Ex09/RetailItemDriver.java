package Ex09;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Scanner;

public class RetailItemDriver {

	public static void main(String[] args) throws IOException {
		Scanner keyboard = new Scanner(System.in);
		int quantity;
		do {
			System.out.print("How many items would you like to purchase: ");
			quantity = keyboard.nextInt();
			if (quantity < 0)

			{
				System.out.println("quantity cannot be less than 1.");
			}
		} while (quantity < 1);
		RetailItem item1 = new RetailItem("item1", 1232, 15.99, 20.99);
		CashRegister sale1 = new CashRegister(item1, quantity);

		DecimalFormat cash = new DecimalFormat("#0.00");

		FileWriter fw = new FileWriter("Sales Receipt.txt");
		PrintWriter outputFile = new PrintWriter(fw);
		outputFile.println("SALES RECEIPT");

		outputFile.println("Unit Price: $" + cash.format(item1.getRetail()));
		outputFile.println("Quantity: " + sale1.getQuantity());
		outputFile.println("Subtotal: $" + cash.format(sale1.getSubtotal()));
		outputFile.println("Sales Tax: $" + cash.format(sale1.getTax()));
		outputFile.println("Total $" + cash.format(sale1.getTotal()));
		System.out.println("A sales receipt was generated.");
		outputFile.close();
	}

}
