package Ex04;

public class Temperature {
	double ftemp;

	public Temperature(double temp) {
		ftemp = temp;
	}

	public void setFahrenheit(double temp) {
		ftemp = temp;
	}

	public double getFahrenheit() {
		return ftemp;
	}

	public double getCelsius() {
		double celsius = (5f / 9f) * (ftemp - 32f);
		return celsius;
	}

	public double getKelvin() {
		double kelvin = ((5f / 9f) * (ftemp - 32f)) + 273f;
		return kelvin;
	}

}
