package Ex03;

public class InvalidPriceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidPriceException(String msg) {
		super(msg);
	}

}