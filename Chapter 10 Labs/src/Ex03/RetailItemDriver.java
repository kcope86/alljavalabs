package Ex03;

public class RetailItemDriver {

	public static void main(String[] args) {
		RetailItem item1 = new RetailItem();
		RetailItem item2 = new RetailItem();
		RetailItem item3 = new RetailItem();

		item1.setDescription("Jacket");
		try {
			item1.setUnitsOnHand(12);

		} catch (InvalidQuantityException ex) {
			System.out.println(ex.toString());
		}
		try {
			item1.setPrice(59.95);

		} catch (InvalidPriceException ex) {
			System.out.println(ex.toString());
		}

		item2.setDescription("Designer Jeans");
		try {
			item2.setUnitsOnHand(-1);

		} catch (InvalidQuantityException ex) {
			System.out.println(ex.toString());
		}
		try {
			item2.setPrice(34.95);

		} catch (InvalidPriceException ex) {
			System.out.println(ex.toString());
		}

		item3.setDescription("Shirt");
		try {
			item3.setUnitsOnHand(20);
		} catch (InvalidQuantityException ex) {
			System.out.println(ex.toString());
		}
		try {
			item3.setPrice(-24.95);
		} catch (InvalidPriceException ex) {
			System.out.println(ex.toString());
		}
		System.out.println(item1.getDescription());
		System.out.println(item1.getUnitsOnHand() + " units on hand");
		System.out.printf("%.2f \n", item1.getPrice());
		System.out.println();

		System.out.println(item2.getDescription());
		System.out.println(item2.getUnitsOnHand() + " units on hand");
		System.out.printf("%.2f \n", item2.getPrice());
		System.out.println();

		System.out.println(item3.getDescription());
		System.out.println(item3.getUnitsOnHand() + " units on hand");
		System.out.printf("%.2f \n", item3.getPrice());
		System.out.println();

	}

}
