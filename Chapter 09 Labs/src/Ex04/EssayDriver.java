package Ex04;

import java.util.Scanner;

public class EssayDriver {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);

		double grammar;
		double spelling;
		double length;
		double content;

		System.out.println("Please enter a students scores in the following categories: ");
		do {
			System.out.print("Grammar (up to 30 points): ");
			grammar = keyboard.nextDouble();
			if (grammar > 30) {
				System.out.println("Grammar score cannot be greater than 30.");
			}
			if (grammar < 0) {
				System.out.println("Grammar score cannot be less than 0.");
			}
		} while (grammar > 30 || grammar < 0);

		do {
			System.out.print("Spelling (up to 20 points): ");
			spelling = keyboard.nextDouble();
			if (spelling > 20) {
				System.out.println("Spelling score cannot be greater than 20.");
			}
			if (spelling < 0) {
				System.out.println("Spelling score cannot be less than 0.");
			}
		} while (spelling > 20 || spelling < 0);

		do {
			System.out.print("Correct Length (up to 20 points): ");
			length = keyboard.nextDouble();
			if (length > 20) {
				System.out.println("Length score cannot be greater than 20.");
			}
			if (length < 0) {
				System.out.println("Length score cannot be less than 0.");
			}
		} while (length > 20 || length < 0);

		do {
			System.out.print("Content (up to 30 points): ");
			content = keyboard.nextDouble();
			if (content > 30) {
				System.out.println("Content score cannot be greater than 30.");
			}
			if (content < 0) {
				System.out.println("Content score cannot be less than 0.");
			}
		} while (content > 30 || content < 0);

		Essay essay1 = new Essay(grammar, spelling, length, content);
		essay1.setScore(grammar + spelling + length + content);

		System.out.println("Student score is " + essay1.getScore());
		System.out.println("Student grade is " + essay1.getGrade());
	}
}
