package Ex14;

import java.io.IOException;

public class FileDisplayDriver {

	public static void main(String[] args) throws IOException {

		FileDisplay favoriteSong1 = new FileDisplay("DarkestHour.txt");
		FileDisplay favoriteSong2 = new FileDisplay("DarkestHour.txt");
		FileDisplay favoriteSong3 = new FileDisplay("DarkestHour.txt");

		favoriteSong1.displayHead();
		System.out.println();
		System.out.println();

		favoriteSong2.displayContents();
		System.out.println();
		System.out.println();

		favoriteSong3.displayWithLineNumbers();
		System.out.println();
		System.out.println();

	}

}
