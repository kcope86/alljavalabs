package Ex08;

public class Runner {
	private String name;
	private double time;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public Runner(String name, double time) {
		this.name = name;
		this.time = time;
	}

	public String getFirstPlace(Runner one, Runner two, Runner three) {

		if (one.getTime() < two.getTime() && one.getTime() < three.getTime()) {
			return one.getName();
		} else if (two.getTime() < one.getTime() && two.getTime() < three.getTime()) {
			return two.getName();
		} else {
			return three.getName();
		}
	}

	public String getSecondPlace(Runner one, Runner two, Runner three) {

		if (one.getTime() < two.getTime() && one.getTime() > three.getTime()) {
			return one.getName();
		} else if (two.getTime() < one.getTime() && two.getTime() > three.getTime()) {
			return two.getName();
		} else {
			return three.getName();
		}
	}

	public String getThirdPlace(Runner one, Runner two, Runner three) {

		if (one.getTime() > two.getTime() && one.getTime() > three.getTime()) {
			return one.getName();
		} else if (two.getTime() > one.getTime() && two.getTime() > three.getTime()) {
			return two.getName();
		} else {
			return three.getName();
		}
	}

	public String displayRunner(Runner runner) {
		return runner.getName() + ": " + runner.getTime();

	}
}
