package Ex08;

public class RetailItem {

	private String description;
	private int itemNumber;
	private CostData cost;

	public RetailItem(String desc, int itemNum, double wholesale, double retail) {
		description = desc;
		itemNumber = itemNum;
		cost = new CostData(wholesale, retail);

	}

	public String toString() {
		String str;
		str = String.format(
				"Description: %s\n" + "Item Number: %d\n" + "Wholesale cost: $%,.2f\n" + "Retail Price: $%,.2f\n",
				description, itemNumber, cost.wholesale, cost.retail);
		return str;
	}

	public double getWholeSale() {
		return cost.wholesale;
	}

	public double getRetail() {
		return cost.retail;
	}

	public void setWholeSale(double w) {
		cost.wholesale = w;
	}

	public void setRetail(double r) {
		cost.retail = r;
	}

	private class CostData {
		public double wholesale, retail;

		public CostData(double w, double r) {
			wholesale = w;
			retail = r;
		}

	}

}
