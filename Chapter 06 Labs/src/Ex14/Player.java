package Ex14;

public class Player {

	private String name;
	private int score = 0;

	public Player(String name) {

		this.name = name;

	}

	public String guess(int side) {
		if (side == 1) {
			return "Heads";
		} else {
			return "Tails";
		}
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void wins() {
		score++;
	}

}
