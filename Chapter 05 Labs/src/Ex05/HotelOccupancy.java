package Ex05;

import java.util.Scanner;

public class HotelOccupancy {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);

		int floors;
		int totalRooms = 0;
		int rooms;
		int totalOccupied = 0;
		int occupied;
		int vacant = 0;
		do {
			System.out.print("How many floors does the hotel have?");
			floors = keyboard.nextInt();
			if (floors < 1) {
				System.out.println("number of floors must be greater than zero.");
			}
		} while (floors < 1);

		for (int x = 1; x <= floors; x++) {
			do {
				System.out.printf("How many rooms are on floor %d: ", x);
				rooms = keyboard.nextInt();
			} while (rooms < 10);
			totalRooms += rooms;
			do {
				System.out.printf("How many rooms on floor %d are occupied: ", x);
				occupied = keyboard.nextInt();
				if (occupied > rooms) {
					System.out.println("occupied rooms cannot exceed total rooms.");
				}
			} while (occupied > rooms);
			totalOccupied += occupied;
			vacant = totalRooms - totalOccupied;

		}
		System.out.printf("The hotel has %d rooms, %d of them are occupied, and %d of them are vacant.", totalRooms,
				totalOccupied, vacant);
	}
}
