package Ex10;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class BankAccountFile {
	private final int RECORD_SIZE = 24; // Record length

	private RandomAccessFile accountFile; // Random–Access file

	public BankAccountFile(String filename) throws FileNotFoundException {
		// Open the file for reading and writing.
		accountFile = new RandomAccessFile(filename, "rw");
	}

	public void writeAccountItem(BankAccount account) throws IOException {

		accountFile.writeDouble(account.getBalance());
		accountFile.writeDouble(account.getInterestRate());
		accountFile.writeDouble(account.getInterest());

	}

	public BankAccount readAccountItem() throws IOException, NegativeStartingBalance, NegativeInterestRate {
		double balance = accountFile.readDouble();
		double interestRate = accountFile.readDouble();
		double interest = accountFile.readDouble();
		BankAccount b1 = new BankAccount(balance, interestRate);
		return b1;
	}

	public int getByteNumber(int x) {
		return RECORD_SIZE * x;
	}

	public void moveFilePointer(int x) throws IOException {
		accountFile.seek(getByteNumber(x));
	}

}
