package Ex06;

public class Population {
	private double organisms;

	private double percentIncrease;
	private int daysToMultiply;

	public double getOrganisms() {
		return organisms;
	}

	public Population(double size, double increase, int days) {
		organisms = size;
		percentIncrease = increase;
		daysToMultiply = days;
	}

	public void setOrganisms(double startingOrganisms) {
		this.organisms = startingOrganisms;
	}

	public int getDaysToMultiply() {
		return daysToMultiply;
	}

	public void setDaysToMultiply(int daysToMultiply) {
		this.daysToMultiply = daysToMultiply;
	}

	public void getIncrease() {

		System.out.printf("Your population is starting out with %.0f organisms\n"
				+ "it will grow at a rate of %.0f percent\n" + "for %d days\n", organisms, percentIncrease,
				daysToMultiply);
		System.out.println();
		percentIncrease = percentIncrease / 100.0;

		for (int x = 1; x <= daysToMultiply; ++x) {

			double afterIncrease = (organisms + organisms * percentIncrease);
			System.out.printf("After %d day(s) your population will be %.2f\n", x, afterIncrease);
			organisms = afterIncrease;

		}

	}
}
