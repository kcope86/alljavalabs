package Ex01;

public class Area {

	private static int tries = 0;
	private int length;
	private int width;
	private int height;
	private int instance;

	public static double calcArea(double r) {
		double area = Math.PI * Math.pow(r, 2);
		tries++;
		return area;

	}

	public static double calcArea(double width, double length) {
		double area = width * length;
		tries++;
		return area;
	}

	public static float calcArea(float r, float h) {
		float area = (float) Math.PI * (float) Math.pow(r, 2) * h;
		tries++;
		return area;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Area(int length, int width, int height) {
		this.length = length;
		this.width = width;
		this.height = height;
		instance = tries;
	}

	public Area() {
		instance = tries;

	}

	public int getInstance() {
		return instance;
	}

}
