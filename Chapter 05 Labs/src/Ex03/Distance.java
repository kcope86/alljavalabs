package Ex03;

public class Distance {
	private int speed;
	private int timeInHours;

	public Distance(int speed, int time) {
		this.speed = speed;
		timeInHours = time;
	}

	public int getDistance() {
		return speed * timeInHours;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getTimeInHours() {
		return timeInHours;
	}

	public void setTimeInHours(int timeInHours) {
		this.timeInHours = timeInHours;
	}
}
