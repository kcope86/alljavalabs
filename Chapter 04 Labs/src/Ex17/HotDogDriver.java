package Ex17;

import java.util.Scanner;

public class HotDogDriver {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int guests;
		int dogsPerGuest;

		System.out.print("How many guests will be eating: ");
		guests = keyboard.nextInt();
		System.out.print("How many hot dogs will be provided for each guest: ");
		dogsPerGuest = keyboard.nextInt();
		HotDogs cookout1 = new HotDogs(guests, dogsPerGuest);

		System.out.printf(
				"You will need: %d hot dogs, and %d buns\n " + "%d packs of hot dogs\n " + "%d bags of hot dog buns ",
				cookout1.getTotalDogs(), cookout1.getTotalBuns(), cookout1.calcDogPacks(), cookout1.calcBunBags());

		System.out.println();
		System.out.printf("You will have:\n " + "%d leftover hot dogs\n " + "and\n " + "%d leftover buns",
				cookout1.getLeftOverDogs(), cookout1.getLeftOverBuns());

	}
}
