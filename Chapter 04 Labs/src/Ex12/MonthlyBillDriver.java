package Ex12;

import java.util.Scanner;

import Ex11.MonthlyBill;

public class MonthlyBillDriver {

	public static void main(String[] args) {
		String input;
		char plan;
		int minutes;
		Scanner keyboard = new Scanner(System.in);
		do {
			System.out.print("Do you have plan A B or C? ");
			input = keyboard.nextLine().toUpperCase();
			plan = input.charAt(0);

		} while (plan != 'A' && plan != 'B' && plan != 'C');
		System.out.print("How many minutes did you use: ");
		minutes = keyboard.nextInt();
		MonthlyBill bill1 = new MonthlyBill(plan, minutes);
		MonthlyBill billA = new MonthlyBill('A', minutes);
		MonthlyBill billB = new MonthlyBill('B', minutes);
		MonthlyBill billC = new MonthlyBill('C', minutes);

		System.out.printf("The total charges for plan %s are as follows:\n", bill1.getPlan());
		if (bill1.getPlan() == 'A') {
			System.out.printf("Monthly charges: $%.2f\n", bill1.getPLAN_A());
		}
		if (bill1.getPlan() == 'B') {
			System.out.printf("Monthly charges: $%.2f\n", bill1.getPLAN_B());
		}
		if (bill1.getPlan() == 'C') {
			System.out.printf("Monthly charges: $%.2f\n", bill1.getPLAN_C());
		}
		System.out.printf("you used %d minutes\n", minutes);
		System.out.printf("Your total monthly bill is $%,.2f\n", bill1.calcMonthlyBill());

		if (bill1.calcMonthlyBill() == billA.calcMonthlyBill() && bill1.calcMonthlyBill() > billB.calcMonthlyBill()) {
			System.out.printf("If you switch to plan B your total will be $%,.2f\n" + " a savings of $%,.2f.",
					billB.calcMonthlyBill(), bill1.calcMonthlyBill() - billB.calcMonthlyBill());
		}
		if (bill1.calcMonthlyBill() == billA.calcMonthlyBill() && bill1.calcMonthlyBill() > billC.calcMonthlyBill()) {
			System.out.printf("If you switch to plan C your total will be $%,.2f\n" + " a savings of $%,.2f.",
					billC.calcMonthlyBill(), bill1.calcMonthlyBill() - billC.calcMonthlyBill());
		}
		if (bill1.calcMonthlyBill() == billB.calcMonthlyBill() && bill1.calcMonthlyBill() > billC.calcMonthlyBill()
				&& bill1.calcMonthlyBill() > billC.calcMonthlyBill()) {
			System.out.printf("If you switch to plan C your total will be $%,.2f\n" + " a savings of $%,.2f.",
					billC.calcMonthlyBill(), bill1.calcMonthlyBill() - billC.calcMonthlyBill());
		}

	}

}
