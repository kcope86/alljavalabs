package Ex10;

public class InvalidNumberFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidNumberFormatException(String msg) {
		super(msg);
	}

}
