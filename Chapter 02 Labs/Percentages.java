import java.util.Scanner;
public class Percentages
{
	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			String input;
			int males;
			int females;
			float malePercentage;
			float femalePercentage;
			int classSize;
			
			System.out.print("How many males are in the class:  ");
			males = keyboard.nextInt();
			System.out.print("How many females are in the class:  ");
			females = keyboard.nextInt();
			classSize = males + females;
			malePercentage = ((float)males / classSize) * 100;
			femalePercentage = ((float)females / classSize) * 100;
			System.out.println("The class is " + malePercentage + "% male and " + femalePercentage + "% female.");
			
			System.out.println("Would you like to check different amounts?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
		}while (repeat == 'Y' || repeat == 'y');
		
	}
}