package Ex07;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileEncryption {

	public static void encrypt(String file) throws IOException {
		boolean endOfFile = false;
		String name;
		String nameEncrypted = "";
		int character;
		char ch;

		DataInputStream inputFile = new DataInputStream(new FileInputStream(file));
		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream("encryptedData.dat"));

		while (!endOfFile) {
			try {
				name = inputFile.readUTF();

				for (int i = 0; i < name.length(); i++) {
					character = (int) name.charAt(i) + 25;
					ch = (char) character;
					nameEncrypted += ch;

				}
				outputFile.writeUTF(nameEncrypted);

				nameEncrypted = "";

			} catch (EOFException ex) {
				endOfFile = true;

			}

		}

		inputFile.close();
		outputFile.close();

	}
}
