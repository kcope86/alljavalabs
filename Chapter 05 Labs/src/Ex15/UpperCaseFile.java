package Ex15;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class UpperCaseFile {

	private String file1;
	private String file2;
	File readFile;
	Scanner inputFile;
	PrintWriter outputFile;

	public UpperCaseFile(String f1, String f2) throws IOException {
		readFile = new File(f1);
		inputFile = new Scanner(readFile);
		outputFile = new PrintWriter(f2);

	}

	public void writeToUpper() {
		while (inputFile.hasNext()) {
			outputFile.println(inputFile.nextLine().toUpperCase());

		}
		System.out.println("Your file has been created.");
		outputFile.close();
	}

}
