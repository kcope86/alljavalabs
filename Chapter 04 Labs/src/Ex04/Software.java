package Ex04;

public class Software {
	private final double BASE_PRICE = 99;
	private double discountRate;
	private int intDiscount;
	private double quantity;
	private double totalPrice;

	public double getQuantity() {
		return quantity;
	}

	public Software(double quantity) {
		this.quantity = quantity;
	}

	public int calcDiscount(double quantity) {
		if (quantity >= 100) {
			intDiscount = 50;

		} else if (quantity >= 50 && quantity <= 99) {
			intDiscount = 40;

		} else if (quantity >= 20 && quantity <= 49) {
			intDiscount = 30;

		} else if (quantity >= 10 && quantity <= 19) {
			intDiscount = 20;

		}
		return intDiscount;
	}

	public double calcTotalCost(double quantity) {
		if (quantity >= 100) {
			discountRate = .5;
			totalPrice = (quantity * BASE_PRICE) * discountRate;
		} else if (quantity >= 50 && quantity <= 99) {
			discountRate = .6;
			totalPrice = (quantity * BASE_PRICE) * discountRate;
		} else if (quantity >= 20 && quantity <= 49) {
			discountRate = .7;
			totalPrice = (quantity * BASE_PRICE) * discountRate;
		} else if (quantity >= 10 && quantity <= 19) {
			discountRate = .8;
			totalPrice = (quantity * BASE_PRICE) * discountRate;
		}
		return totalPrice;
	}
}
