package Ex07;

import java.util.Scanner;

public class FatGramDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		double calories;
		double gramsOfFat;
		boolean validAmounts = false;

		do {
			System.out.print("Enter the number of calories: ");
			calories = keyboard.nextDouble();
			System.out.print("Enter the amount of grams of fat: ");
			gramsOfFat = keyboard.nextDouble();

			if (gramsOfFat * 9 < calories) {
				validAmounts = true;
			} else {
				System.out.println("grams of fat cannot exceed number of total calories divided by 9.");

			}

		} while (validAmounts == false);
		FatGram food1 = new FatGram(gramsOfFat, calories);
		System.out.printf("the percentage of calories from fat is %.2f\n", food1.calcFatCalPercentage());
		System.out.printf("this food is %s", food1.isLowFat(food1.calcFatCalPercentage()));

	}

}
