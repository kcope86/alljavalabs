package Ex05;

public class RetailItem 
{
	
	String description;
	int unitsOnHand;
	double price;
	
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public void setUnitsOnHand(int units)
	{
		unitsOnHand = units;
	}
	
	public void setPrice(double price)
	{
		this.price = price;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public int getUnitsOnHand()
	{
		return unitsOnHand;
	}
	
	public double getPrice()
	{
		return price;
	}
}
