package Ex01;

import java.util.Scanner;

public class RomanNumerals {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int playerNum;

		char repeat;

		do {
			String input;
			System.out.print("Enter a number from 1 - 10: ");
			playerNum = keyboard.nextInt();
			keyboard.nextLine();

			if (playerNum == 1) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is I");
			} else if (playerNum == 2) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is II");
			} else if (playerNum == 3) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is III");
			} else if (playerNum == 4) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is IV");
			} else if (playerNum == 5) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is V");
			} else if (playerNum == 6) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is VI");
			} else if (playerNum == 7) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is VII");
			} else if (playerNum == 8) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is VIII");
			} else if (playerNum == 9) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is IX");
			} else if (playerNum == 10) {
				System.out.println("The Roman Numeral for the number " + playerNum + " is X");
			} else if (playerNum > 10 || playerNum < 1) {
				System.out.println("The number you entered is not between 1 and 10.");
			}

			System.out.println("Would you like to try again?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.nextLine();
			repeat = input.charAt(0);
		} while (repeat == 'Y' || repeat == 'y');
		keyboard.close();
	}

}
