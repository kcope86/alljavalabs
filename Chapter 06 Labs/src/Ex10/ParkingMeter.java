package Ex10;

public class ParkingMeter {

	private int minutesPurchased;

	public int getMinutesPurchased() {
		return minutesPurchased;
	}

	public void setMinutesPurchased(int minutesPurchased) {
		this.minutesPurchased = minutesPurchased;
	}

	public ParkingMeter(int minutesPurchased) {

		this.minutesPurchased = minutesPurchased;
	}

}
