package Ex01;

public class PhoneBill {

	// Arrays that store area codes and rates
	int[] areaCodes = new int[] { 262, 414, 608, 715, 815, 920 };
	double[] rates = new double[] { .07, .10, .05, .16, .24, .14 };

	// fields
	private int area;
	private int minutes;

	// constructor
	public PhoneBill(int area, int minutes) {
		this.area = area;
		this.minutes = minutes;

	}

	// area code validation
	public static int areaSearch(int[] areaCodes, int value) {
		int index;
		int element;
		boolean found;

		// searches through array
		index = 0;

		// stores the index for the found element
		element = -1;
		found = false;

		while (!found && index < areaCodes.length) {
			if (areaCodes[index] == value) {
				found = true;
				element = index;
			}

			index++;
		}
		// return found at index or -1 for not found.
		return element;
	}

	// method to calculate bill
	public double calcBill() {
		double bill;

		bill = rates[areaSearch(areaCodes, area)] * minutes;
		return bill;

	}
}
