package Ex15;

import java.util.Scanner;

public class PointsDriver {

	public static void main(String[] args) {
		int books;
		Scanner keyboard = new Scanner(System.in);
		System.out.print("How many books did you purchase this month: ");
		books = keyboard.nextInt();

		Points p1 = new Points(books);
		System.out.printf("%d books has earned you %d points", books, p1.calcClubPoints());
	}

}
