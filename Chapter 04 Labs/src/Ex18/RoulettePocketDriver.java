package Ex18;

import java.util.Scanner;

public class RoulettePocketDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int number;
		String stringNumber;

		do {

			System.out.print("Enter a pocket number: ");
			stringNumber = keyboard.nextLine();

			number = Integer.parseInt(stringNumber);
			if ((number < 0 || number > 36) && (stringNumber != "00")) {
				System.out.println("That is not a valid number.");
			}

		} while ((number < 0 || number > 36) && (stringNumber != "00"));

		RoulettePocket pocket1 = new RoulettePocket(stringNumber);

		System.out.printf("%s is a %s pocket.", stringNumber, pocket1.getPocketColor());

		/*
		 * if (input == "00") { RoulettePocket pocket1 = new RoulettePocket("00"); }
		 * else if ()
		 */

		keyboard.close();

	}
}
