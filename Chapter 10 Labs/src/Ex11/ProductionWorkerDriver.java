package Ex11;

import java.util.Scanner;

public class ProductionWorkerDriver {

	public static void main(String[] args) throws InvalidEmployeeNumberException {
		String name;
		int empNumber = 1234;
		String hireDate;
		int shift;
		double payRate;

		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter the employee name: ");
		name = keyboard.nextLine();
		System.out.println();

		System.out.print("Enter employee number: ");

		empNumber = keyboard.nextInt();
		keyboard.nextLine();
		try {
			Employee.isValid(empNumber);
		} catch (InvalidEmployeeNumberException ex) {
			System.out.println(ex.getMessage());
		}
		System.out.print("Enter Date Hired: ");
		hireDate = keyboard.nextLine();
		System.out.print("Enter pay rate: ");
		payRate = keyboard.nextDouble();

		try {
			ProductionWorker.isValidPayRate(payRate);
		} catch (InvalidPayRateException ex) {
			System.out.println(ex.getMessage());
		}
		System.out.print("Enter shift: ");
		shift = keyboard.nextInt();
		keyboard.nextLine();
		try {
			ProductionWorker.isValidShift(shift);
		} catch (InvalidShiftException ex) {
			System.out.println(ex.getMessage());
		}
		System.out.println();

		ProductionWorker worker1 = new ProductionWorker();
		worker1.setName(name);
		worker1.setEmployeeNumber(empNumber);
		worker1.setHireDate(hireDate);
		worker1.setShift(shift);
		worker1.setPayRate(payRate);

		System.out.println("Using Mutators");
		System.out.println(worker1.toString());
		System.out.println();

		ProductionWorker worker2 = new ProductionWorker(name, empNumber, hireDate, shift, payRate);
		System.out.println("Using constructor with fields");
		System.out.println(worker2.toString());
	}

}
