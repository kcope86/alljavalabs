package Ex09;
import java.util.Scanner;
public class CircleDriver {

	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);

		System.out.print("What is the radius of your circle? ");
		Circle circle1 = new Circle(keyboard.nextDouble());
		
		System.out.print("Your circles area is " + circle1.getArea() + "\n");
		System.out.print("Your circles diameter is " + circle1.getDiameter() + "\n");
		System.out.print("Your circles circumference is " + circle1.getCircumference() + "\n");
		
	}

}
