package Ex04;

public class InvalidMonthNumberException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidMonthNumberException(String msg) {
		super(msg);
	}
}
