package Ex19;

import java.util.Scanner;

public class TroubleShootingDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		String userInput;
		char response;

		System.out.println("Reboot the computer and try to connect.");
		System.out.print("Did that fix your problem? ");
		userInput = keyboard.nextLine().toUpperCase();
		response = userInput.charAt(0);
		if (response == 'N') {
			System.out.println("Reboot the router and try to connect.");
			System.out.print("Did that fix your problem? ");
			userInput = keyboard.nextLine().toUpperCase();
			response = userInput.charAt(0);
		}
		if (response == 'N') {
			System.out.println("Make sure cables between the\n" + "router and modem are plugged in firmly.");
			System.out.print("Did that fix your problem? ");
			userInput = keyboard.nextLine().toUpperCase();
			response = userInput.charAt(0);
		}
		if (response == 'N') {
			System.out.println("Move the router to a new \n" + "location and try to connect.");
			System.out.print("Did that fix your problem? ");
			userInput = keyboard.nextLine().toUpperCase();
			response = userInput.charAt(0);
		}
		if (response == 'N') {
			System.out.println("Get a new router.");

		} else {
			System.out.println("Glad to help!");
		}

		keyboard.close();
	}

}
