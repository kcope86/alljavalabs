package Examples;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CompareStrings extends Application {
	private String[] users;
	private String[] passwords;

	Label l1;
	Label l2;
	Button submit;
	Label result;
	TextField userInput;
	TextField passInput;

	private String user;
	private String password;

	VBox input;
	HBox userPrompt;
	HBox passPrompt;
	HBox passFail;

	GridPane grid;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		users = new String[] { "KEVIN", "JOSH" };
		passwords = new String[] { "Rocksteady119", "Password" };

		l1 = new Label("Enter User Name: ");
		l2 = new Label("Enter your Password: ");
		submit = new Button("Check");
		result = new Label("results");
		userInput = new TextField();
		passInput = new TextField();
		;
		userPrompt = new HBox(20, l1, userInput);
		passPrompt = new HBox(20, l2, passInput, submit);
		passFail = new HBox(20, result);
		input = new VBox(20, userPrompt, passPrompt, passFail);

		grid = new GridPane();
		grid.add(input, 0, 0);

		Scene scene = new Scene(grid, 400, 400);
		primaryStage.setScene(scene);
		primaryStage.show();

		submit.setOnAction(e -> {
			user = userInput.getText().toUpperCase();
			password = passInput.getText();

			if (user.equals("KEVIN")) {
				if (password.equals("Rocksteady119")) {
					result.setText(String.format("Welcome %s", users[0]));
				}
			} else if (user.equals("JOSH")) {
				if (password.equals("Password")) {
					result.setText(String.format("Welcome %s", users[1]));
				}
			}

		});

	}

}
