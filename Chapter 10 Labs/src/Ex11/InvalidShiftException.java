package Ex11;

public class InvalidShiftException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidShiftException(String msg) {
		super(msg);
	}
}
