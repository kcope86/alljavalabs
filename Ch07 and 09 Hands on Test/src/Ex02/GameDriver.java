package Ex02;

public class GameDriver {

	public static void main(String[] args) {

		// create game1
		Game game1 = new Game("Checkers", 2);

		// display game1
		System.out.println(game1.toString());

		// create game2
		GameWithTimeLimit game2 = new GameWithTimeLimit("Chess", 2, 20);

		// display game2
		System.out.println(game2.toString());

	}

}
