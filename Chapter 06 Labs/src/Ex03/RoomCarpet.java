package Ex03;

public class RoomCarpet {

	private RoomDimension size;
	private double cost;

	public RoomCarpet(RoomDimension dim, double cost) {
		size = dim;
		this.cost = cost;
	}

	public double getTotalCost() {
		return size.getArea() * cost;
	}

	public String toString() {

		return "Total cost of the carpet will be $" + getTotalCost();
	}

}
