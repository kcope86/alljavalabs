package Ex02;

public class TestScores {

	private int[] exam;

	public int[] getExam() {
		return exam;
	}

	public void setExam(int[] exam, int size) {
		for (int i = 0; i < exam.length; i++) {
			this.exam[i] = exam[i];

		}
	}

	public TestScores() {
		exam = new int[0];
	}

	public TestScores(int[] exam, int size) throws InvalidTestScore {
		this.exam = new int[size];
		for (int i = 0; i < size; i++) {
			if (exam[i] < 0 || exam[i] > 100) {
				throw new InvalidTestScore("Invalid Test Scores");
			}
			this.exam[i] = exam[i];

		}
	}

	public double getAverage() {
		double average = 0;
		for (int i = 0; i < exam.length; i++) {
			average += exam[i];
		}
		return average / exam.length;
	}

}
