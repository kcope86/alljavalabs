package Ex10;

public class BankAccount {
	private double balance; // Account balance
	private double interestRate; // Interest rate
	private double interest; // Interest earned

	public BankAccount(double startBalance, double intRate) throws NegativeStartingBalance, NegativeInterestRate {
		if (startBalance < 0)
			throw new NegativeStartingBalance(startBalance);
		if (intRate < 0)
			throw new NegativeInterestRate(intRate);

		balance = startBalance;
		interestRate = intRate;

	}

	public void deposit(double amount) {
		balance = balance + amount;
	}

	/**
	 * The withdraw method subtracts the parameter amount from the balance field.
	 */

	public void withdraw(double amount) {
		balance = balance - amount;
	}

	/**
	 * The addInterest method adds the interest for the month to balance.
	 */

	public void addInterest() {
		interest = balance * interestRate;
		balance = balance + interest;
	}

	/**
	 * The getBalance method returns the value in the balance field.
	 */

	public double getBalance() {
		return balance;
	}

	public double getInterestRate() {
		return interestRate;
	}

	/**
	 * The getInterest method returns the value in the interest field.
	 */

	public double getInterest() {
		return interest;
	}
}
