package Ex11;

public class MonthlyBill {
	private char plan;
	private int minutes;
	final double PLAN_A = 39.99;
	final double PLAN_B = 59.99;
	final double PLAN_C = 69.99;

	public double getPLAN_A() {
		return PLAN_A;
	}

	public double getPLAN_B() {
		return PLAN_B;
	}

	public double getPLAN_C() {
		return PLAN_C;
	}

	public char getPlan() {
		return plan;
	}

	public void setPlan(char plan) {
		this.plan = plan;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public MonthlyBill(char plan, int minutes) {
		this.plan = plan;
		this.minutes = minutes;
	}

	public double calcMonthlyBill() {

		double total;
		if (plan == 'A') {
			if (minutes > 450) {
				total = PLAN_A + ((minutes - 450) * .45);
				return total;
			} else {
				total = PLAN_A;
				return total;
			}
		} else if (plan == 'B') {
			if (minutes > 900) {
				total = PLAN_B + ((minutes - 900) * .45);
				return total;
			} else {
				total = PLAN_B;
				return total;
			}
		} else {
			return PLAN_C;
		}
	}

}
