package Ex01;

import java.util.Scanner;

public class SumOfNumbers {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter a positive integer number greater than 0: ");
		int x = keyboard.nextInt();
		int i = 1;
		for (int y = 1; y <= x; ++y) {
			i = i + y;
		}
		System.out.println(i);
	}
}
