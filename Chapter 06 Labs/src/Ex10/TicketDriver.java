package Ex10;

import java.util.Scanner;

public class TicketDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		ParkedCar car1 = new ParkedCar("Mitsubishi", "Galant", "White", "123456", 121);
		ParkingMeter meter1 = new ParkingMeter(60);
		PoliceOfficer cop1 = new PoliceOfficer("Farva", 1234);

		ParkingTicket p1 = cop1.inspectCar(car1, meter1);

		ParkedCar car2 = new ParkedCar("GMC", "Suburbon", "White", "232545", 120);
		ParkingMeter meter2 = new ParkingMeter(60);

		ParkingTicket p2 = cop1.inspectCar(car2, meter2);

		ParkedCar car3 = new ParkedCar("Chevy", "Tahoe", "Green", "946698", 60);
		ParkingMeter meter3 = new ParkingMeter(60);

		ParkingTicket p3 = cop1.inspectCar(car3, meter3);

		if (p1 == null) {
			System.out.printf("No ticket will be issued for %s - %s", car1.getColor(), car1.getMake());
		} else {
			System.out.println(p1.toString());
		}

		if (p2 == null) {
			System.out.printf("No ticket will be issued for %s - %s", car2.getColor(), car2.getMake());
		} else {
			System.out.println(p2.toString());

		}

		if (p3 == null) {
			System.out.printf("No ticket will be issued for %s - %s", car3.getColor(), car3.getMake());
		} else {
			System.out.println(p3.toString());

		}

	}

}
