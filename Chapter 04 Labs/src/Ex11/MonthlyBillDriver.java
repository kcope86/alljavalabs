package Ex11;

import java.util.Scanner;

public class MonthlyBillDriver {

	public static void main(String[] args) {
		String input;
		char plan;
		int minutes;
		Scanner keyboard = new Scanner(System.in);
		do {
			System.out.print("Do you have plan A B or C? ");
			input = keyboard.nextLine().toUpperCase();
			plan = input.charAt(0);

		} while (plan != 'A' && plan != 'B' && plan != 'C');
		System.out.print("How many minutes did you use: ");
		minutes = keyboard.nextInt();
		MonthlyBill bill1 = new MonthlyBill(plan, minutes);
		System.out.printf("The total charges for plan %s are as follows:\n", plan);
		if (plan == 'A') {
			System.out.printf("Monthly charges: $%.2f\n", bill1.getPLAN_A());
		}
		if (plan == 'B') {
			System.out.printf("Monthly charges: $%.2f\n", bill1.getPLAN_B());
		}
		if (plan == 'C') {
			System.out.printf("Monthly charges: $%.2f\n", bill1.getPLAN_C());
		}
		System.out.printf("you used %d minutes\n", minutes);
		System.out.printf("Your total monthly bill is $%.2f", bill1.calcMonthlyBill());

	}

}
