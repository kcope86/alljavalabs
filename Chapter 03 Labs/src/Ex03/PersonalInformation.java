package Ex03;

public class PersonalInformation 
{
	String name;
	String address;
	int age;
	String phoneNumber;
	
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public String getPhone()
	{
		return phoneNumber;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setAddress(String address)
	{
		this.address = address;
	}
	
	public void setAge(int age)
	{
		this.age = age;
	}
	
	public void setPhone(String phone)
	{
		phoneNumber = phone;
	}
	
	
	
	
	
	
	
}