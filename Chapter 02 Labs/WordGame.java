import java.util.Scanner;
public class WordGame
{
	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			String input;
			String name;
			String age;
			String city;
			String college;
			String profession;
			String animal;
			String petName;
			
			
			System.out.print("What's your name? ");
			name = keyboard.nextLine();
			System.out.print("What's your age? ");
			age = keyboard.nextLine();
			System.out.print("What's your city? ");
			city = keyboard.nextLine();
			System.out.print("What college do you go to? ");
			college = keyboard.nextLine();
			System.out.print("What's your profession? ");
			profession = keyboard.nextLine();
			System.out.print("What kind of pet would you like? ");
			animal = keyboard.nextLine();
			System.out.print("What would you name your pet? ");
			petName = keyboard.nextLine();
			System.out.println();
			
			
			System.out.printf("There once was a person named %s who lived in %s. At the age of %s,\n" + 
			"%s went to college at %s. %s graduated and went to work as a\n" +
			"%s. Then, %s adopted a(n) %s named %s. They both lived\n" +
			"happily ever after...\n", name, city, age, name, college, name, profession, name, animal, petName);
			System.out.println();
			
			System.out.println("Would you like to play again?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
			System.out.println();
		}while (repeat == 'Y' || repeat == 'y');
		
	}
}