package Ex07;

public class Widget 
{
	
	
	float days;
	float widgets;
	final int WIDGETS_PER_HOUR = 10;
	final int HOURS_PER_DAY = 16;
	final int WIDGETS_PER_DAY = 160;
	
	public float checkDays()
	{
		days = widgets / (float)WIDGETS_PER_DAY;
		
		return days;
	}
	
	public Widget(int w)
	{
		widgets = w;
	}

}
