package Ex05;

import java.util.Random;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;;

public class CoinFlip extends Application {

	Button heads;
	Button tails;
	Image head;
	Image tail;
	ImageView coin;

	Label instructions;
	Label result;

	private int player;
	private int coinResult;
	private Random rand;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		head = new Image("file:heads.jpg");
		tail = new Image("file:Tails.png");
		instructions = new Label("Select either heads or tails");
		heads = new Button("Heads");
		tails = new Button("Tails");
		coin = new ImageView();
		result = new Label("");

		GridPane grid = new GridPane();
		grid.add(instructions, 0, 0);
		grid.add(heads, 0, 1);
		grid.add(tails, 1, 1);
		grid.add(coin, 0, 2);
		grid.add(result, 0, 3);
		Scene scene = new Scene(grid, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.show();

		heads.setOnAction(e -> {
			rand = new Random();

			int side = rand.nextInt(2);
			if (side == 0) {
				coin.setImage(head);
				result.setText("You win!");

			} else if (side == 1) {
				coin.setImage(tail);
				result.setText("You lose!");

			}

		});

		tails.setOnAction(e -> {
			rand = new Random();

			int side = rand.nextInt(2);
			if (side == 0) {
				coin.setImage(head);
				result.setText("You lose!");

			} else if (side == 1) {
				coin.setImage(tail);
				result.setText("You win!");

			}

		});

	}

}
