package Ex08;

public class RunnerDriver {

	public static void main(String[] args) {
		Runner runner1, runner2, runner3;

		runner1 = new Runner("Kevin", 11.50);
		runner2 = new Runner("Josh", 10.20);
		runner3 = new Runner("Forest", 9.45);

		// Race race1 = new Race(runner1, runner2, runner3);
		System.out.println("The runners times were");
		System.out.printf(runner1.displayRunner(runner1) + "\n");
		System.out.printf(runner2.displayRunner(runner2) + "\n");
		System.out.printf(runner3.displayRunner(runner3) + "\n");
		System.out.printf("First place is %s\n", runner1.getFirstPlace(runner1, runner2, runner3));
		System.out.printf("Second place is %s\n", runner1.getSecondPlace(runner1, runner2, runner3));
		System.out.printf("Third place is %s\n", runner1.getThirdPlace(runner1, runner2, runner3));

	}

}
