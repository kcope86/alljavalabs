package Ex17;

public class HotDogs {

	private final int DOG_PACKS = 10;
	private final int BUN_BAGS = 8;
	private int guests;
	private int dogs;
	private int totalDogs;
	private int leftOverDogs;
	private int leftOverBuns;
	private int totalBuns;
	private int numOfPacks;
	private int numOfBags;

	public int getNumOfPacks() {
		return numOfPacks;
	}

	public void setNumOfPacks(int numOfPacks) {
		this.numOfPacks = numOfPacks;
	}

	public int getNumOfBags() {
		return numOfBags;
	}

	public void setNumOfBags(int numOfBags) {
		this.numOfBags = numOfBags;
	}

	public int getGuests() {
		return guests;
	}

	public void setGuests(int guests) {
		this.guests = guests;
	}

	public int getDogs() {
		return dogs;
	}

	public void setDogs(int dogs) {
		this.dogs = dogs;
	}

	public int getTotalDogs() {
		return totalDogs;
	}

	public void setTotalDogs(int totalDogs) {
		this.totalDogs = totalDogs;
	}

	public int getLeftOverDogs() {
		return leftOverDogs;
	}

	public void setLeftOverDogs(int leftOverDogs) {
		this.leftOverDogs = leftOverDogs;
	}

	public int getLeftOverBuns() {
		return leftOverBuns;
	}

	public void setLeftOverBuns(int leftOverBuns) {
		this.leftOverBuns = leftOverBuns;
	}

	public int getTotalBuns() {
		return totalBuns;
	}

	public void setTotalBuns(int totalBuns) {
		this.totalBuns = totalBuns;
	}

	public HotDogs(int guests, int dogs) {
		this.guests = guests;
		this.dogs = dogs;
		totalDogs = guests * dogs;
		totalBuns = guests * dogs;

	}

	public int calcDogPacks() {

		if (totalDogs % DOG_PACKS == 0) {
			numOfPacks = totalDogs / DOG_PACKS;
			leftOverDogs = 0;

		} else if (totalDogs % DOG_PACKS != 0) {
			numOfPacks = totalDogs / DOG_PACKS + 1;
			leftOverDogs = DOG_PACKS - (totalDogs % DOG_PACKS);
		}

		return numOfPacks;
	}

	public int calcBunBags() {
		if (totalDogs % BUN_BAGS == 0) {
			numOfBags = totalDogs / BUN_BAGS;
			leftOverBuns = 0;

		} else if (totalDogs % BUN_BAGS != 0) {
			numOfBags = totalDogs / BUN_BAGS + 1;
			leftOverBuns = BUN_BAGS - (totalDogs % BUN_BAGS);
		}

		return numOfBags;
	}
}
