package Ex02;

public class Book {

	private String name;
	private int yearPublished;
	private String Author;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPublished() {
		return yearPublished;
	}

	public void setPublished(int published) {
		this.yearPublished = published;
	}

	public String getAuthor() {
		return Author;
	}

	public void setAuthor(String author) {
		Author = author;
	}

	public Book(String name, int published, String author) {
		super();
		this.name = name;
		this.yearPublished = published;
		Author = author;
	}

}
