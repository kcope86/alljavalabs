package Ex08;

import java.util.Scanner;

public class RetailItemDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int quantity;
		do {
			System.out.print("How many items would you like to purchase: ");
			quantity = keyboard.nextInt();
			if (quantity < 0)

			{
				System.out.println("quantity cannot be less than 1.");
			}
		} while (quantity < 1);
		RetailItem item1 = new RetailItem("item1", 1232, 15.99, 20.99);
		CashRegister sale1 = new CashRegister(item1, quantity);

		System.out.println(item1.toString());
		System.out.printf("Your subtotal is %,.2f\n", sale1.getSubtotal());
		System.out.printf("Your sales tax is %,.2f\n", sale1.getTax());
		System.out.printf("Your total order is %,.2f\n", sale1.getTotal());

	}

}
