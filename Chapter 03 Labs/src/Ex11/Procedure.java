package Ex11;

public class Procedure 
	{
		private String procedureName;
		private String procedureDate;
		private String practitionerName;
		private double balance;
		
		public void setProcedureName(String pName)
		{
			procedureName = pName;
		}
		
		public void setProcedureDate(String pDate)
		{
			procedureDate = pDate;
		}
		
		public void setPractitionerName(String docName)
		{
			practitionerName = docName;
		}
		
		public String getProcedureName()
		{
			return procedureName;
		}
		
		public String getProcedureDate()
		{
			return procedureDate;
		}
		
		public String getPractitionerName()
		{
			return practitionerName;
		}
		
		public double getBalance()

		{
			return balance;
		}
		
		public Procedure(String procedure, String date, String doctor, double balance)
		{
		procedureName = procedure;
		procedureDate = date;
		practitionerName = doctor;
		this.balance = balance;
		}
	}
