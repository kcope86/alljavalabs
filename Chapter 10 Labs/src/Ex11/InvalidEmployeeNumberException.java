package Ex11;

public class InvalidEmployeeNumberException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidEmployeeNumberException(String msg) {
		super(msg);
	}

}
