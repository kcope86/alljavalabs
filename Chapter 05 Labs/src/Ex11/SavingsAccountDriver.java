package Ex11;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import Ex10.SavingsAccount;

public class SavingsAccountDriver {

	public static void main(String[] args) throws IOException {
		Scanner keyboard = new Scanner(System.in);

		double annualInterest;
		double deposits = 0;
		double withdrawls = 0;

		do {
			System.out.print("Enter your annual interest rate: ");
			annualInterest = keyboard.nextDouble();
			if (annualInterest < 0) {
				System.out.println("intereast cannot be negative");
			}
		} while (annualInterest < 0);

		SavingsAccount account1 = new SavingsAccount(500);
		account1.setInterestRate(annualInterest);

		File depositFile = new File("Deposits.txt");
		Scanner deposited = new Scanner(depositFile);
		File withdrawlFile = new File("Withdrawls.txt");
		Scanner withdrawn = new Scanner(withdrawlFile);

		while (deposited.hasNext()) {
			double scannerInput = deposited.nextDouble();
			deposits += scannerInput;
		}
		while (withdrawn.hasNext()) {
			double scannerInput = withdrawn.nextDouble();
			withdrawls += scannerInput;
		}

		account1.deposit(deposits);
		account1.calcTotalDeposits(deposits);

		account1.withdrawl(withdrawls);
		account1.calcTotalWithdrawls(withdrawls);

		account1.addMonthlyInterest();
		account1.calcTotalInterest(account1.calcMonthlyInterest());

		System.out.printf("total deposit ammount this period was $%,.2f\n", account1.getTotalDeposits());
		System.out.printf("total withdrawl ammount this period was $%,.2f\n", account1.getTotalWithdrawls());
		System.out.printf("total interest earned this period was $%,.2f\n", account1.getTotalInterest());
		System.out.printf("at this time your account balance is $%,.2f\n", account1.getBalance());

		deposited.close();
		withdrawn.close();
		keyboard.close();
	}
}
