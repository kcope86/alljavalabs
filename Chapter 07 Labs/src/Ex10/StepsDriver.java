package Ex10;

import java.io.IOException;
import java.util.Scanner;

public class StepsDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		String[] months = new String[] { "January", "February", "March", "April", "May", "June", "July", "August",
				"September", "October", "November", "December" };
		Steps thisYear = null;
		try {
			thisYear = new Steps();
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
		int input;
		int month;

		System.out.println("Enter the month that you would like to check monthly average for.");
		System.out.print("For example: Enter 1 for January, 2 for February, 3 for March... Etc");
		input = keyboard.nextInt();
		month = input - 1;

		System.out.printf("in %s, your step average was: %f", months[month], thisYear.monthlyAverage(month));
		keyboard.close();
	}

}
