package Ex03;

import java.util.Scanner;

public class RoomDimensionsDriver {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		double length;
		double width;
		double price;

		do {
			System.out.print("Enter length of the room: ");
			length = keyboard.nextDouble();
			if (length < 1) {
				System.out.println("Length cannot be less than 1.");
			}
		} while (length < 1);

		do {
			System.out.print("Enter width of the room: ");
			width = keyboard.nextDouble();
			if (width < 1) {
				System.out.println("width cannot be less than 1.");
			}
		} while (width < 1);
		do {
			System.out.print("Enter price per square foot of carpet: ");
			price = keyboard.nextDouble();
			if (price < 1) {
				System.out.println("Price cannot be less than 1.");
			}
		} while (price < 1);

		RoomDimension room1 = new RoomDimension(length, width);

		RoomCarpet carpet1 = new RoomCarpet(room1, price);

		System.out.println(room1.toString());
		System.out.println(carpet1.toString());
		keyboard.close();
	}

}
