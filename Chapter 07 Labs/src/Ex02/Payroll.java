package Ex02;

public class Payroll {

	private int[] employeeId = new int[] { 5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489 };
	private int[] hours = new int[7];
	private double[] payRate = new double[7];
	private double[] wages = new double[7];

	public int getId(int index) {

		return employeeId[index];
	}

	public void setId(int index, int idNum) {
		employeeId[index] = idNum;
	}

	public Payroll(int[] hours, double[] payRate) {
		for (int i = 0; i < employeeId.length; i++) {

			this.hours[i] = hours[i];
			this.payRate[i] = payRate[i];
			wages[i] = hours[i] * payRate[i];
		}

	}

	public int idCheck(int[] array, int value) {
		int index;
		int element;
		boolean found;

		index = 0;
		element = -1;
		found = false;

		while (!found && index < array.length) {

			if (array[index] == value) {
				found = true;
				element = index;
			}

			index++;
		}

		return element;
	}

	public double getGrossPay(int id) {
		return wages[idCheck(employeeId, id)];

	}

}
