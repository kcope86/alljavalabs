package Ex01;

import java.util.Scanner;

public class RainfallDriver {

	public static void main(String[] args) {
		double[] rain = new double[12];
		double input = 0;

		Scanner keyboard = new Scanner(System.in);

		for (int i = 0; i < rain.length; i++) {
			do {
				System.out.print("Enter the ammount of rainfall for month " + (i + 1) + ": ");
				input = keyboard.nextDouble();
				if (input < 0) {
					System.out.println("Rainfall for the month cannot be negative.");
				}
			} while (input < 0);
			rain[i] = input;
		}

		Rainfall year1 = new Rainfall(rain);

		System.out.println("The total rainfall for the last 12 months was: " + year1.getYearly());
		System.out.println();
		System.out.println("The average monthly rainfall for the last 12 months was: " + year1.getAvgRain());
		System.out.println();
		System.out.println("The month with the most rain was month: " + year1.calcMost());
		System.out.println();
		System.out.println("The month with the least rain was month: " + year1.calcLeast());
		System.out.println();

		keyboard.close();

	}

}
