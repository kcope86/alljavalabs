package Ex06;

public class DriverExam {
	private char[] answerKey = new char[] { 'B', 'D', 'A', 'A', 'C', 'A', 'B', 'A', 'C', 'D', 'B', 'C', 'D', 'A', 'D',
			'C', 'C', 'B', 'D', 'A' };
	private char[] student;

	public DriverExam(char[] array) {
		student = new char[array.length];
		for (int i = 0; i < array.length; i++) {
			student[i] = array[i];
		}

	}

	public int totalCorrect() {
		int correct = 0;
		for (int i = 0; i < answerKey.length; i++) {
			if (student[i] == answerKey[i])
				correct++;
		}
		return correct;
	}

	public int totalIncorrect() {
		int incorrect = 0;
		for (int i = 0; i < answerKey.length; i++) {
			if (student[i] != answerKey[i])
				incorrect++;

		}
		return incorrect;
	}

	public boolean passed() {
		boolean pass;
		if (totalCorrect() < 15) {
			pass = false;
		} else {
			pass = true;
		}
		return pass;
	}

	public int[] questionsMissed() {
		int[] missed = new int[totalIncorrect()];
		int m = 0;
		for (int i = 0; i < answerKey.length; i++) {
			if (student[i] != answerKey[i]) {
				missed[m] = (i + 1);
				m++;
			}

		}
		return missed;
	}

	public static int InputValidation(char value) {
		char[] array = new char[] { 'A', 'B', 'C', 'D' };
		int index, element;
		boolean found;

		index = 0;

		element = -1;
		found = false;

		while (!found && index < array.length) {

			if (array[index] == value) {
				found = true;
				element = index;
			}

			index++;
		}

		return element;
	}

}
