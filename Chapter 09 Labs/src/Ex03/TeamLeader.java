package Ex03;

public class TeamLeader extends ProductionWorker {

	private int trainingRequired;
	private int trainingReceived;
	private double bonus;

	public int getTrainingRequired() {
		return trainingRequired;
	}

	public void setTrainingRequired(int trainingRequired) {
		this.trainingRequired = trainingRequired;
	}

	public int getTrainingReceived() {
		return trainingReceived;
	}

	public void setTrainingReceived(int trainingReceived) {
		this.trainingReceived = trainingReceived;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

	public TeamLeader(String name, String empNumber, String hireDate, int shift, double payRate, double bonus,
			int trainingRequired, int trainingReceived) {
		super(name, empNumber, hireDate, shift, payRate);
		this.bonus = bonus;
		this.trainingRequired = trainingRequired;
		this.trainingReceived = trainingReceived;
	}

	public String toString() {
		return String.format(
				"Employee Name: %s\n" + "Employee Number: %s\n" + "Employee Hire Date: %s\n" + "Employee Shift: %s\n"
						+ "Employee Pay Rate: %.2f\n" + "Employee Training Attended: %d\n"
						+ "Employee Training Required: %d\n" + "Employee Monthly Bonus: $%,.2f",
				this.getName(), this.getEmployeeNumber(), this.getHireDate(), this.getShift(), this.getPayRate(),
				this.getTrainingReceived(), this.getTrainingRequired(), this.getBonus());

	}

	public TeamLeader() {

	}

}
