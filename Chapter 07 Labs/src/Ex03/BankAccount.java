package Ex03;

public class BankAccount {

	private int[] bankAccount = new int[] { 5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 8080152, 4562555,
			5552012, 5050552, 7825887, 1250255, 1005231, 6545231, 3852085, 7576551, 7881200, 4581002 };

	public static void selectionSort(int[] array) {
		int startScan, index, minIndex, minValue;

		for (startScan = 0; startScan < (array.length - 1); startScan++) {
			minIndex = startScan;
			minValue = array[startScan];
			for (index = startScan + 1; index < array.length; index++) {
				if (array[index] < minValue) {
					minValue = array[index];
					minIndex = index;
				}
			}
			array[minIndex] = array[startScan];
			array[startScan] = minValue;
		}
	}

	public static int accountSearch(int[] array, int value) {
		int index, // Loop control variable
				element; // Element the value is found at
		boolean found; // Flag indicating search results

		// Element 0 is the starting point of the search.
		index = 0;

		// Store the default values for element and found.
		element = -1;
		found = false;

		// Search the array.
		while (!found && index < array.length) {
			// Does this element have the value?
			if (array[index] == value) {
				found = true; // Indicate the value is found.
				element = index; // Save the subscript of the value.
			}

			// Increment index so we can look at the next element.
			index++;
		}

		// Return either the subscript of the value (if found)
		// or -1 to indicate the value was not found.
		return element;
	}

}
