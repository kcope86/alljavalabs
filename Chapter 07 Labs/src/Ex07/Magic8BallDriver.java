package Ex07;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Magic8BallDriver {

	public static void main(String[] args) throws IOException {
		File file = new File("8ball.txt");
		Scanner inputFile = new Scanner(file);
		Scanner keyboard = new Scanner(System.in);

		String input;
		String[] magic8Ball;
		char repeat;

		magic8Ball = new String[11];
		for (int i = 0; i < magic8Ball.length && inputFile.hasNext(); i++) {
			magic8Ball[i] = inputFile.nextLine();
		}

		Magic8Ball eightBall = new Magic8Ball(magic8Ball);
		do {
			System.out.println("Ask the Magic 8 Ball a question...");
			input = keyboard.nextLine();
			System.out.println();
			System.out.println(eightBall.getFortune());
			System.out.println();
			System.out.println("Would you like to ask another question?");
			input = keyboard.nextLine().toUpperCase();
			repeat = input.charAt(0);
		} while (repeat != 'N');

	}

}
