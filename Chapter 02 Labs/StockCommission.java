
public class StockCommission
{
	public static void main(String[]args)
	{
			final int SHARES = 1000;
			final double PRICE_PER_SHARE = 25.50;
			final double COMMISSION = 0.02;
			
			
			
			System.out.printf("Before commission:  $%,.2f\n", SHARES * PRICE_PER_SHARE);
			System.out.printf("Commission will be: $%,.2f\n", (SHARES * PRICE_PER_SHARE) * COMMISSION);
			System.out.printf("After commission:   $%,.2f\n", (SHARES * PRICE_PER_SHARE) + ((SHARES * PRICE_PER_SHARE) * COMMISSION));
			
	}
}