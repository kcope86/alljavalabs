package Ex14;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileDisplay {

	File myFile;
	Scanner inputFile;

	public FileDisplay(String fName) throws IOException {
		myFile = new File(fName);
		inputFile = new Scanner(myFile);
	}

	public void displayHead() {
		for (int i = 0; i < 5; i++) {
			if (inputFile.hasNext()) {
				System.out.println(inputFile.nextLine());

			}

		}
		inputFile.close();
	}

	public void displayContents() {
		while (inputFile.hasNext()) {
			System.out.println(inputFile.nextLine());

		}
		inputFile.close();
	}

	public void displayWithLineNumbers() {
		int i = 1;
		while (inputFile.hasNext()) {
			System.out.println(i + " " + inputFile.nextLine());
			++i;
		}
		inputFile.close();
	}

}
