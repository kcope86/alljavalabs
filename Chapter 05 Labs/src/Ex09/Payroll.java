package Ex09;

public class Payroll {
	private int employeeID;
	private double grossPay;
	private double stateTax;
	private double federalTax;
	private double ficaWithHeld;

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public double getGrossPay() {
		return grossPay;
	}

	public void setGrossPay(double grossPay) {
		this.grossPay = grossPay;
	}

	public double getStateTax() {
		return stateTax;
	}

	public void setStateTax(double stateTax) {
		this.stateTax = stateTax;
	}

	public double getFederalTax() {
		return federalTax;
	}

	public void setFederalTax(double federalTax) {
		this.federalTax = federalTax;
	}

	public double getFicaWithHeld() {
		return ficaWithHeld;
	}

	public void setFicaWithHeld(double ficaWithHeld) {
		this.ficaWithHeld = ficaWithHeld;
	}

	public Payroll(int employeeID, double grossPay, double stateTax, double federalTax, double ficaWithHeld) {

		this.employeeID = employeeID;
		this.grossPay = grossPay;
		this.stateTax = stateTax;
		this.federalTax = federalTax;
		this.ficaWithHeld = ficaWithHeld;
	}

	public double calcNetPay() {
		double netPay = grossPay - stateTax - federalTax - ficaWithHeld;
		return netPay;

	}

}
