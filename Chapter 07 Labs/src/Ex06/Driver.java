package Ex06;

import java.util.Scanner;

public class Driver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		System.out.println("Welcome to the drivers test.");
		System.out.println("Answer questions with either A, B, C, or D.");

		char[] student = new char[20];

		char repeat = 0;

		do {
			String input;

			char answer;
			for (int i = 0; i < student.length; ++i) {

				do {
					System.out.printf("Question %d: ", i + 1);
					input = keyboard.nextLine().toUpperCase();
					answer = input.charAt(0);
					if (DriverExam.InputValidation(answer) < 0) {
						System.out.println("That is not a valid answer.");
					}
				} while (DriverExam.InputValidation(answer) < 0);
				student[i] = answer;
			}
			DriverExam exam1 = new DriverExam(student);

			if (exam1.passed()) {
				System.out.println("You Passed");
				System.out.println();
			} else {
				System.out.println("You did not pass");
				System.out.println();
			}

			System.out.printf("You answered %d question(s) correctly.\n" + "And %d questions incorrectly.\n",
					exam1.totalCorrect(), exam1.totalIncorrect());

			if (exam1.totalCorrect() < 20) {
				System.out.println("The question(s) you missed were:");
				for (int i = 0; i < exam1.totalIncorrect(); i++) {
					System.out.println(exam1.questionsMissed()[i]);
				}
			}
			if (!exam1.passed()) {
				System.out.println("Would you like to test again?");
				System.out.print("Enter Y for Yes or N for No: ");
				input = keyboard.nextLine().toUpperCase();
				repeat = input.charAt(0);
			}
		} while (repeat == 'Y');

	}

}
