package HOT0304;

public class Lawn {
	private int length;
	private int width;

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public Lawn(int length, int width) {
		this.length = length;
		this.width = width;
	}

	public Lawn() {

	}

	public int calcSquareFeet() {
		int feetSquared;
		feetSquared = length * width;

		return feetSquared;
	}

}
