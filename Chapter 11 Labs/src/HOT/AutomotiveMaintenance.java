package HOT;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AutomotiveMaintenance extends Application {
	Separator horizontal;
	Separator horizontal2;

	CheckBox oilChange;
	CheckBox lubeJob;
	CheckBox radiatorFlush;
	CheckBox transmissionFlush;
	CheckBox inspection;
	CheckBox muffReplace;
	CheckBox tireRotation;

	TextField hours;
	TextField parts;

	Label routinePrompt;
	Label nonRoutinePrompt;
	Label partsLbl;
	Label laborLbl;

	Label results;

	Button submit;
	Button exit;

	VBox checkBoxes;
	VBox nonRoutine;

	HBox partsRow;
	HBox laborRow;
	HBox buttons;
	HBox receipt;

	GridPane grid;

	private int laborHours;
	private double laborTotal;
	private double partsTotal;
	private double runnungTotal = 0;

	private final double OIL_CHANGE = 26;
	private final double LUBE_JOB = 18;
	private final double RADIATOR_FLUSH = 30;
	private final double TRANSMISSION_FLUSH = 80;
	private final double INSPECTION = 15;
	private final double REPLACE_MUFF = 100;
	private final double TIRE_ROTATION = 20;
	private final double LABOR_COST = 20;

	private String fees = "Total Charges: ";

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Ranken's Automotive Maintanance");
		oilChange = new CheckBox("Oil Change ($26.00)");
		lubeJob = new CheckBox("Lube Job ($18.00)");
		radiatorFlush = new CheckBox("Radiator Flush ($30.00)");
		transmissionFlush = new CheckBox("Transmission Flush ($80.00)");
		inspection = new CheckBox("Inspection ($15.00)");
		muffReplace = new CheckBox("Muffler Replacement ($100.00)");
		tireRotation = new CheckBox("Tire Rotation ($20.00)");
		hours = new TextField();
		parts = new TextField();
		parts.setText("0");
		hours.setText("0");
		results = new Label("Total Charges");

		routinePrompt = new Label("Routine Services");
		nonRoutinePrompt = new Label("Nonroutine Services");
		partsLbl = new Label("Parts Charges");
		laborLbl = new Label("Hours of Labor");
		submit = new Button("Calculate Charges");
		exit = new Button("Exit");
		horizontal = new Separator();
		horizontal.setPrefWidth(800);
		horizontal2 = new Separator();
		horizontal2.setPrefWidth(800);
		receipt = new HBox(20, results);

		checkBoxes = new VBox(routinePrompt, oilChange, lubeJob, radiatorFlush, transmissionFlush, inspection,
				muffReplace, tireRotation, horizontal);
		partsRow = new HBox(20, partsLbl, parts);
		laborRow = new HBox(20, laborLbl, hours);
		nonRoutine = new VBox(20, nonRoutinePrompt, partsRow, laborRow, horizontal2);
		buttons = new HBox(submit, exit);
		buttons.setAlignment(Pos.CENTER);

		grid = new GridPane();
		grid.add(checkBoxes, 0, 0);
		grid.add(nonRoutine, 0, 1);
		grid.add(buttons, 0, 2);
		grid.add(results, 0, 3);
		grid.setAlignment(Pos.BASELINE_LEFT);
		grid.setPadding(new Insets(20));

		Scene scene = new Scene(grid, 400, 400);
		primaryStage.setScene(scene);
		primaryStage.show();

		submit.setOnAction(e -> {
			runnungTotal = 0;
			boolean isValid = true;
			parts.setStyle("-fx-text-inner-color: black;");
			hours.setStyle("-fx-text-inner-color: black;");

			try {
				partsTotal = Double.parseDouble(parts.getText());
			} catch (NumberFormatException ex) {
				parts.setStyle("-fx-text-inner-color: red;");
				parts.setText("Invalid Entry");
				isValid = false;
			}
			try {
				laborHours = Integer.parseInt(hours.getText());
			} catch (NumberFormatException ex) {
				hours.setStyle("-fx-text-inner-color: red;");
				hours.setText("Invalid Entry");
				isValid = false;
			}

			if (isValid == true) {
				if (oilChange.isSelected()) {
					runnungTotal += OIL_CHANGE;

				}
				if (lubeJob.isSelected()) {
					runnungTotal += LUBE_JOB;

				}
				if (radiatorFlush.isSelected()) {
					runnungTotal += RADIATOR_FLUSH;

				}
				if (transmissionFlush.isSelected()) {
					runnungTotal += TRANSMISSION_FLUSH;

				}
				if (inspection.isSelected()) {
					runnungTotal += INSPECTION;

				}
				if (muffReplace.isSelected()) {
					runnungTotal += REPLACE_MUFF;

				}
				if (tireRotation.isSelected()) {
					runnungTotal += TIRE_ROTATION;

				}

				laborTotal = laborHours * LABOR_COST;

				results.setText(String.format("Total Charges: $%,.2f", runnungTotal + partsTotal + laborTotal));
				parts.setText("0");
				hours.setText("0");
			}

		});

		exit.setOnAction(e -> {
			primaryStage.close();

		});

	}

}
