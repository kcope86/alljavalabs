package Ex06;

import java.util.Scanner;

public class PopulationDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		double startingPopulation;
		double percentIncrease;
		int daysOfGrowth;
		do {
			System.out.print("enter the starting size of the population: ");
			startingPopulation = keyboard.nextDouble();
			System.out.println();
			if (startingPopulation < 2)
				System.out.println("Starting population must be greater than 1.");
		} while (startingPopulation < 2);
		do {
			System.out.print("enter the average daily population increase: ");
			percentIncrease = keyboard.nextDouble();
			System.out.println();
			if (percentIncrease < 0)
				System.out.println("rate of increase cannot be negative.");
		} while (percentIncrease < 0);
		do {
			System.out.print("How many days will the population grow: ");
			daysOfGrowth = keyboard.nextInt();
			System.out.println();
			if (daysOfGrowth < 1)
				System.out.println("population must grow for at least 1 day.");
		} while (daysOfGrowth < 1);

		Population civilization1 = new Population(startingPopulation, percentIncrease, daysOfGrowth);
		civilization1.getIncrease();

	}

}
