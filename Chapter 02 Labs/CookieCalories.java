import java.util.Scanner;
public class CookieCalories
{
			

	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			
			int cookies;
			final int CALORIES_PER_COOKIE;
			int totalCalories;
			String input;
			
			System.out.print("How many cookies did you eat? ");
			cookies = keyboard.nextInt();
			//keyboard.nextLine();
			CALORIES_PER_COOKIE = 75;
			totalCalories = cookies * CALORIES_PER_COOKIE;
			System.out.println(cookies + " cookies is " + totalCalories + " calories.");
			System.out.println("Would you like to check another serving size?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
		}while (repeat == 'Y' || repeat == 'y');
		
		
	}
}