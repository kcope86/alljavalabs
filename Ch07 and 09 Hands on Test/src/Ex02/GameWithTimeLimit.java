package Ex02;

//inherits from Game
public class GameWithTimeLimit extends Game {
	// fields
	private int minutes;

	// getter
	public int getMinutes() {
		return minutes;
	}

	// setter
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	// constructor
	public GameWithTimeLimit(String gameName, int maxPlayers, int minutes) {
		super(gameName, maxPlayers);
		this.minutes = minutes;
	}

	// toString method
	public String toString() {
		String str;
		str = String.format("Game Name: %s\n" + "Max Players %d\n" + "Time Limit: %d", gameName, maxPlayers, minutes);
		return str;

	}

}
