package Ex09;

public class SpeedOfSound {
	private int distance;

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public SpeedOfSound(int distance) {
		this.distance = distance;
	}

	public double getSpeedInAir(int distance) {
		double time = (float) distance / 1100;
		return time;
	}

	public double getSpeedInWater(int distance) {
		double time = (float) distance / 4900;
		return time;
	}

	public double getSpeedInSteel(int distance) {
		double time = (float) distance / 16400;
		return time;
	}
}
