import java.util.Scanner;
public class FastFood
{
	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			String input;
			final double HAMBURGER = 1.25;
			final double CHEESEBURGER = 1.50;
			final double SODA = 1.95;
			final double FRIES = 0.95;
			int numberOfHamburgers = 0;
			int numberOfCheeseburgers = 0;
			int numberOfSodas = 0;
			int numberOfFries = 0;
			String name;
			
			System.out.print("How many hamburgers would you like? ");
			numberOfHamburgers += keyboard.nextInt();
			System.out.print("How many Cheeseburgers would you like? ");
			numberOfCheeseburgers += keyboard.nextInt();
			System.out.print("How many sodas would you like? ");
			numberOfSodas += keyboard.nextInt();
			System.out.print("How many orders of fries would you like? ");
			numberOfFries += keyboard.nextInt();
			keyboard.nextLine();
			System.out.print("What is the name for the order? ");
			name = keyboard.nextLine();
			
			System.out.printf("Order for %s: ", name);
			System.out.printf("%,d Hamburger(s) - $%,.2f", numberOfHamburgers, numberOfHamburgers * HAMBURGER );
			System.out.printf("%,d Cheeseburger(s) - $%,.2f", numberOfCheeseburgers, numberOfCheeseburgers * CHEESEBURGER );
			System.out.printf("%,d Soda(s) - $%,.2f", numberOfSodas, numberOfSodas * SODA );
			System.out.printf("%,d Fries(s) - $%,.2f", numberOfFries, numberOfFries * FRIES );
			System.out.printf("Total: %,.2f", (numberOfHamburgers * HAMBURGER) + (numberOfCheeseburgers * CHEESEBURGER ) + (numberOfSodas * SODA ) + (numberOfFries * FRIES ));
			 
			
			
			
			
			
			
			System.out.println("Would you like to order again?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
			System.out.println();
		}while (repeat == 'Y' || repeat == 'y');
		
	}
}