package Ex03;

import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TipCalculator extends Application {

	private double mealCost;
	private double tipAmmount;
	private final double GRATUITY = .18;
	private final double TAX = .07;
	private double taxTotal;
	double foodTotal;

	TextField total;
	Label prompt;
	Label output;
	Button submit;
	private static DecimalFormat cash = new DecimalFormat("0.00");

	public static void main(String[] args) {
		launch();

	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Restaurant Tip Calculator.");

		prompt = new Label("Enter your meal cost: ");
		total = new TextField();
		total.setPromptText("0.00");
		submit = new Button("Submit");
		output = new Label("");

		VBox vBox = new VBox(prompt, total, submit, output);
		vBox.setPadding(new Insets(50));
		vBox.setAlignment(Pos.TOP_CENTER);
		Scene scene = new Scene(vBox, 400, 400);
		primaryStage.setScene(scene);
		primaryStage.show();

		// Register an event handler.
		submit.setOnAction(e -> {
			try {
				foodTotal = Double.parseDouble(total.getText());
				tipAmmount = foodTotal * GRATUITY;
				taxTotal = foodTotal * TAX;
				mealCost = foodTotal + tipAmmount + taxTotal;

				output.setText("Your bill: \n" + "Food subtotal: $" + cash.format(foodTotal) + "\n" + "Tax: $"
						+ cash.format(taxTotal) + "\n" + "18% tip: $" + cash.format(tipAmmount) + "\n"
						+ "Total after tax and tip: $" + cash.format(mealCost));

			} catch (NumberFormatException ex) {
				output.setText("must be in number format (example - 10.99 or 5)");

			}
		});

	}

}
