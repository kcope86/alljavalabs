package Ex01;

public class Employee {
	private String name;
	private String employeeNumber;
	private String hireDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {

		this.employeeNumber = employeeNumber;

	}

	public String getHireDate() {
		return hireDate;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}

	public Employee(String name, String employeeNumber, String hireDate) {

		this.name = name;
		this.employeeNumber = employeeNumber;
		this.hireDate = hireDate;
	}

	public Employee() {

	}

	public static boolean isValid(String empNumber) {
		boolean goodSoFar = true; // Flag
		int index = 0; // Loop control variable

		// Is the string the correct length?
		if (empNumber.length() != 5)
			goodSoFar = false;

		// Test the first three characters for numbers.
		while (goodSoFar && index < 3) {
			if (!Character.isDigit(empNumber.charAt(index)))
				goodSoFar = false;
			index++;
		}

		// Test the fourth character for dash.
		while (goodSoFar && index < 4) {
			if (empNumber.charAt(index) != '-')
				goodSoFar = false;
			index++;
		}
		// Test for the range of letters in fifth character
		while (goodSoFar && index < 5) {
			int a = (int) 'A';
			int m = (int) 'M';
			if (!Character.isLetter(empNumber.charAt(index)) || empNumber.charAt(index) < a
					|| empNumber.charAt(index) > m)
				goodSoFar = false;
			index++;
		}

		// Return the results
		return goodSoFar;
	}

	public String toString() {
		return String.format("Employee Name: %s\n" + "Employee Number: %s\n" + "Employee Hire Date: %s\n"
				+ "Employee Shift: %s\n" + "Employee Pay Rate: %.2f\n", this.getName(), this.getEmployeeNumber(),
				this.getHireDate());

	}
}
