package Ex07;

public class RetailItem {

	private String description;
	private int itemNumber;
	private CostData cost;

	public RetailItem(String desc, int itemNum, double wholesale, double retail) {
		description = desc;
		itemNumber = itemNum;
		cost = new CostData();
		cost.setRetail(retail);
		cost.setWholesale(wholesale);

	}

	public String toString() {
		String str;
		str = String.format(
				"Description: %s\n" + "Item Number: %d\n" + "Wholesale cost: $%,.2f\n" + "Retail Price: $%,.2f\n",
				description, itemNumber, cost.getWholesale(), cost.getRetail());
		return str;
	}

	private class CostData {
		public double wholesale, retail;

		public CostData(double w, double r) {
			wholesale = w;
			retail = r;
		}

		public CostData() {

		}

		public double getWholesale() {
			return wholesale;
		}

		public void setWholesale(double wholesale) {
			this.wholesale = wholesale;
		}

		public double getRetail() {
			return retail;
		}

		public void setRetail(double retail) {
			this.retail = retail;
		}

	}

}
