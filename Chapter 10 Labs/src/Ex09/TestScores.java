package Ex09;

import java.io.Serializable;

public class TestScores implements Serializable {

	private int[] exam;

	public int[] getExam() {
		return exam;
	}

	public void setExam(int[] exam, int size) {
		for (int i = 0; i < exam.length; i++) {
			this.exam[i] = exam[i];

		}
	}

	public TestScores(int[] exam) {
		this.exam = new int[5];
		for (int i = 0; i < 5; i++) {
			if (exam[i] < 0 || exam[i] > 100) {
				throw new IllegalArgumentException("Score must be greater than zero or less than 100.");
			}
			this.exam[i] = exam[i];

		}
	}

	public double getAverage() {
		double average = 0;
		for (int i = 0; i < exam.length; i++) {
			average += exam[i];
		}
		return average / exam.length;
	}

	public double getScore(int i) {

		return exam[i];
	}

}
