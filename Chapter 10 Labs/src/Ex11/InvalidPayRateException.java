package Ex11;

public class InvalidPayRateException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidPayRateException(String msg) {
		super(msg);
	}
}
