package Ex10;

import java.text.DecimalFormat;

public class ParkingTicket {
	private ParkedCar car;
	private PoliceOfficer cop;
	private ParkingMeter meter;
	private double fineAmmount;

	DecimalFormat cash = new DecimalFormat("#0.##");

	public ParkingTicket(ParkedCar car, PoliceOfficer cop, ParkingMeter meter) {
		this.car = car;
		this.cop = cop;
		this.meter = meter;

	}

	public void getCar() {
		System.out.println("Make: " + car.getMake());
		System.out.println("Model: " + car.getModel());
		System.out.println("Color: " + car.getColor());
		System.out.println("Plate #: " + car.getLicenseNumber());
	}

	public double calcFineAmmount() {
		final double BASE_FEE = 25.00;
		final double HOURLY_COST = 10.00;
		int extraHours = 0;
		int extraMinutes = 0;
		fineAmmount = 0;

		fineAmmount = BASE_FEE;
		extraMinutes = car.getMinutesParked() - meter.getMinutesPurchased();

		extraHours = extraMinutes / 60;

		if (extraHours == 1 && extraMinutes % 60 == 0) {
			fineAmmount = (BASE_FEE);
		} else if (extraHours == 1 && extraMinutes % 60 != 0) {
			fineAmmount += (extraHours * HOURLY_COST);
		} else if (extraHours > 1 && extraMinutes % 60 == 0) {
			fineAmmount += (--extraHours * HOURLY_COST);

		} else if (extraHours > 1 && extraMinutes % 60 != 0) {
			fineAmmount += (extraHours * HOURLY_COST);

		}

		return fineAmmount;

	}

	public String toString() {
		return "Ticket summary for \n" + car.getColor() + " - " + car.getMake() + " - " + car.getModel() + "\n"
				+ "License plate #: " + car.getLicenseNumber() + "\n" + "Minutes Purchased: "
				+ meter.getMinutesPurchased() + "\n" + "Minutes Parked: " + car.getMinutesParked() + "\n"
				+ "Minutes over: " + (car.getMinutesParked() - meter.getMinutesPurchased()) + "\n" + "Total Fine: $"
				+ cash.format(calcFineAmmount()) + "\n" + "Issuing Officer: " + cop.getName() + "\n" + "Badge Number: "
				+ cop.getBadgeNumber() + "\n" + "\n";

	}

}
