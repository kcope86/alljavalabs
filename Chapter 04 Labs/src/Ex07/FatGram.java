package Ex07;

public class FatGram {
	private double caloriesFromFat;
	private double gramsOfFat;
	private double totalCalories;
	private double percentFat;

	public double getCaloriesFromFat() {
		return caloriesFromFat;
	}

	public void setCaloriesFromFat(double caloriesFromFat) {
		this.caloriesFromFat = caloriesFromFat;
	}

	public double getGramsOfFat() {
		return gramsOfFat;
	}

	public void setGramsOfFat(double gramsOfFat) {
		this.gramsOfFat = gramsOfFat;
	}

	public double getTotalCalories() {
		return totalCalories;
	}

	public void setTotalCalories(double totalCalories) {
		this.totalCalories = totalCalories;
	}

	public double getPercentFat() {
		return percentFat;
	}

	public void setPercentFat(double percentFat) {
		this.percentFat = percentFat;
	}

	public double calcFatCalPercentage() {

		percentFat = (caloriesFromFat / totalCalories) * 100;
		return percentFat;
	}

	public FatGram(double gramsOfFat, double totalCalories) {

		this.gramsOfFat = gramsOfFat;
		this.totalCalories = totalCalories;
		caloriesFromFat = gramsOfFat * 9;

	}

	public String isLowFat(double percentFat) {

		if (percentFat < 30) {
			return "low in fat";
		} else {
			return "high in fat";
		}
	}

}
