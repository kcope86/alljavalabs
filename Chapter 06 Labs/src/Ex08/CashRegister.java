package Ex08;

public class CashRegister {

	private RetailItem item1;
	private int quantity;

	public CashRegister(RetailItem item, int quantity) {
		item1 = item;
		this.quantity = quantity;

	}

	public double getSubtotal() {
		double subTotal;
		subTotal = item1.getRetail() * quantity;
		return subTotal;
	}

	public double getTax() {
		double salesTax;
		salesTax = (item1.getRetail() * quantity) * .06;
		return salesTax;

	}

	public double getTotal() {
		double total;
		total = getSubtotal() + getTax();
		return total;
	}
}
