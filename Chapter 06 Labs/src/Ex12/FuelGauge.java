package Ex12;

public class FuelGauge {

	private int fuelLevel;

	public boolean hasFuel() {
		if (fuelLevel < 1) {
			return false;
		} else {
			return true;
		}
	}

	public FuelGauge() {

		fuelLevel = 0;

	}

	public int getFuelLevel() {
		return fuelLevel;
	}

	public void addGas() {

		fuelLevel++;

	}

	public void fillUp() {
		for (int i = 0; i < 15; i++) {
			fuelLevel++;

		}
	}

	public void burnGas() {

		fuelLevel--;

	}
}
