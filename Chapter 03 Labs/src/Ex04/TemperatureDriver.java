package Ex04;

import java.util.Scanner;

public class TemperatureDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		System.out.println("Please enter a Fahrenheit temperature: ");
		double temp = keyboard.nextDouble();

		Temperature t1 = new Temperature(temp);

		System.out.printf("You entered %.2f", temp);
		System.out.println();
		System.out.printf("in Celsius, that temperature is %.2f", t1.getCelsius());
		System.out.println();
		System.out.printf("in Kelvin, that temperature is %f", t1.getKelvin());
		System.out.println();
	}

}
