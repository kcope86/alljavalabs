package Ex05;

import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		Payroll emp1 = new Payroll();

		System.out.print("Enter your name: ");
		try {
			emp1.setName(keyboard.nextLine());
		} catch (InvalidNameException ex) {
			System.out.println(ex.toString());
		}

		System.out.println();
		System.out.print("Enter your ID number: ");
		try {
			emp1.setIdNumber(keyboard.nextInt());
		} catch (InvalidIdException ex) {
			System.out.println(ex.toString());
		}
		System.out.println();
		System.out.print("What is your hourly rate: ");
		try {
			emp1.setHourlyRate(keyboard.nextDouble());
		} catch (InvalidHourlyPayException ex) {
			System.out.println(ex.toString());
		}
		System.out.println();
		System.out.print("How many hours did you work: ");
		try {
			emp1.setHoursWorked(keyboard.nextDouble());

		} catch (InvalidHoursException ex) {
			System.out.println(ex.toString());
		}
		System.out.println();

		System.out.println("Your name is: " + emp1.getName());
		System.out.println();
		System.out.println("Your ID number is: " + emp1.getIdNumber());
		System.out.println();
		System.out.println("Your hourly rate is: " + emp1.getHourly());
		System.out.println();
		System.out.println("You worked: " + emp1.getHours() + " hours");
		System.out.println();
		System.out.printf("Your gross pay is: $%.2f", emp1.calcGrossPay(emp1.getHours(), emp1.getHourly()));

	}

}
