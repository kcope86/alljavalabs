package Ex07;

import java.util.Scanner;

public class AverageRainfall {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		int years;
		double monthlyRain;
		double totalRain = 0;
		int months = 0;
		double avgRainfall;

		do {
			System.out.print("How many years would you like to check: ");
			years = keyboard.nextInt();
			if (years < 1) {
				System.out.println("Years must be greater than 0.");
			}
		} while (years < 1);
		for (int y = 1; y <= years; y++) {
			for (int m = 1; m <= 12; m++) {
				do {
					System.out.printf("Enter the number of inches of rainfall for year %d month %d ", y, m);
					monthlyRain = keyboard.nextDouble();
				} while (monthlyRain < 0);
				totalRain += monthlyRain;
				++months;
			}
		}
		System.out.printf("total months: %d\n", months);

		System.out.printf("total rainfall for the past %d years is: %.2f\n", years, totalRain);
		System.out.printf("average rainfall per month is: %.2f\n", totalRain / (float) months);

	}

}
