package Ex04;

public class LandTract {

	private double length;
	private double width;

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double calcArea() {
		return length * width;
	}

	public boolean equals(LandTract t2) {

		boolean isEqual;

		if (this.length == t2.length && this.width == t2.width) {
			isEqual = true;
		} else {
			isEqual = false;
		}
		return isEqual;
	}

	public LandTract(double length, double width) {

		this.length = length;
		this.width = width;
	}

	public String toString() {
		return "The length is " + length + "\nThe width is " + width + "\nThe area is  " + calcArea() + "\n";
	}

}
