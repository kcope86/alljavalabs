package Ex06;

import java.util.Scanner;

public class ShippingChargesDriver {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		double weight;
		int miles;
		System.out.print("Enter the weight in kg of your shipment: ");
		weight = keyboard.nextDouble();
		System.out.print("How many miles are you shipping this order: ");
		miles = keyboard.nextInt();
		System.out.println();

		ShippingCharges shipment1 = new ShippingCharges(weight, miles);

		shipment1.calcWeightPrice();
		shipment1.calcMilePrice();

		System.out.printf("The rate to ship a %.2fKG Package is $%.2f per 500 miles.\n", shipment1.getWeight(),
				shipment1.getRate());
		System.out.printf("You need to ship the package %,d miles.\n", miles);
		System.out.printf("Your total will be $%,.2f", shipment1.calcTotalCost());

		keyboard.close();
	}

}
