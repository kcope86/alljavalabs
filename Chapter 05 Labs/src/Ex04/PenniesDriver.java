package Ex04;

import java.util.Scanner;

public class PenniesDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int days;
		double cents = .01;
		double total = .00;
		do {
			System.out.print("How many days did you work? ");
			days = keyboard.nextInt();
			if (days < 1) {
				System.out.println("Must enter a number greater than 0");
			}
		} while (days < 1);
		// System.out.printf("Day 1 you made $%.2f\n", cents);

		for (int x = 1, y = 2; x <= days; ++x) {

			System.out.printf("Day %d you made $%,.2f\n", x, cents);
			total += cents;
			cents *= y;

		}
		System.out.printf("The total earned is $%,.2f", total);

	}

}
