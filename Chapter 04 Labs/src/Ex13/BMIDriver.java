package Ex13;

import java.util.Scanner;

public class BMIDriver {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter your weight: ");
		double weight = keyboard.nextDouble();
		System.out.print("Enter your height: ");
		double height = keyboard.nextDouble();
		BMI person1 = new BMI(weight, height);
		System.out.println();
		System.out.printf("Your body mass index is %.2f\n", person1.getBMI());
		System.out.printf("That is considered %s ", person1.isHealthy());

		keyboard.close();
	}
}
