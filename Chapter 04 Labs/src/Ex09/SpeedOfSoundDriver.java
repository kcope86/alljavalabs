package Ex09;

import java.util.Scanner;

public class SpeedOfSoundDriver {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		String input;
		char choice;
		int distance;
		do {
			System.out.println("Which substance is sound travelling through?");
			System.out.println("Enter 1 for air");
			System.out.println("Enter 2 for water");
			System.out.println("Enter 3 for steel");
			input = keyboard.nextLine();
			choice = input.charAt(0);
		} while (choice != '1' && choice != '2' && choice != '3');
		if (choice == '1') {
			System.out.print("How many feet did the sound travel? ");
			distance = keyboard.nextInt();
			SpeedOfSound speed1 = new SpeedOfSound(distance);
			System.out.printf("it took sound %.2f seconds to travel that distance through air",
					speed1.getSpeedInAir(distance));

		}
		if (choice == '2') {
			System.out.print("How many feet did the sound travel? ");
			distance = keyboard.nextInt();
			SpeedOfSound speed1 = new SpeedOfSound(distance);
			System.out.printf("it took sound %.2f seconds to travel that distance through water",
					speed1.getSpeedInWater(distance));
		}

		if (choice == '3') {
			System.out.print("How many feet did the sound travel? ");
			distance = keyboard.nextInt();
			SpeedOfSound speed1 = new SpeedOfSound(distance);
			System.out.printf("it took sound %.2f seconds to travel that distance through water",
					speed1.getSpeedInSteel(distance));
		}
	}
}
