package Ex12;

import java.util.Scanner;

public class BarChart {

	public static String calcBars(double sales) {
		String bars = "";
		for (int x = 0; x < sales; ++x)
			bars += "*";

		return bars;
	}

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int store1;
		int store2;
		int store3;
		int store4;
		int store5;
		String oneStar;

		System.out.print("Enter the sales for store 1: ");
		store1 = keyboard.nextInt() / 100;
		System.out.print("Enter the sales for store 2: ");
		store2 = keyboard.nextInt() / 100;
		System.out.print("Enter the sales for store 3: ");
		store3 = keyboard.nextInt() / 100;
		System.out.print("Enter the sales for store 4: ");
		store4 = keyboard.nextInt() / 100;
		System.out.print("Enter the sales for store 5: ");
		store5 = keyboard.nextInt() / 100;

		System.out.println("Store 1: " + BarChart.calcBars(store1));
		System.out.println("Store 2: " + BarChart.calcBars(store2));
		System.out.println("Store 3: " + BarChart.calcBars(store3));
		System.out.println("Store 4: " + BarChart.calcBars(store4));
		System.out.println("Store 5: " + BarChart.calcBars(store5));

	}

}
