package Ex13;

public class Player {

	private String name;
	private int score;

	public Player(String name) {

		this.name = name;
		score = 50;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int calcScore(int die1, int die2) {
		if (score - (die1 + die2) < 1) {
			score += die1 + die2;
		} else if (score - (die1 + die2) > 1) {
			score -= die1 + die2;
		} else {
			score = 1;
		}
		return score;

	}
}
