package Ex04;

import java.util.Scanner;

public class SoftwareDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		char repeat;
		do {
			double quantity;

			String input;
			System.out.print("How many packages would you like to buy? ");
			quantity = keyboard.nextDouble();
			keyboard.nextLine();
			Software customer1 = new Software(quantity);

			System.out.printf("After a %d percent discount\n", customer1.calcDiscount(customer1.getQuantity()));
			System.out.printf("Your total cost will be $%,.2f\n", customer1.calcTotalCost(customer1.getQuantity()));

			System.out.println("Would you like to buy a different ammount? ");
			System.out.println("Enter Y for yes, or N for no: ");
			input = keyboard.nextLine();
			repeat = input.charAt(0);

		} while (repeat == 'Y' || repeat == 'y');

		keyboard.close();

	}

}
