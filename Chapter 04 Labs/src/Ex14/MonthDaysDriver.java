package Ex14;

import java.util.Scanner;

public class MonthDaysDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter a month (1 - 12): ");
		int month = keyboard.nextInt();
		System.out.print("Enter a year: ");
		int year = keyboard.nextInt();
		MonthDays query1 = new MonthDays(month, year);
		System.out.printf("%s %d has %d days", query1.getStringMonth(), year, query1.getNumberOfDays());

	}

}
