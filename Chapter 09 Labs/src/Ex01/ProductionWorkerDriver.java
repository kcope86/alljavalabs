package Ex01;

import java.util.Scanner;

public class ProductionWorkerDriver {

	public static void main(String[] args) {
		String name;
		String empNumber = "XXX-X";
		String input;
		String hireDate;
		int shift;
		double payRate;

		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter the employee name: ");
		name = keyboard.nextLine();
		System.out.println();
		do {
			System.out.print("Enter employee number: ");
			input = keyboard.nextLine().toUpperCase();
			if (Employee.isValid(input)) {
				empNumber = input;
			} else {
				System.out.println("Invalid format for employee number.");
			}

		} while (!Employee.isValid(input));
		System.out.print("Enter Date Hired: ");
		hireDate = keyboard.nextLine();
		System.out.print("Enter pay rate: ");
		payRate = keyboard.nextDouble();
		do {
			System.out.print("Enter shift: ");
			shift = keyboard.nextInt();
			keyboard.nextLine();
			if (shift < 1 || shift > 2) {
				System.out.println("Employee must be shift 1 or shift 2.");
			}
		} while (shift < 1 || shift > 2);
		System.out.println();
		ProductionWorker worker1 = new ProductionWorker();
		worker1.setName(name);
		worker1.setEmployeeNumber(empNumber);
		worker1.setHireDate(hireDate);
		worker1.setShift(shift);
		worker1.setPayRate(payRate);

		System.out.println("Using Mutators");
		System.out.println(worker1.toString());
		System.out.println();

		ProductionWorker worker2 = new ProductionWorker(name, empNumber, hireDate, shift, payRate);
		System.out.println("Using constructor with fields");
		System.out.println(worker2.toString());
	}

}
