package Ex08;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileDecryption {

	public static void decrypt(String file) throws IOException {
		boolean endOfFile = false;
		String nameEncrypted;
		String nameDecrypted = "";
		int character;
		char ch;

		DataInputStream inputFile = new DataInputStream(new FileInputStream(file));
		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream("decryptedData.dat"));

		while (!endOfFile) {
			try {
				nameEncrypted = inputFile.readUTF();

				for (int i = 0; i < nameEncrypted.length(); i++) {
					character = (int) nameEncrypted.charAt(i) - 25;
					ch = (char) character;
					nameDecrypted += ch;

				}
				outputFile.writeUTF(nameDecrypted);

				nameDecrypted = "";

			} catch (EOFException ex) {
				endOfFile = true;

			}

		}

		inputFile.close();
		outputFile.close();

	}
}
