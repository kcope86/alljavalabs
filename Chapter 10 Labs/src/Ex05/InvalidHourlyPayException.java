package Ex05;

public class InvalidHourlyPayException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidHourlyPayException(String msg) {
		super(msg);
	}

}
