package Ex05;

public class BankAccount {
	// private int[] bankAccount = new int[18];

	/*
	 * public BankAccount(int[] array) { for (int i = 0; i < array.length; i++) {
	 * bankAccount[i] = array[i]; }
	 * 
	 * }
	 */

	public static int accountSearch(int[] array, int value) {
		int index;
		int element;
		boolean found;

		index = 0;

		element = -1;
		found = false;

		while (!found && index < array.length) {
			if (array[index] == value) {
				found = true;
				element = index;
			}

			index++;
		}

		return element;
	}
}
