import java.util.Scanner;
public class MilesPerGallon
{
	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			String input;
			double milesDriven;
			double gallonsConsumed;
			double Mpg;
			
			System.out.print("How many miles did you drive? ");
			milesDriven = keyboard.nextDouble();
			System.out.print("How many gallons did you use? ");
			gallonsConsumed = keyboard.nextDouble();
			Mpg = milesDriven / gallonsConsumed;
			System.out.println("You drove " + milesDriven + " miles and used " + gallonsConsumed + " gallons. that is " + Mpg + " Miles per gallon.");
			
			
			
			
			System.out.println("Would you like to check different amounts?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
		}while (repeat == 'Y' || repeat == 'y');
		
	}
}