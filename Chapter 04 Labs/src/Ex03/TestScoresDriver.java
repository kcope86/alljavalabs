package Ex03;

import java.util.Scanner;

public class TestScoresDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		char repeat;
		do {
			double test1;
			double test2;
			double test3;

			String input;
			System.out.print("Enter score for test 1: ");
			test1 = keyboard.nextDouble();
			System.out.print("Enter score for test 2: ");
			test2 = keyboard.nextDouble();
			System.out.print("Enter score for test 3: ");
			test3 = keyboard.nextDouble();
			keyboard.nextLine();

			TestScores student1 = new TestScores(test1, test2, test3);

			System.out.printf("The average of your scores is %.2f\n" + "Your grade is a(n): %s.\n",
					student1.getAverage(), student1.calcLetterGrade());
			System.out.println();

			System.out.println("Would you like to enter different scores?");
			System.out.println("Enter Y for yes, or N for no: ");
			input = keyboard.nextLine();
			repeat = input.charAt(0);

		} while (repeat == 'Y' || repeat == 'y');

		keyboard.close();
	}

}
