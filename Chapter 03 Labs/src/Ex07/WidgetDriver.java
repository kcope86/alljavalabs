package Ex07;
import java.util.Scanner;
public class WidgetDriver {

	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		int widgets = 0;
		
		System.out.print("How many widgets would you like made? ");
		widgets = keyboard.nextInt();
		
		Widget w1 = new Widget(widgets);
		
		
		System.out.println("Your order will take " + w1.checkDays() + " days.");
		keyboard.close();
	}

}
