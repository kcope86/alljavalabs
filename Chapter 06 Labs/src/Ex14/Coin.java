package Ex14;

import java.util.Random;

public class Coin {

	private String sideUp;

	public Coin() {
		toss();
	}

	public void toss() {
		Random rand = new Random();
		int side = rand.nextInt(2) + 1;
		if (side == 1) {
			sideUp = "Heads";
		} else if (side == 2) {
			sideUp = "Tails";
		}
	}

	public String getSideUp() {
		return sideUp;
	}

}
