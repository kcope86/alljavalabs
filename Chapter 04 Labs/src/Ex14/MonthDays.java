package Ex14;

public class MonthDays {
	private int month;
	private int year;
	private String stringMonth;

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public MonthDays(int month, int year) {
		this.month = month;
		this.year = year;
	}

	public int getNumberOfDays() {

		switch (month) {
		case 1:
			return 31;
		case 2:
			if (year % 100 == 0 && year % 400 == 0) {
				return 29;
			} else if (year % 100 != 0 && year % 4 == 0) {
				return 29;
			} else {
				return 28;
			}
		case 3:
			return 31;
		case 4:
			return 30;
		case 5:
			return 31;
		case 6:
			return 30;
		case 7:
			return 31;
		case 8:
			return 31;
		case 9:
			return 30;
		case 10:
			return 31;
		case 11:
			return 30;
		case 12:
			return 31;
		default:
			return 28;

		}

	}

	public String getStringMonth() {
		switch (month) {
		case 1:
			return "January";
		case 2:
			return "February";
		case 3:
			return "March";
		case 4:
			return "April";
		case 5:
			return "May";
		case 6:
			return "June";
		case 7:
			return "July";
		case 8:
			return "August";
		case 9:
			return "Septmber";
		case 10:
			return "October";
		case 11:
			return "November";
		case 12:
			return "December";
		default:
			return "Not a valid month.";

		}

	}

	public void setStringMonth(String stringMonth) {
		this.stringMonth = stringMonth;
	}
}
