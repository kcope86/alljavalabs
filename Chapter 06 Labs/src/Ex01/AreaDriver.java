package Ex01;

public class AreaDriver {

	public static void main(String[] args) {
		System.out.println("Area is " + Area.calcArea(5));
		System.out.println("Area is " + Area.calcArea(5.0, 10.0));
		System.out.println("Area is " + Area.calcArea(5f, 10f));

	}

}
