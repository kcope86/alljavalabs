package Ex02;

public class InvalidTestScore extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidTestScore(String msg) {
		super(msg);
	}

}
