package Ex03;

public class TestScores {
	private double test1;
	private double test2;
	private double test3;

	public TestScores(double score1, double score2, double score3) {
		test1 = score1;
		test2 = score2;
		test3 = score3;

	}

	public double getTest1() {
		return test1;
	}

	public double getTest2() {
		return test2;
	}

	public double getTest3() {
		return test3;
	}

	public double getAverage() {
		return (test1 + test2 + test3) / 3;
	}

	public String calcLetterGrade() {
		double avg = (test1 + test2 + test3) / 3;
		String grade;
		if (avg >= 90 && avg <= 100) {
			grade = "A";
		} else if (avg >= 80 && avg <= 89) {
			grade = "B";
		} else if (avg >= 70 && avg <= 79) {
			grade = "C";
		} else if (avg >= 60 && avg <= 69) {
			grade = "D";
		} else {
			grade = "F";
		}

		return grade;
	}

}
