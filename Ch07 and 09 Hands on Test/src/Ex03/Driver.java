package Ex03;

public class Driver {

	public static void main(String[] args) {

		Vehicle car = new Car();
		car.accelerate();
		car.accelerate();
		car.accelerate();

		System.out.println(car.getSpeed());

		Vehicle truck = new Truck();
		truck.accelerate();
		truck.accelerate();
		truck.accelerate();

		System.out.println(truck.getSpeed());

	}

}
