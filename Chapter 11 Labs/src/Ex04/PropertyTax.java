package Ex04;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PropertyTax extends Application {

	TextField input;
	Label prompt;
	Label output;
	Button submit;

	private double actualValue;
	private double assessmentValue;
	private final double ASSESSMENT = .60;
	private final double PROPERTY_TAX = .64;
	private double taxTotal;
	double Total;

	public static void main(String[] args) {
		launch();

	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		input = new TextField();
		submit = new Button("Submit");
		prompt = new Label("Enter the actual value of your property:");
		output = new Label("");

		VBox vBox = new VBox(10, prompt, input, submit, output);
		vBox.setPadding(new Insets(50));
		vBox.setAlignment(Pos.TOP_CENTER);
		Scene scene = new Scene(vBox, 400, 400);
		primaryStage.setScene(scene);
		primaryStage.show();

		// Register an event handler.
		submit.setOnAction(e -> {
			try {
				actualValue = Double.parseDouble(input.getText());
				assessmentValue = actualValue * ASSESSMENT;
				taxTotal = (assessmentValue / 100) * PROPERTY_TAX;
				output.setText(String.format("The actual value is: $%,.2f \n" + "The assessment value is: $%,.2f\n"
						+ "Total property tax owed is: $%,.2f", actualValue, assessmentValue, taxTotal));

			} catch (NumberFormatException ex) {
				output.setText("must be in number format (example - 10.99 or 5)");

			}
		});
	}

}
