package Ex09;

import java.util.Random;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class TicTacToe extends Application {

	int[][] numbers;
	ImageView[][] images;
	Random rand;
	int nums;
	int xWins = 0;
	int oWins = 0;
	HBox row1;
	HBox row2;
	HBox row3;
	HBox row4;

	ImageView one = new ImageView();
	ImageView two = new ImageView();
	ImageView three = new ImageView();
	ImageView four = new ImageView();
	ImageView five = new ImageView();
	ImageView six = new ImageView();
	ImageView seven = new ImageView();
	ImageView eight = new ImageView();
	ImageView nine = new ImageView();

	Image x;
	Image o;

	Button newGame;

	Label results;

	public int xWins(int[][] board) {

		// Check Diagonal
		if (board[0][0] == 1 && board[1][1] == 1 && board[2][2] == 1) {
			xWins++;
		}
		if (board[0][2] == 1 && board[1][1] == 1 && board[2][0] == 1) {
			xWins++;

		}

		// Check Columns
		if (board[0][0] == 1 && board[1][0] == 1 && board[2][0] == 1) {
			xWins++;

		}
		if (board[0][1] == 1 && board[1][1] == 1 && board[2][1] == 1) {
			xWins++;

		}
		if (board[0][2] == 1 && board[1][2] == 1 && board[2][2] == 1) {
			xWins++;

		}

		// Check Rows
		if (board[0][0] == 1 && board[0][1] == 1 && board[0][2] == 1) {
			xWins++;

		}
		if (board[1][0] == 1 && board[1][1] == 1 && board[1][2] == 1) {
			xWins++;

		}
		if (board[2][0] == 1 && board[2][1] == 1 && board[2][2] == 1) {
			xWins++;

		}
		return xWins;

	}

	public int oWins(int[][] board) {

		// Check Diagonal
		if (board[0][0] == 0 && board[1][1] == 0 && board[2][2] == 0) {
			oWins++;
		}
		if (board[0][2] == 0 && board[1][1] == 0 && board[2][0] == 0) {
			oWins++;

		}

		// Check Columns
		if (board[0][0] == 0 && board[1][0] == 0 && board[2][0] == 0) {
			oWins++;

		}
		if (board[0][1] == 0 && board[1][1] == 0 && board[2][1] == 0) {
			oWins++;

		}
		if (board[0][2] == 0 && board[1][2] == 0 && board[2][2] == 0) {
			oWins++;

		}

		// Check Rows
		if (board[0][0] == 0 && board[0][1] == 0 && board[0][2] == 0) {
			oWins++;

		}
		if (board[1][0] == 0 && board[1][1] == 0 && board[1][2] == 0) {
			oWins++;

		}
		if (board[2][0] == 0 && board[2][1] == 0 && board[2][2] == 0) {
			oWins++;

		}
		return oWins;

	}

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		rand = new Random();
		numbers = new int[3][3];
		one = new ImageView();
		two = new ImageView();
		three = new ImageView();
		four = new ImageView();
		five = new ImageView();
		six = new ImageView();
		seven = new ImageView();
		eight = new ImageView();
		nine = new ImageView();
		images = new ImageView[][] { { one, two, three }, { four, five, six }, { seven, eight, nine }

		};
		newGame = new Button("New Game");
		results = new Label();
		x = new Image("file:x.png");
		o = new Image("file:o.png");

		row1 = new HBox(15, one, two, three);
		row1.setAlignment(Pos.CENTER);
		row1.setPadding(new Insets(10));

		row2 = new HBox(15, four, five, six);
		row2.setAlignment(Pos.CENTER);
		row2.setPadding(new Insets(10));

		row3 = new HBox(15, seven, eight, nine);
		row3.setAlignment(Pos.CENTER);
		row3.setPadding(new Insets(10));

		row4 = new HBox(15, newGame, results);
		row4.setAlignment(Pos.CENTER);
		row4.setPadding(new Insets(10));
		GridPane grid = new GridPane();
		grid.add(row1, 0, 0);
		grid.add(row2, 0, 1);
		grid.add(row3, 0, 2);
		grid.add(row4, 0, 3);

		Scene scene = new Scene(grid, 800, 600);
		primaryStage.setScene(scene);
		primaryStage.show();

		newGame.setOnAction(e -> {
			for (int i = 0; i < numbers.length; i++) {
				for (int j = 0; j < numbers[i].length; j++) {
					nums = rand.nextInt(2);

					numbers[i][j] = nums;
				}

			}

			for (int i = 0; i < numbers.length; i++) {
				for (int j = 0; j < numbers[i].length; j++) {

					if (numbers[i][j] == 1) {
						images[i][j].setImage(x);
					}
					if (numbers[i][j] == 0) {
						images[i][j].setImage(o);
					}
				}

			}
			if (xWins(numbers) > oWins(numbers)) {
				results.setText("X Wins");
			} else if (xWins(numbers) < oWins(numbers)) {
				results.setText("O Wins");
			} else {
				results.setText("Draw");
			}
			xWins = 0;
			oWins = 0;

		});

	}

}
