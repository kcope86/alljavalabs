package Ex02;

import java.util.Scanner;

public class TimeCalculator {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		char repeat;
		do {
			String input;

			System.out.print("Enter a number of seconds: ");
			int seconds = keyboard.nextInt();
			keyboard.nextLine();

			final int MINUTES = 60;
			final int HOURS = 3600;
			final int DAYS = 86400;

			int leftOverSeconds;
			int numOfMinutes;
			int numOfHours;
			int numOfDays;
			if (seconds < 0) {
				System.out.println("Please enter a number above 0: ");
			}

			else if (seconds < 60 && seconds > 0) {
				System.out.printf("You entered %d seconds.\n", seconds);
			}

			else if (seconds >= 60 && seconds < HOURS) {
				leftOverSeconds = seconds % MINUTES;
				System.out.printf("You entered %d minute(s) and %d second(s).\n", seconds / 60, leftOverSeconds);
			}

			else if (seconds >= HOURS && seconds < DAYS) {
				numOfHours = seconds / HOURS;
				numOfMinutes = (seconds - (numOfHours * HOURS)) / MINUTES;
				leftOverSeconds = (seconds - (numOfHours * HOURS)) % MINUTES;
				System.out.printf("You entered %d hour(s), %d minute(s), and %d second(s) ", numOfHours, numOfMinutes,
						leftOverSeconds);
			}

			else if (seconds >= DAYS) {
				numOfDays = seconds / DAYS;
				numOfHours = (seconds - (numOfDays * DAYS)) / HOURS;
				numOfMinutes = ((seconds - (numOfDays * DAYS)) % HOURS) / MINUTES;
				leftOverSeconds = ((seconds - (numOfDays * DAYS)) % HOURS) % MINUTES;
				System.out.printf("You entered %d days, %d hour(s), %d minute(s), and %d second(s) ", numOfDays,
						numOfHours, numOfMinutes, leftOverSeconds);
			}

			System.out.println("Would you like to try again?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.nextLine();
			repeat = input.charAt(0);
		} while (repeat == 'Y' || repeat == 'y');

		keyboard.close();
	}

}
