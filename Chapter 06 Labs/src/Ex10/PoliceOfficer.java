package Ex10;

public class PoliceOfficer {
	private String name;
	private int badgeNumber;

	public ParkingTicket inspectCar(ParkedCar car, ParkingMeter meter) {
		ParkingTicket p1 = null;
		if (car.getMinutesParked() > meter.getMinutesPurchased()) {
			p1 = new ParkingTicket(car, this, meter);

		}
		return p1;

	}

	public PoliceOfficer(String name, int badgeNumber) {

		this.name = name;
		this.badgeNumber = badgeNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBadgeNumber() {
		return badgeNumber;
	}

	public void setBadgeNumber(int badgeNumber) {
		this.badgeNumber = badgeNumber;
	}

}
