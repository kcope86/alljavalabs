package Ex13;

public class BMI {
	private double weight;
	private double height;

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public BMI(double weight, double height) {
		this.weight = weight;
		this.height = height;
	}

	public double getBMI() {
		double bmi = weight * 703 / Math.pow(height, 2.0);
		return bmi;
	}

	public String isHealthy() {
		double bmi = weight * 703 / Math.pow(height, 2.0);
		if (bmi < 18.5) {
			return "underweight";
		}
		if (bmi >= 18.5 && bmi <= 25) {
			return "optimal";
		} else {
			return "overweight";
		}

	}
}
