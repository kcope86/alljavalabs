package Ex09;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class SerializeDriver {

	public static void main(String[] args) throws IOException {
		Scanner keyboard = new Scanner(System.in);

		int[] exam1 = new int[5];
		int size;

		System.out.println("Please enter 5 test scores");

		for (int i = 0; i < exam1.length; i++) {
			System.out.println("Enter test score" + (i + 1));
			exam1[i] = keyboard.nextInt();

		}

		TestScores exam;

		try {
			exam = new TestScores(exam1);

		}

		catch (IllegalArgumentException ex) {
			System.out.println(ex.toString());
			exam = new TestScores(exam1);
		}
		System.out.printf("The average of the 5 test scores is: %.2f\n", exam.getAverage());

		ObjectOutputStream objectOutputFile = new ObjectOutputStream(new FileOutputStream("testScores.dat"));

		objectOutputFile.writeObject(exam);

		objectOutputFile.close();
		System.out.println("test score objects saved to testScores.dat");
	}

}
