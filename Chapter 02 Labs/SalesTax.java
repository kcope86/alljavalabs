import java.util.Scanner

public class SalesTax
{
	
	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			
			double purchaseAmount;
			double totalTax;
			final double STATE_TAX = 0.055;
			final double COUNTY_TAX = 0.02;
			double tax;
			
			
			System.out.print("What was the purchase amount? ");
			purchaseAmount = keyboard.nextDouble();
			System.out.println(purchaseAmount);
			System.out.println("State Tax: $" + (purchaseAmount * STATE_TAX) );
			System.out.println("County Tax: $" + (purchaseAmount * COUNTY_TAX) );
			System.out.println("Total Tax: $" + (purchaseAmount * STATE_TAX) );
			
			
			
			System.out.println("Would you like to check another purchase?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
		}while (repeat == 'Y' || repeat == 'y');
		
		
	}
}