package Ex01;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Sales {

	public static void main(String[] args) throws IOException {

		Scanner keyboard = new Scanner(System.in);

		PrintWriter outputFile = new PrintWriter("WeeklySales.txt");
		DecimalFormat cash = new DecimalFormat("#0.00");
		double totalSales = 0;
		double todaySales;
		for (int i = 1; i < 6; i++) {
			do {
				System.out.println("Enter the sales ammount for day " + i + ": ");
				todaySales = keyboard.nextDouble();
				if (todaySales < 0) {
					System.out.println("Sales for the day cannot be negative.");
				}
			} while (todaySales < 0);
			totalSales += todaySales;
			outputFile.println("Day " + i + "'s sales are $" + cash.format(todaySales));

		}
		outputFile.println("Total sales are $" + cash.format(totalSales));
		System.out.println("WeeklySales.txt file created");
		outputFile.close();
	}

}
