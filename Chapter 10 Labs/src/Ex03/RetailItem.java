package Ex03;

public class RetailItem {

	String description;
	int unitsOnHand;
	double price;

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUnitsOnHand(int units) throws InvalidQuantityException {
		if (units < 0) {
			throw new InvalidQuantityException("Quantity cannot be negative");
		}
		unitsOnHand = units;

	}

	public void setPrice(double price) throws InvalidPriceException {
		if (price < 0) {
			throw new InvalidPriceException("Price cannot be negative");
		}
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public int getUnitsOnHand() {
		return unitsOnHand;
	}

	public double getPrice() {
		return price;
	}
}
