package Ex11;

public class Employee {
	private String name;
	private int employeeNumber;
	private String hireDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {

		this.employeeNumber = employeeNumber;

	}

	public String getHireDate() {
		return hireDate;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}

	public Employee(String name, int employeeNumber, String hireDate) {

		this.name = name;
		this.employeeNumber = employeeNumber;
		this.hireDate = hireDate;
	}

	public Employee() {

	}

	public static void isValid(int empNumber) throws InvalidEmployeeNumberException {
		if (empNumber < 0 || empNumber > 9999) {
			throw new InvalidEmployeeNumberException("Invalid Employee Number");
		}

	}

}
