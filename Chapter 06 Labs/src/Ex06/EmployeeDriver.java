package Ex06;

public class EmployeeDriver {
	public static void main(String[] args) {

		Employee emp1 = new Employee("Kevin Cope", 123, "Programming", "Developer");
		Employee emp2 = new Employee("Kevin Cope", 456);
		Employee emp3 = new Employee();

		System.out.println("Employee 1 data:");
		System.out.println("Name: " + emp1.getName());
		System.out.println("ID number: " + emp1.getIdNumber());
		System.out.println("Department: " + emp1.getDepartment());
		System.out.println("Position: " + emp1.getPosition());
		System.out.println();

		System.out.println("Employee 2 data:");
		System.out.println("Name: " + emp2.getName());
		System.out.println("ID number: " + emp2.getIdNumber());
		System.out.println("Department: " + emp2.getDepartment());
		System.out.println("Position: " + emp2.getPosition());
		System.out.println();

		System.out.println("Employee 3 data:");
		System.out.println("Name: " + emp3.getName());
		System.out.println("ID number: " + emp3.getIdNumber());
		System.out.println("Department: " + emp3.getDepartment());
		System.out.println("Position: " + emp3.getPosition());
		System.out.println();

	}

}
