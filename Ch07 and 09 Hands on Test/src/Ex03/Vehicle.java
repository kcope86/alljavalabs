package Ex03;

public class Vehicle {
	int speed = 0;

	public int getSpeed() {
		return speed;
	}

	public void accelerate() {
		speed += 5;
	}

}
