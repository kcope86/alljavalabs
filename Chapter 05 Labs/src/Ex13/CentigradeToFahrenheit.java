package Ex13;

public class CentigradeToFahrenheit {

	public static void main(String[] args) {

		double f;
		double c;
		for (double i = 0; i < 20; i++) {
			c = i;
			f = 9f / 5f * i + 32;

			System.out.printf("At %.2f degrees Centigrade the temperature is %.2f Fahrenheit.\n", c, f);
		}

	}

}
