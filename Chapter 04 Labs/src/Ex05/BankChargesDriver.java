package Ex05;

import java.util.Scanner;

public class BankChargesDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		double startingBalance;
		double numOfChecks;

		System.out.print("enter your current bank balance: ");
		startingBalance = keyboard.nextDouble();
		System.out.println();
		System.out.print("How many checks did you write this month: ");
		numOfChecks = keyboard.nextDouble();
		System.out.println();

		BankCharges account1 = new BankCharges(startingBalance, numOfChecks);

		System.out.printf("Your beginning balance is %,.2f\n", startingBalance);
		System.out.println();
		System.out.printf("After the $10 monthly maintenance fee your balance is %,.2f\n", account1.applyMonthlyFee());
		System.out.println();
		if (account1.getBalance() < 400) {
			System.out.printf("Your account has dropped below $400 and a $15 service fee was applied\n"
					+ "Your balance is now %,.2f\n", account1.applyLowFundsFee());

		}
		System.out.println();
		System.out.printf("You wrote %.0f checks this month.\n", account1.getChecks());
		System.out.println();

		System.out.printf("Total check fee this month is $%,.2f\n", account1.calcCheckFees());
		System.out.println();
		System.out.printf("Your total service fees this month are $%,.2f\n", account1.getServiceFees());
		System.out.println();

		System.out.printf("Your ending balance is $%,.2f\n", (account1.getBalance() - account1.calcCheckFees()));
		System.out.println();

		keyboard.close();
	}

}
