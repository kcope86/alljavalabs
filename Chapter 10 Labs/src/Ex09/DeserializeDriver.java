package Ex09;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeserializeDriver {
	public static void main(String[] args) throws Exception {
		final int NUM_ITEMS = 5; // Number of items

		// Create the stream objects.

		ObjectInputStream objectInputFile = new ObjectInputStream(new FileInputStream("testScores.dat"));

		// Create an array to hold InventoryItem objects.
		TestScores exam1;

		// Read the serialized objects from the file.

		exam1 = (TestScores) objectInputFile.readObject();

		// Close the file.
		objectInputFile.close();

		// Display the objects.

		System.out.println("Your data has been decrypted.");
		System.out.println("Average of the test scores is " + exam1.getAverage() + ". ");

	}
}
