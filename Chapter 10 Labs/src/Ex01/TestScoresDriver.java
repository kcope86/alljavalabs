package Ex01;

import java.util.Scanner;

public class TestScoresDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		int[] exam1;
		int size;

		System.out.println("How many test scores do you need to enter? ");
		size = keyboard.nextInt();

		exam1 = new int[size];
		for (int i = 0; i < size; i++) {
			System.out.println("Enter test score" + (i + 1));
			exam1[i] = keyboard.nextInt();

		}

		TestScores exam;

		try {
			exam = new TestScores(exam1, size);

		}

		catch (IllegalArgumentException ex) {
			System.out.println(ex.toString());
			exam = new TestScores();
		}
		System.out.printf("The average of the %d test scores is: %.2f", size, exam.getAverage());

	}

}
