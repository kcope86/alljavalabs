import java.util.Scanner;
public class StringManipulator
{
	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			String input;
			String city;
			
			
			System.out.print("What is your favorite city?  ");
			city = keyboard.nextLine();
			int nameSize = city.length();
			String cityUpper = city.toUpperCase();
			String cityLower = city.toLowerCase();
			char fLetter = city.charAt(0);
			
			System.out.println(city + "has " + nameSize + " characters");
			System.out.println(cityUpper);
			System.out.println(cityLower);
			System.out.println(fLetter);
			
			
			
			
			System.out.println("Would you like to check different amounts?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
		}while (repeat == 'Y' || repeat == 'y');
		
	}
}