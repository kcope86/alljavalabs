import java.util.Scanner;
public class RestaurantBill
{
	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			String input;
			double mealCost;
			final double SALES_TAX = .075;
			final double TIP = .18;
			
			
			
			System.out.print("How much did your meal cost?  ");
			mealCost = keyboard.nextDouble();
			
			
			
			System.out.printf("your meal costs $%,.2f before tax and tip \n", mealCost);
			System.out.printf("Tip $%,.2f \n", mealCost * TIP);
			System.out.printf("Tax $%,.2f \n", mealCost * SALES_TAX);
			System.out.printf("Total is $%,.2f \n", mealCost  + (mealCost * SALES_TAX) + (mealCost * TIP));
			
			
			
			
			
			System.out.println("Would you like to check different amounts?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
		}while (repeat == 'Y' || repeat == 'y');
		
	}
}