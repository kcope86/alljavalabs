package Ex02;

public class InventoryItemDriver {

	public static void main(String[] args) {

		InventoryItem item1 = new InventoryItem("this item has a value of ", 5);
		InventoryItem item2 = new InventoryItem(item1);

		System.out.println("Item 1: " + item1.getDescription() + " " + item1.getUnits());
		System.out.println("Item 2: " + item2.getDescription() + " " + item2.getUnits());
	}

}
