package Ex06;

import java.io.IOException;
import java.util.Scanner;

public class FileArrayDriver {

	public static void main(String[] args) throws IOException {
		Scanner keyboard = new Scanner(System.in);

		int arraySize;
		System.out.print("How many numbers would you like to store in your array? ");
		arraySize = keyboard.nextInt();

		int[] writeArray = new int[arraySize];
		int[] readArray = new int[arraySize];

		System.out.println("Enter the numbers for your array");
		for (int i = 0; i < arraySize; i++) {
			System.out.print((i + 1) + ": ");
			writeArray[i] = keyboard.nextInt();
		}

		FileArray.writeArray("writeArray.dat", writeArray);

		try {
			readArray = FileArray.readArray("writeArray.dat", readArray);

		} catch (IOException ex) {
			System.out.println(ex.toString());
		}

		for (int i = 0; i < readArray.length; i++) {
			System.out.println(readArray[i]);

		}

	}

}
