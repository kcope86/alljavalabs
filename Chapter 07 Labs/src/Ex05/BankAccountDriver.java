package Ex05;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import Ex03.BankAccount;

public class BankAccountDriver {

	public static void main(String[] args) throws IOException {
		Scanner keyboard = new Scanner(System.in);
		File file = new File("AccountNumbers.txt");
		Scanner inputFile = new Scanner(file);

		int[] bankAccount = new int[18];

		for (int i = 0; i < bankAccount.length && inputFile.hasNext(); i++) {
			bankAccount[i] = inputFile.nextInt();
		}
		String input;
		char repeat;
		do {
			System.out.print("Please enter your account number: ");
			int idnum = keyboard.nextInt();

			if (BankAccount.accountSearch(bankAccount, idnum) == -1) {
				System.out.print("Account number not found");
				System.out.println();

			} else {
				System.out.println(bankAccount[BankAccount.accountSearch(bankAccount, idnum)] + " Is valid.");
				System.out.println();
			}

			System.out.print("Would you like to check a different account number? ");
			keyboard.nextLine();
			input = keyboard.nextLine().toUpperCase();
			repeat = input.charAt(0);

		} while (repeat != 'N');
	}

}
