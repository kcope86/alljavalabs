package Ex05;

public class InvalidNameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidNameException(String msg) {
		super(msg);
	}
}
