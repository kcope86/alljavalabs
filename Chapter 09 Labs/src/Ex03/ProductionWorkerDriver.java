package Ex03;

import java.util.Scanner;

public class ProductionWorkerDriver {

	public static void main(String[] args) {
		String name;
		String empNumber = "XXX-X";
		String input;
		String hireDate;
		int shift;
		double payRate;
		final int TRAINING_NEEDED = 4;
		int trainingAttended;
		final double MONTHLY_BONUS = 1500;

		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter the employee name: ");
		name = keyboard.nextLine();
		System.out.println();
		do {
			System.out.print("Enter employee number: ");
			input = keyboard.nextLine().toUpperCase();
			if (Employee.isValid(input)) {
				empNumber = input;
			} else {
				System.out.println("Invalid format for employee number.");
			}

		} while (!Employee.isValid(input));
		System.out.print("Enter Date Hired: ");
		hireDate = keyboard.nextLine();
		System.out.print("Enter pay rate: ");
		payRate = keyboard.nextDouble();
		do {
			System.out.print("Enter shift: ");
			shift = keyboard.nextInt();
			keyboard.nextLine();
			if (shift < 1 || shift > 2) {
				System.out.println("Employee must be shift 1 or shift 2.");
			}
		} while (shift < 1 || shift > 2);
		System.out.println();
		System.out.println("Enter ammount of training attended: ");
		trainingAttended = keyboard.nextInt();

		Employee worker2 = new TeamLeader(name, empNumber, hireDate, shift, payRate, MONTHLY_BONUS, TRAINING_NEEDED,
				trainingAttended);
		System.out.println(worker2.toString());
		System.out.println();
		keyboard.close();

	}

}
