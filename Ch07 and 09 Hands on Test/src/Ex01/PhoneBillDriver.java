package Ex01;

import java.util.Scanner;

public class PhoneBillDriver {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);

		// Arrays that store area codes and rates
		int[] areaCodes = new int[] { 262, 414, 608, 715, 815, 920 };

		// Arrays that store area codes and rates
		double[] rates = new double[] { .07, .10, .05, .16, .24, .14 };

		// fields for area code and minutes used
		int areaCode;
		int minutes;

		// loop for input of area code
		do {
			System.out.print("Enter your area code: ");
			areaCode = keyboard.nextInt();
			if (PhoneBill.areaSearch(areaCodes, areaCode) < 0) {
				System.out.println("Invalid area code");
			}
		} while (PhoneBill.areaSearch(areaCodes, areaCode) < 0);

		// loop for input of minutes
		do {
			System.out.print("How many minutes did you use: ");
			minutes = keyboard.nextInt();
			if (minutes < 0) {
				System.out.println("minutes used cannot be negative.");
			}
		} while (minutes < 0);

		// instantiate new phone bill object
		PhoneBill bill1 = new PhoneBill(areaCode, minutes);

		// output charges
		System.out.printf("Your total phone bill will be $%,.2f", bill1.calcBill());

	}

}
