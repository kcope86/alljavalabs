package Ex11;

public class Patient 
	{
	
	
		private String firstName;
		private String middleName;
		private String lastName;
		private String streetAddress;
		private String city;
		private String state;
		private String zip;
		private String phoneNumber;
		private String emergencyName;
		private String emergencyPhone;
		
		
		//Setters aka Mutators
		public void setFname(String fName)
		{
			firstName = fName;
		}
		
		public void setMname(String mName)
		{
			middleName = mName;
		}
		
		public void setLname(String lName)
		{
			lastName = lName;
		}
		
		public void setAddress(String address)
		{
			streetAddress = address;
		}
		
		public void setCity(String city)
		{
			this.city = city;
		}
		
		public void setState(String state)
		{
			this.state = state;
		}
		
		public void setZip(String zip)
		{
			this.zip = zip;
		}
		
		public void setPhone(String phone)
		{
			phoneNumber = phone;
		}
		
		public void setEmergencyName(String name)
		{
			emergencyName = name;
		}
		
		public void setEmergencyPhone(String ePhone)
		{
			emergencyPhone = ePhone;
		}
		
		//Getters aka Accessors
		
		public String getFirstName()
		{
			return firstName;
		}
		
		public String getMiddleName()
		{
			return middleName;
		}
		
		public String getLastName()
		{
			return lastName;
		}
		
		public String getAddress()
		{
			return streetAddress;
		}
		
		public String getCity()
		{
			return city;
		}
		
		public String getState()
		{
			return state;
		}
		
		public String getZip()
		{
			return zip;
		}
		
		public String getPhoneNumber()
		{
			return phoneNumber;
		}
		
		public String getEmergencyName()
		{
			return emergencyName;
		}
		
		public String getEmergencyPhone()
		{
			return emergencyPhone;
		}
		
		
		public Patient(String fName, String mName, String lName, String sAddress, String city, String state, String zip, String phone, String emergencyName, String emergencyPhone)
		{
			firstName =  fName;
			middleName = mName;
			lastName = lName;
			streetAddress = sAddress;
			this.city = city;
			this.state = state;
			this.zip = zip;
			phoneNumber = phone;
			this.emergencyName = emergencyName;
			this.emergencyPhone = emergencyPhone;
			
		}
		
		
	}
