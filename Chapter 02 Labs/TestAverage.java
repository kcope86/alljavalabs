import java.util.Scanner;
public class TestAverage
{
	public static void main(String[]args)
	{
		
		Scanner keyboard = new Scanner (System.in);
		char repeat;
		do
		{
			String input;
			double testOne;
			double testTwo;
			double testThree;
			double average;
			
			System.out.print("Enter the score for test one:  ");
			testOne = keyboard.nextDouble();
			System.out.print("Enter the score for test two:  ");
			testTwo = keyboard.nextDouble();
			System.out.print("Enter the score for test three:  ");
			testThree = keyboard.nextDouble();
			average = (testOne + testTwo + testThree) / 3;
			System.out.println("Test One ---  " + testOne +
			"\nTest Two --- "  + testTwo + 
			"\nTest Three ---" + testThree + 
			"\nTest Average - " + average);
			
			
			
			
			System.out.println("Would you like to check different amounts?");
			System.out.print("Enter Y for yes, or N for No ");
			input = keyboard.next();
			repeat = input.charAt(0);
		}while (repeat == 'Y' || repeat == 'y');
		
	}
}