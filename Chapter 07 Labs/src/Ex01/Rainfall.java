package Ex01;

public class Rainfall {

	private double[] monthlyRainfall;

	public Rainfall(double[] rain) {
		monthlyRainfall = new double[rain.length];
		for (int i = 0; i < rain.length; ++i) {
			monthlyRainfall[i] = rain[i];
		}

	}

	public double getYearly() {
		double yearlyRain = 0;
		for (double var : monthlyRainfall) {
			yearlyRain += var;
		}
		return yearlyRain;
	}

	public double getAvgRain() {

		return getYearly() / monthlyRainfall.length;
	}

	public int calcMost() {
		double most;
		int monthWithMost = 0;
		most = monthlyRainfall[0];
		for (int i = 1; i < monthlyRainfall.length; i++) {
			if (monthlyRainfall[i] > most) {
				most = monthlyRainfall[i];
				monthWithMost = i;
			}
		}
		return monthWithMost + 1;
	}

	public int calcLeast() {
		double least;
		int monthWithLeast;
		least = monthlyRainfall[0];
		monthWithLeast = 0;
		for (int i = 1; i < monthlyRainfall.length; i++) {
			if (monthlyRainfall[i] < least) {
				least = monthlyRainfall[i];
				monthWithLeast = i;
			}
		}
		return monthWithLeast + 1;
	}
}
