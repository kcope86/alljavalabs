package Ex10;

import java.util.Random;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SlotMachine extends Application {

	ImageView slot1;
	ImageView slot2;
	ImageView slot3;
	Image cherry;
	Image seven;
	Image gold;
	HBox slotWindow;
	HBox input;
	VBox container;
	VBox current;
	VBox totals;
	Label thisWager;
	Label results;
	Label total;
	Label prompt;
	Button spin;
	TextField bet;

	Random rand;
	int num;
	int[] slots;
	ImageView[] images;
	double winnings = 0;
	double thisRound = 0;
	double wager;
	double totalWager = 0;
	Label spent;
	Label upDown;

	public int checkMatches(int[] slots) {
		int matches = 0;
		int cherry = 0;
		int seven = 0;
		int gold = 0;

		for (int i = 0; i < slots.length; i++) {
			if (slots[i] == 0) {
				cherry++;
			} else if (slots[i] == 1) {
				seven++;
			} else {
				gold++;
			}
			if (cherry == 2 || seven == 2 || gold == 2) {
				matches = 2;
			} else if (cherry == 3 || seven == 3 || gold == 3) {
				matches = 3;
			} else {
				matches = 0;
			}

		}

		return matches;

	}

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		rand = new Random();
		cherry = new Image("file:cherry.png");
		seven = new Image("file:7.png");
		gold = new Image("file:gold.png");

		spin = new Button("Spin");
		bet = new TextField();
		bet.setPromptText("$0");
		prompt = new Label("Enter your bet");
		total = new Label();
		results = new Label();
		spent = new Label();
		upDown = new Label();
		thisWager = new Label();
		slot1 = new ImageView();
		slot2 = new ImageView();
		slot3 = new ImageView();

		slots = new int[3];
		images = new ImageView[] { slot1, slot2, slot3 };
		current = new VBox(15, thisWager, results);
		totals = new VBox(spent, total, upDown);

		slotWindow = new HBox(slot1, slot2, slot3);
		input = new HBox(20, prompt, bet);
		GridPane grid = new GridPane();
		grid.add(current, 0, 0);
		grid.add(totals, 1, 0);
		GridPane.setMargin(current, new Insets(20, 20, 20, 20)); // margins around the whole grid
		GridPane.setMargin(totals, new Insets(20, 20, 20, 20)); // margins around the whole grid

		container = new VBox(20, input, spin, grid, slotWindow);

		Scene scene = new Scene(container, 800, 600);
		primaryStage.setScene(scene);
		primaryStage.show();

		spin.setOnAction(e -> {
			bet.setStyle("-fx-text-inner-color: black;");

			try {
				wager = Double.parseDouble(bet.getText());
				totalWager += wager;
				thisWager.setText(String.format("Wagered this spin: $%,.2f", wager));
			} catch (NumberFormatException ex) {
				bet.setStyle("-fx-text-inner-color: red;");
				bet.setText("Must be number format");
				wager = 0;
			}

			for (int i = 0; i < slots.length; i++) {
				num = rand.nextInt(3);
				slots[i] = num;
			}

			for (int i = 0; i < slots.length; i++) {
				if (slots[i] == 0) {
					images[i].setImage(cherry);
				} else if (slots[i] == 1) {
					images[i].setImage(seven);
				} else {
					images[i].setImage(gold);
				}

			}

			thisRound = wager * checkMatches(slots);
			winnings += thisRound;
			spent.setText(String.format("Total Spent: $%,.2f", totalWager));
			results.setText(String.format("Ammount Won This Spin: $%,.2f", thisRound));
			total.setText(String.format("Total Ammount Won$%,.2f", winnings));
			if (winnings > totalWager) {
				upDown.setText(String.format("Up by $%,.2f", winnings - totalWager));

			}
			if (winnings < totalWager) {
				upDown.setText(String.format("Down by $%,.2f", totalWager - winnings));
			}
			if (winnings == totalWager) {
				upDown.setText("Cash out to break even");
			}

		});
	}

}
