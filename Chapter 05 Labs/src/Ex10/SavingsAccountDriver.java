package Ex10;

import java.util.Scanner;

public class SavingsAccountDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		double annualInterest;
		int months;
		double startingBalance;
		double deposits;
		double withdrawls;
		double depositsTotal;
		double withdrawlsTotal;
		double interestEarned;
		System.out.print("How many months has your account been open: ");
		months = keyboard.nextInt();
		System.out.print("Enter your starting balance: ");
		startingBalance = keyboard.nextDouble();
		System.out.print("Enter your annual interest rate: ");
		annualInterest = keyboard.nextDouble();

		SavingsAccount account1 = new SavingsAccount(startingBalance);
		account1.setInterestRate(annualInterest);

		for (int i = 1; i <= months; i++) {

			System.out.printf("How much was deposited in month %d: ", i);
			deposits = keyboard.nextDouble();
			account1.deposit(deposits);
			account1.calcTotalDeposits(deposits);

			System.out.printf("How much was withdrawn in month %d: ", i);
			withdrawls = keyboard.nextDouble();
			account1.withdrawl(withdrawls);
			account1.calcTotalWithdrawls(withdrawls);
			account1.addMonthlyInterest();
			account1.calcTotalInterest(account1.calcMonthlyInterest());

		}

		System.out.printf("total deposit ammount this period was $%,.2f\n", account1.getTotalDeposits());
		System.out.printf("total withdrawl ammount this period was $%,.2f\n", account1.getTotalWithdrawls());
		System.out.printf("total interest earned this period was $%,.2f\n", account1.getTotalInterest());
		System.out.printf("at this time your account balance is $%,.2f\n", account1.getBalance());

	}

}
