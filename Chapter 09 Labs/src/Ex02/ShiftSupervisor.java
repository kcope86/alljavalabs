package Ex02;

public class ShiftSupervisor extends Employee {

	private double salary;
	private double bonus;

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public ShiftSupervisor(String name, String employeeNumber, String hireDate, double salary, double bonus) {
		super(name, employeeNumber, hireDate);
		this.salary = salary;
		this.bonus = bonus;
	}

	public String toString() {
		return String.format(
				"Employee Name: %s\n" + "Employee Number: %s\n" + "Employee Hire Date: %s\n"
						+ "Employee salary: $%,.2f\n" + "Employee Salary: $%,.2f\n",
				this.getName(), this.getEmployeeNumber(), this.getHireDate(), this.getSalary(), this.getBonus());

	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

	public ShiftSupervisor() {

	}

}
