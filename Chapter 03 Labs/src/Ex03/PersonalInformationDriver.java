package Ex03;

public class PersonalInformationDriver {

	public static void main(String[] args) 
	{
		PersonalInformation p1 = new PersonalInformation();
		PersonalInformation p2 = new PersonalInformation();
		PersonalInformation p3 = new PersonalInformation();
		
		p1.setName("Kevin Cope");
		p1.setAddress("5636 Delintry dr. APT h St. Louis, MO 63129");
		p1.setAge(31);
		p1.setPhone("(636)215-5396");
		
		p2.setName("Katie White");
		p2.setAddress("5636 Delintry dr. APT h St. Louis, MO 63129");
		p2.setAge(24);
		p2.setPhone("(314)456-3719");
		
		p3.setName("Michael Cope");
		p3.setAddress("2018 Arlene dr Arnold, MO 63010");
		p3.setAge(26);
		p3.setPhone("(636)282-2886");
		
		System.out.println(p1.getName());
		System.out.println(p1.getAddress());
		System.out.println(p1.getAge());
		System.out.println(p1.getPhone());
		System.out.println();
		
		System.out.println(p2.getName());
		System.out.println(p2.getAddress());
		System.out.println(p2.getAge());
		System.out.println(p2.getPhone());
		System.out.println();

		System.out.println(p3.getName());
		System.out.println(p3.getAddress());
		System.out.println(p3.getAge());
		System.out.println(p3.getPhone());
		System.out.println();

		
		
		
	}

}
