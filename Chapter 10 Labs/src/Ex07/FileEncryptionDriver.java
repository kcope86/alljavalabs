package Ex07;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class FileEncryptionDriver {

	public static void main(String[] args) throws IOException {

		String[] friends = new String[3];

		Scanner keyboard = new Scanner(System.in);

		System.out.print("Enter the names of three friends\n");

		for (int i = 0; i < friends.length; i++) {
			System.out.print("Enter a name for friend " + (i + 1) + ": ");
			friends[i] = keyboard.nextLine();
		}

		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream("dataToEncrypt.dat"));
		for (int i = 0; i < 3; i++) {
			outputFile.writeUTF(friends[i]);
		}
		outputFile.close();

		FileEncryption.encrypt("dataToEncrypt.dat");
		System.out.println("Your data has been encrypted.");

	}

}
