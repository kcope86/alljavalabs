package HOT0304;

import java.util.Scanner;

public class LawnDriver {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);

		// variables for length and width of yards
		int length1, length2, width1, width2;

		// constant price variables for different lawn sizes
		final double UNDER_400_COST = 25.00;
		final double COST_400_600 = 35.00;
		final double OVER_600_COST = 50.00;

		// variable storing the number of weeks in a lawn mowing season
		final int SEASON = 20;

		// ask for length and width of first yard

		System.out.print("Enter the length of the first lawn");
		length1 = keyboard.nextInt();
		System.out.print("Enter the width of the first lawn");
		width1 = keyboard.nextInt();
		System.out.println();

		// create first lawn object using 2 parameter constructor
		Lawn lawn1 = new Lawn(length1, width1);

		// ask for length and width of first yard
		System.out.print("Enter the length of the second lawn");
		length2 = keyboard.nextInt();
		System.out.print("Enter the width of the second lawn");
		width2 = keyboard.nextInt();

		// create first lawn object using default constructor
		Lawn lawn2 = new Lawn();

		// set length and width of second lawn object using mutators
		lawn2.setLength(length2);
		lawn2.setWidth(width2);

		// call method to calculate area of both lawn objects
		System.out.printf("the area of the first lawn is %d square feet\n", lawn1.calcSquareFeet());
		System.out.printf("the area of the second lawn is %d square feet\n", lawn2.calcSquareFeet());

		// conditional statements to calculate weekly and monthly cost for lawn1 object
		if (lawn1.calcSquareFeet() < 400) {
			System.out.printf("The first lawn you entered will cost $%,.2f a week, or $%,.2f for the season.\n",
					UNDER_400_COST, UNDER_400_COST * SEASON);
		} else if (lawn1.calcSquareFeet() >= 400 && lawn1.calcSquareFeet() < 600) {
			System.out.printf("The first lawn you entered will cost $%,.2f a week, or $%,.2f for the season.\n",
					COST_400_600, COST_400_600 * SEASON);
		} else if (lawn1.calcSquareFeet() >= 600) {
			System.out.printf("The first lawn you entered will cost $%,.2f a week, or $%,.2f for the season.\n",
					OVER_600_COST, OVER_600_COST * SEASON);
		}

		// conditional statements to calculate weekly and monthly cost for lawn1 object
		if (lawn2.calcSquareFeet() < 400) {
			System.out.printf("The second lawn you entered will cost $%,.2f a week, or $%,.2f for the season.\n",
					UNDER_400_COST, UNDER_400_COST * SEASON);
		} else if (lawn2.calcSquareFeet() >= 400 && lawn2.calcSquareFeet() < 600) {
			System.out.printf("The second lawn you entered will cost $%,.2f a week, or $%,.2f for the season.\n",
					COST_400_600, COST_400_600 * SEASON);
		} else if (lawn2.calcSquareFeet() >= 600) {
			System.out.printf("The second lawn you entered will cost $%,.2f a week, or $%,.2f for the season.\n",
					OVER_600_COST, OVER_600_COST * SEASON);
		}

	}

}
