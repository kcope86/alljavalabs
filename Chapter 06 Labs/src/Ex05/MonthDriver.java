package Ex05;

import java.util.Scanner;

public class MonthDriver {

	public static void main(String[] args) {

		int monthNum;
		String monthName;

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the name of the month you were born.");
		System.out.print("NOTE - ENTERING AN INVALID VALUE WILL DEFAULT TO JANUARY!\n");
		monthName = keyboard.nextLine().toUpperCase();

		System.out.print("Enter the number of your birth month: ");
		System.out.print("NOTE - ENTERING AN INVALID VALUE WILL DEFAULT TO JANUARY!\n");
		monthNum = keyboard.nextInt();

		Month numberMonth = new Month(monthNum);

		Month nameMonth = new Month(monthName);

		System.out.printf("the first month you entered was %s\n", nameMonth.toString());

		System.out.printf("the second month you entered was %s\n", numberMonth.toString());

		if (nameMonth.equals(numberMonth)) {
			System.out.println("These months are equal.");

		} else {
			System.out.println("These months are not equal");
		}

		if (nameMonth.greaterThat(numberMonth)) {
			System.out.printf("%s is greater than %s\n", nameMonth, numberMonth);
		}
		if (nameMonth.lessThan(numberMonth)) {
			System.out.printf("%s is less than %s\n", nameMonth, numberMonth);

		}

	}

}
