
public class Employee 
{
private String name;
private int idNumber;
private String department;
private String position;

public void setName(String x)
{
	name = x;
}

public void setIdNumber(int idNum)
{
	idNumber = idNum;
}

public void setDepartment(String DPT)
{
	department = DPT;
}

public void setPosition(String pos)
{
	position = pos;
}

public String getName()
{
	return name;
}

public int getId()
{
	return idNumber;
}

public String getDepartment()
{
	return department;
}

public String getPosition()
{
	return position;
}

/*public String override toString()
{
	return 
}
*/


}
