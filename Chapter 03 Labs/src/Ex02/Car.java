package Ex02;

public class Car 
{
	int yearModel;
	String make;
	int speed;
	
	
	public Car(int year, String make)
	{
		yearModel = year;
		this.make = make;
		speed = 0;
	}
	
	public int getYear()
	{
		return yearModel;
	}
	
	
	public String getMake()
	{
		return make;
	}
	
	public int getSpeed()
	{
		return speed;
	}
	
	public void accelerate()
	{
		speed += 5;
	}
	
	public void brake()
	{
		speed -= 5;
	}
	
}
