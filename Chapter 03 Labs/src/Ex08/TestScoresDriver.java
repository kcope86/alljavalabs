package Ex08;
import java.util.Scanner;
public class TestScoresDriver {

	
	
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		TestScores t1 = new TestScores();
		
		System.out.print("Enter the first test score: ");
		t1.setTest1(keyboard.nextDouble());
		System.out.print("Enter the second test score: ");
		t1.setTest2(keyboard.nextDouble());
		System.out.print("Enter the third test score: ");
		t1.setTest3(keyboard.nextDouble());
		
		System.out.printf("The average of your test scores is %.2f", t1.getAverage());
		
		keyboard.close();
	}

}
