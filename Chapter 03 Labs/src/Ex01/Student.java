package Ex01;

public class Student

{
	// Instance fields with a private access modifier
	private int studentId;
	private String studentName;
	public double gpa;

	public double getGpa() {
		return gpa;
	}

	public void setGpa(double gpa) {
		this.gpa = gpa;
	}

	public void setStudentName(String name) {
		studentName = name;
	}

	public String getName() {
		return studentName;
	}

	// Mutator method AKA setter
	public void setId(int id) {
		studentId = id;
	}

	// Accessor method AKA getter
	public int getId() {
		return studentId;
	}

}
