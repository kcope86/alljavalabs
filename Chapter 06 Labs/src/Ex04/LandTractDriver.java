package Ex04;

import java.util.Scanner;

public class LandTractDriver {

	public static void main(String[] args) {

		double length1;
		double length2;
		double width1;
		double width2;

		Scanner keyboard = new Scanner(System.in);

		System.out.print("Enter the length of the first tract of land: ");
		length1 = keyboard.nextDouble();

		System.out.print("Enter the width of the first tract of land: ");
		width1 = keyboard.nextDouble();

		System.out.print("Enter the length of the second tract of land: ");
		length2 = keyboard.nextDouble();

		System.out.print("Enter the width of the second tract of land: ");
		width2 = keyboard.nextDouble();

		LandTract t1 = new LandTract(length1, width1);
		LandTract t2 = new LandTract(length2, width2);

		System.out.println(t1.toString());
		System.out.println(t2.toString());
		if (t1.equals(t2)) {
			System.out.println("These are the same size.");
		} else {
			System.out.println("These are not the same size.");
		}

	}

}
