package Ex02;

import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		int[] employeeId = new int[] { 5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489 };
		int[] hours = new int[7];
		double[] payRate = new double[7];

		int hoursWorked = 0;
		double hourlyPay = 0;

		for (int i = 0; i < employeeId.length; i++) {
			do {
				System.out.print("Enter the hours for employee: " + employeeId[i] + " ");
				hoursWorked = keyboard.nextInt();
				if (hoursWorked < 1) {
					System.out.println("Hours cannot be less than 1.");
					System.out.println();
				}
				hours[i] = hoursWorked;
			} while (hoursWorked < 1);

			do {
				System.out.print("Enter the pay rate for employee: " + employeeId[i] + " ");
				hourlyPay = keyboard.nextInt();
				System.out.println();

				if (hourlyPay < 6.00) {
					System.out.println("Hours cannot be less than 1.");
					System.out.println();

				}
			} while (hourlyPay < 6.00);
			payRate[i] = hourlyPay;
		}

		Payroll q1 = new Payroll(hours, payRate);

		for (int i = 0; i < employeeId.length; i++) {
			System.out.println("gross pay for employee #" + employeeId[i] + " is: $" + q1.getGrossPay(employeeId[i]));
		}
		System.out.println();

		int idNumber = 0;
		do {
			System.out.print("Enter an ID number to check the gross pay or -1 to quit: ");
			idNumber = keyboard.nextInt();
			if (idNumber == -1) {
				break;
			}

			if (q1.idCheck(employeeId, idNumber) == -1) {
				System.out.println("That ID number was not found.");
				System.out.println();

			} else {
				System.out.println("The gross pay for " + idNumber + " is $" + q1.getGrossPay(idNumber));
				System.out.println();

			}

		} while (idNumber != -1);
		System.out.println("Thank you for using the payroll system.");
	}

}
