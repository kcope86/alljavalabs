package Ex10;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Steps {
	int year[][] = new int[12][];
	Scanner inputFile;

	public Steps() throws IOException {

		File file = new File("steps.txt");
		Scanner inputFile = new Scanner(file);

		year[0] = new int[31];
		year[1] = new int[28];
		year[2] = new int[31];
		year[3] = new int[30];
		year[4] = new int[31];
		year[5] = new int[30];
		year[6] = new int[31];
		year[7] = new int[31];
		year[8] = new int[30];
		year[9] = new int[31];
		year[10] = new int[30];
		year[11] = new int[31];

		int month = 0;

		do {
			for (int i = 0; i < year[month].length && inputFile.hasNext(); i++) {
				year[month][i] = inputFile.nextInt();
			}
			month++;
		} while (month < 12);
	}

	public double monthlyAverage(int month) {
		double monthAverage = 0;
		int days = 1;

		for (int i = 0; i < year[month].length; i++) {
			monthAverage += year[month][i];
			days++;
		}
		monthAverage /= days;
		return monthAverage;
	}

	public double yearlyAverage() {
		double yearlyAverage = 0;
		int month = 0;
		int days = 1;

		do {
			for (int i = 0; i < year[month].length && inputFile.hasNext(); i++) {
				yearlyAverage += year[month][i];
			}
			month++;
		} while (month < 12);
		yearlyAverage /= days;
		return yearlyAverage;
	}
}
