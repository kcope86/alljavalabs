package Ex03;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class DistanceDriver {

	public static void main(String[] args) throws IOException {

		Scanner keyboard = new Scanner(System.in);
		int speed;
		int time;
		do {
			System.out.print("Enter a speed: ");
			speed = keyboard.nextInt();
			if (speed < 0) {
				System.out.println("speed cannot be less than zero miles per hour.");
			}
		} while (speed < 0);

		do {
			System.out.print("Enter a time: ");
			time = keyboard.nextInt();
			if (time < 1) {
				System.out.println("time cannot be less than one hour.");
			}
		} while (time < 1);

		FileWriter fw = new FileWriter("Distances.txt", true);
		PrintWriter outputFile = new PrintWriter(fw);
		Distance distance1 = new Distance(speed, time);

		for (int hours = 1; hours <= time; hours++) {
			distance1.setTimeInHours(hours);

			outputFile.println("in " + hours + " hour(s) you traveled " + distance1.getDistance() + " miles\n");

		}
		outputFile.close();
		keyboard.close();

	}

}
