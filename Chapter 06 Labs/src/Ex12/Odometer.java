package Ex12;

public class Odometer {

	private int mileage;

	public int getMileage() {
		return mileage;
	}

	public void addMiles() {
		if (mileage < 999999) {
			mileage++;
		} else {
			mileage = 0;
		}

	}

	public void decreaseGas(FuelGauge g1) {
		if (mileage % 24 == 0)
			g1.burnGas();
	}

	public Odometer() {
		mileage = 0;
	}

}
