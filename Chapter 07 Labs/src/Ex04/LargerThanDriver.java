package Ex04;

import java.util.Scanner;

public class LargerThanDriver {
	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		String input;
		char repeat;
		int arraySize;
		int checkNum;
		int[] array;
		do {
			do {
				System.out.print("How many numbers would you like in your array? ");
				arraySize = keyboard.nextInt();
				System.out.println();

				if (arraySize < 1) {
					System.out.println("Array must store at least 1 integer.");
					System.out.println();
				}
			} while (arraySize < 1);

			array = new int[arraySize];
			System.out.println("Enter the numbers you would like in your array.");
			for (int i = 0; i < array.length; ++i) {
				System.out.print("enter value for number " + (i + 1) + ": ");
				array[i] = keyboard.nextInt();
			}
			System.out.println();

			System.out.print("Enter smallest number to display all greater numbers in array: ");
			checkNum = keyboard.nextInt();
			keyboard.nextLine();
			LargerThanN.displayLargerThan(array, checkNum);
			System.out.println();

			System.out.println("Would you like to try again?");
			System.out.print("Enter Y for yes, or N for no: ");
			input = keyboard.nextLine().toUpperCase();
			repeat = input.charAt(0);

		} while (repeat != 'N');

	}

}
