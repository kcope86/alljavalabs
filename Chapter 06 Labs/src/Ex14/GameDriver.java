package Ex14;

import java.util.Scanner;

public class GameDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		int guess1;
		int guess2;
		Coin flip;
		String input;
		char repeat;

		System.out.print("Enter name for player 1: ");
		Player player1 = new Player(keyboard.nextLine());
		System.out.print("Enter name for player 2: ");
		Player player2 = new Player(keyboard.nextLine());
		System.out.println();
		flip = new Coin();

		do {
			flip.toss();
			System.out.println(player1.getName() + "'s score: " + player1.getScore());
			System.out.println(player2.getName() + "'s score: " + player2.getScore());
			System.out.println();
			System.out.println(player1.getName() + " Press 1 for heads, or 2 for tails");
			guess1 = keyboard.nextInt();
			System.out.println();
			System.out.println(player2.getName() + "Press 1 for heads, or 2 for tails");
			guess2 = keyboard.nextInt();
			keyboard.nextLine();
			System.out.println();
			System.out.println("it's " + flip.getSideUp());
			if (player1.guess(guess1) == flip.getSideUp() && player2.guess(guess2) != flip.getSideUp()) {
				System.out.println(player1.getName() + " wins");
				player1.wins();
			} else if (player1.guess(guess1) != flip.getSideUp() && player2.guess(guess2) == flip.getSideUp()) {
				System.out.println(player2.getName() + " wins");
				player2.wins();
			} else if (player1.guess(guess1) == flip.getSideUp() && player2.guess(guess2) == flip.getSideUp()) {
				System.out.println("DRAW!");
				player1.wins();
				player2.wins();
			}

			if (player1.getScore() == 5) {
				break;
			}
			if (player2.getScore() == 5) {
				break;
			}
			System.out.print("Press F to flip again: ");
			input = keyboard.nextLine().toUpperCase();
			repeat = input.charAt(0);

		} while (repeat == 'F');
		if (player1.getScore() == 5 && player2.getScore() != 5) {
			System.out.println(player1.getName() + " Wins!");
		} else if (player2.getScore() != 5 && player2.getScore() == 5) {
			System.out.println(player2.getName() + " Wins!");
		} else if (player1.getScore() == 5 && player2.getScore() == 5) {
			System.out.println("It's a Draw!");
		}
	}

}
