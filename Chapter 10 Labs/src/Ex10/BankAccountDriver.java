package Ex10;

import java.io.IOException;

public class BankAccountDriver {

	public static void main(String[] args) throws NegativeStartingBalance, NegativeInterestRate, IOException {

		String fileName = "accounts.dat";
		BankAccount read1;
		BankAccount read2;
		BankAccount read3;
		BankAccount modified;

		BankAccount account1 = new BankAccount(20, 0.05);
		BankAccount account2 = new BankAccount(200, 0.1);
		BankAccount account3 = new BankAccount(300, 0.05);

		BankAccountFile file1 = new BankAccountFile(fileName);
		file1.writeAccountItem(account1);
		file1.writeAccountItem(account2);
		file1.writeAccountItem(account3);

		file1.moveFilePointer(2);
		read3 = file1.readAccountItem();

		file1.moveFilePointer(0);
		read1 = file1.readAccountItem();
		read2 = file1.readAccountItem();
		System.out.println("Account 1");

		System.out.println(read1.getBalance());
		System.out.println(read1.getInterestRate());
		System.out.println();
		System.out.println("Account 2");

		System.out.println(read2.getBalance());
		System.out.println(read2.getInterestRate());
		System.out.println();
		System.out.println("Account 3");

		System.out.println(read3.getBalance());
		System.out.println(read3.getInterestRate());
		System.out.println();

		read2.addInterest();
		file1.moveFilePointer(1);
		file1.writeAccountItem(read2);

		file1.moveFilePointer(1);

		modified = file1.readAccountItem();
		System.out.println("Modified account 2 (after interest is added using addInterest())");

		System.out.println(modified.getBalance());
		System.out.println(modified.getInterestRate());

	}

}
