package Ex10;
import java.util.Scanner;
public class PetDriver {

	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		
		Pet pet1 = new Pet();
		
		System.out.print("What kind of pet do you have? ");
		pet1.setType(keyboard.nextLine());
		System.out.println();
		
		
		System.out.print("What is your pets name? ");
		pet1.setName(keyboard.nextLine());
		System.out.println();
		
		
		System.out.print("How old is your pet? ");
		pet1.setAge(keyboard.nextInt());
		System.out.println();
		
		System.out.printf("You have a %d year old %s named %s.", pet1.getAge(), pet1.getType(), pet1.getName());
		
		
	}

}
