package Ex01;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LatinToEnglish extends Application {

	private Label messageLbl;
	private Label englishLbl;

	public static void main(String[] args) {
		// Launch the application.
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {

		primaryStage.setTitle("This window translates Latin to english.");
		messageLbl = new Label("Click on a button to translate it's word to English.");
		Button b1 = new Button("Sinister");
		Button b2 = new Button(" Dexter ");
		Button b3 = new Button("Medium");
		englishLbl = new Label("");

		VBox vBox = new VBox(10, messageLbl, b1, b2, b3, englishLbl);
		vBox.setAlignment(Pos.CENTER);
		Scene scene = new Scene(vBox, 400, 200);
		primaryStage.setScene(scene);
		primaryStage.show();

		b1.setOnAction(new SinisterClickHandler());
		b2.setOnAction(new DexterClickHandler());
		b3.setOnAction(new MediumClickHandler());

	}

	class SinisterClickHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			englishLbl.setText("Left");
		}
	}

	class DexterClickHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			englishLbl.setText("Right");
		}
	}

	class MediumClickHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			englishLbl.setText("Center");
		}
	}

}
