package Ex09;

public class Circle 
	{
		private double radius;
		final private double PI = 3.14159;

		public void setRadius(double r)
		{
			radius = r;
		}
		
		public double getRadius()
		{
			return radius;
		}
		
		public Circle(double radius)
		{
			this.radius = radius;
		}
		
		public double getArea()
		{
			double area = PI * radius * radius;
			return area;
		}
		
		public double getCircumference()
		{
			double circumference = 2 * PI * radius;
			return circumference;
		}
		
		public double getDiameter()
		{
			double diameter = radius * 2;
			return diameter;
			
		}
	}
