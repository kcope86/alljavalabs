package Ex03;

public class RoomDimension {

	private double length;
	private double width;

	public RoomDimension(double length, double width) {
		this.length = length;
		this.width = width;
	}

	public double getArea() {
		return length * width;
	}

	public String toString() {
		return "the length of the room is: " + length + "\nthe width of the room is: " + width
				+ "\nthe area of the room is: " + getArea();

	}

}
