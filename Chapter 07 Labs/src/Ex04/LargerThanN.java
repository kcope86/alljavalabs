package Ex04;

public class LargerThanN {

	public static void displayLargerThan(int[] array, int n) {
		System.out.println("The numbers in your array that are greater than " + n + " are.");
		for (int i = 0; i < array.length; i++) {
			if (array[i] > n) {
				System.out.println(array[i]);
			}
		}
	}

}
