package Ex05;

public class Month {
	private int monthNumber;

	public Month() {
		monthNumber = 1;
	}

	public Month(int x) {
		if (x < 1 || x > 12)
			monthNumber = 1;
		else
			monthNumber = x;
	}

	public Month(String month) {
		switch (month) {
		case "JANUARY":
			monthNumber = 1;
			break;
		case "FEBRUARY":
			monthNumber = 2;
			break;
		case "MARCH":
			monthNumber = 3;
			break;
		case "APRIL":
			monthNumber = 4;
			break;
		case "MAY":
			monthNumber = 5;
			break;
		case "JUNE":
			monthNumber = 6;
			break;
		case "JULY":
			monthNumber = 7;
			break;
		case "AUGUST":
			monthNumber = 8;
			break;
		case "SEPTEMBER":
			monthNumber = 9;
			break;
		case "OCTOBER":
			monthNumber = 10;
			break;
		case "NOVEMBER":
			monthNumber = 11;
			break;
		case "DECEMBER":
			monthNumber = 12;
			break;
		default:
			monthNumber = 1;

		}

	}

	public void setMonthNumber(int monthNumber) {
		if (monthNumber < 1 || monthNumber > 12)
			this.monthNumber = 1;
		else
			this.monthNumber = monthNumber;
	}

	public int getMonthNumber() {
		return monthNumber;
	}

	public String getMonthName() {
		switch (monthNumber) {
		case 1:
			return "JANUARY";
		case 2:
			return "FEBRUARY";
		case 3:
			return "MARCH";
		case 4:
			return "APRIL";
		case 5:
			return "MAY";
		case 6:
			return "JUNE";
		case 7:
			return "JULY";
		case 8:
			return "AUGUST";
		case 9:
			return "SEPTEMBER";
		case 10:
			return "OCTOBER";
		case 11:
			return "NOVEMBER";
		case 12:
			return "DECEMBER";
		default:
			return "JANUARY";

		}
	}

	public String toString() {
		return getMonthName();
	}

	public boolean equals(Month m2) {

		boolean isEqual;

		if (this.monthNumber == m2.monthNumber) {
			isEqual = true;
		} else {
			isEqual = false;
		}
		return isEqual;
	}

	public boolean greaterThat(Month m2) {

		boolean isEqual;

		if (this.monthNumber > m2.monthNumber) {
			isEqual = true;
		} else {
			isEqual = false;
		}
		return isEqual;
	}

	public boolean lessThan(Month m2) {

		boolean isEqual;

		if (this.monthNumber < m2.monthNumber) {
			isEqual = true;
		} else {
			isEqual = false;
		}
		return isEqual;
	}

}
