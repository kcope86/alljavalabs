package Ex06;

public class Payroll 
{
	String name;
	int idNumber;
	double hourlyRate;
	double hoursWorked;

	
	public String getName()
	{
		return name;
	}
	
	public int getIdNumber()
	{
		return idNumber;
	}
	
	public double getHourly()
	{
		return hourlyRate;
	}
	
	public double getHours()
	{
		return hoursWorked;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setIdNumber(int id)
	{
		idNumber = id;
	}
	public void setHourlyRate(double rate)
	{
		hourlyRate = rate;
	}
	
	public void setHoursWorked(double hours)
	{
		hoursWorked = hours;
	}
	
	public double calcGrossPay(double hours, double rate)
	{
		double grossPay = hours * rate;
		return grossPay;
	}
	
}
