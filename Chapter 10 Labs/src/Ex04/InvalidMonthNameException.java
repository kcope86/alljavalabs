package Ex04;

public class InvalidMonthNameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidMonthNameException(String msg) {
		super(msg);
	}
}
