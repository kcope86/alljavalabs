package Ex02;

public class CarDriver 
{

	public static void main(String[] args) 
	{
		Car c1 = new Car(2011, "Mitsubishi");
		
		c1.accelerate();
		System.out.println(c1.getSpeed());
		c1.accelerate();
		System.out.println(c1.getSpeed());
		c1.accelerate();
		System.out.println(c1.getSpeed());
		c1.accelerate();
		System.out.println(c1.getSpeed());
		c1.accelerate();
		System.out.println(c1.getSpeed());
		
		c1.brake();
		System.out.println(c1.getSpeed());
		c1.brake();
		System.out.println(c1.getSpeed());
		c1.brake();
		System.out.println(c1.getSpeed());
		c1.brake();
		System.out.println(c1.getSpeed());
		c1.brake();
		System.out.println(c1.getSpeed());

	}

}
